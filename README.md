# avrstart

Quick Setup for programming Arduino clones under linux via USB and Arduino Bootloader, without Arduino IDE.

Requirements:
1. computer with debian
2. root access and a user on this computer
3. one cheap 'Arduino nano' clone or another board with USB and Atmega
4. usb cable

This will be done:
1. configuring of USB devices
2. avrdude to access 
3. gcc-avr for compiling

## Prepare User Permissions

The user needs to have the rights to use the USB ports, so add him to the suitable groups:

Possible groups:
* *dialout* - as if the user is allowed to use old serial attached modems
* *tty* - as if the user is allowed to use old teletype devices

### Excursion: Using *sudo* vs Gaining a Root Shell

On debian and/or ubuntu there are different ways to execute commands as root

1. *sudo command* - execute only one command
2. *sudo -s* - switch to root on ubuntu - if user is allowed to.
3. *su* - switch to root on debian - if user is allowed to.
4. Applications -> System Tools -> Rootshell or similar
5. Login as root on login prompt - let's ignore user accounts, do everything as root. **EVIL**
6. Login as root via ssh: *ssh -l root localhost* - but root login via ssh should be disabled

### Add User to Groups

Add a user to the groups with usermod:
```
 sudo usermod -a -G dialout <user>
 sudo usermod -aG tty <user>
```
or
```
 $ su
 # usermod -a -G dialout <user>
 # usermod -a -G tty <user>	
 # logout
 $
```

The groups and users are stored in */etc/group*, which can be edited directly .

as a user, check the groups with 
```
 groups
```

After changing the groups of a user, the user needs to sign out and log in again.

## Change the Access Rights of USB Devices

If the Arduino or similar device is really old or vintage, it might use an MAX232 chip.

When an USB device is attached to a linux computer, the **udev** device manager takes care of it.

To check which USB devices are attached, use *lsusb*.

Arduino Nano and its clones uses USB-TTL converter chip, common chips are:
* The original FTDI: Future Technology Devices International, Ltd FT232 USB-Serial (UART) IC
* QinHeng Electronics HL-340 USB-Serial adapter
* Prolific Technology, Inc. PL2303 Serial Port
* CP2102 USB 2.0 to TTL Module Serial Converter
* and many more, and in variants

with *lsusb* these might be listed as:
```
  Bus 002 Device 017: ID 0403:6001 Future Technology Devices International, Ltd FT232 USB-Serial (UART) IC
  Bus 002 Device 011: ID 1a86:7523 QinHeng Electronics HL-340 USB-Serial adapter
  Bus 002 Device 014: ID 067b:2303 Prolific Technology, Inc. PL2303 Serial Port
```

Important is the *ID*, the first part is the Vendor, the second part his Product.

Next, the udev rules file is made:
* /etc/udev/rules.d/10-local.rules

sample content of .rules file:
~~~~
# ID 1a86:7523 QinHeng Electronics HL-340 USB-Serial adapter
SUBSYSTEM=="tty", ATTRS{idVendor}=="1a86", ATTRS{idProduct}=="7523", MODE="0660", GROUP="dialout"
# ID 067b:2303 Prolific Technology, Inc. PL2303 Serial Port
SUBSYSTEM=="tty", ATTRS{idVendor}=="067b", ATTRS{idProduct}=="2303", MODE="0660", GROUP="dialout"
# Future Technology Devices International, Ltd FT232 USB-Serial (UART) IC
SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", MODE="0660", GROUP="dialout"
SUBSYSTEM=="tty" ,ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6010", MODE="0660", GROUP="dialout"
SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6014", MODE="0660", GROUP="dialout"
# Arduino Uno
SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0043", MODE="0660", GROUP="dialout"

#SUBSYSTEM=="usb"

# Atmel Corp.JTAG ICE mkII
SUBSYSTEM=="usb", ATTR{idVendor}=="03eb", ATTR{idProduct}=="2103", MODE="660", GROUP="dialout"

# Atmel Corp. AVRISP mkII
SUBSYSTEM=="usb", ATTR{idVendor}=="03eb", ATTR{idProduct}=="2104", MODE="660", GROUP="dialout"

# Atmel Corp. Dragon
SUBSYSTEM=="usb", ATTR{idVendor}=="03eb", ATTR{idProduct}=="2107", MODE="660", GROUP="dialout"

~~~~
The naming of the file follows some arcane patterns, starting with a number, some description, and .rules at the end.

The layout is always similar:
* SUBSYSTEM - condition for what subsystem, here "tty" or "usb"
* ATTRS{idVendor} - condition for the vendor, found with lsusb
* ATRRS{idProduct} - condition for the product, found with lsusb
* MODE - settings of the mode, "0660" means that root and the group have read and write access
* GROUP - settings of the group, here "dialout". 

The Arduino Uno uses an extra Atmega chip with usb as an USB to TTL converter.

Additionally, the listing above includes some programmers to directly program Atmegas. 

After saving the .rules file, the rules needs to be reloaded with either
```
  udevadm control --reload-rules
```
or
```
  udevcontrol --reload-rules
```
  
If the device was plugged in before, it needs to be reattached.

## testing the bootloader

I assume the Arduino comes with an arduino 'compatible' bootloader. Using this saves the setup and cabling of an extra programmer.  

Install avrdude:
```
  apt-get install avrdude
```
With a user in the right group (dialout), set up udev rules and freshly attached Arduino, look for it:
```
  ls -l /dev/tty*
```

* */dev/tty* without a number is the terminal for the current process. try *echo hello > /dev/tty*
* */dev/ttyn* with n a number, are virtual console on the computer. tty0 is again the actual console, tty1 and up are accessable with CTRL-ALT-F1 and up
* most USB to TTL converter will be listed as */dev/ttyUSBn* - the one with the highest number n should be the attached Arduino. Often, there should one be one /tty/devUSB0
* some USB to TTL converter might be listed as */dev/ttyACMn* - Arduino Uno might be listed as /dev/ttyACM0
* old serial ports will be listed as */dev/ttySn* - max232 converters will be here. To find real existing serial ports, do *cat /proc/tty/driver/serial* as root 

For a Arduino nano, /dev/ttyUSB0 is the most probable candidate.

Next is trying to get a connection between Avrdude and the Arduino
```
  avrdude -P /dev/ttyUSB0 -c arduino -b 57600 -p m328p
```

The parameters are:
* -p m328p - expect an Atmega328p controller. Might be Atmega168 or something else, depending on clone and board. As we are probing, this is not important
* -P /dev/ttyUSB0 - the Arduino should be here
* -c arduino - using the Arduino bootloader
* -b 57600 - the baud rate of the bootloader. Older arduino bootloaders use 19200

Errors:
* *ioctl("TIOCMGET"): Invalid argument* - wrong port
* *avrdude: stk500_recv(): programmer is not responding* - wrong baud rate or missing boot loader
* *avrdude: ser_open(): can't open device "/dev/ttyUSBn": Permission denied* - wrong udev rules or user is not in correct group

On success, the result gives the controller:
* *avrdude: Device signature = 0x1e950f (probably m328p)* - m328p is the controller

## writing a program

Install compiler and libraries:
  apt-get install gcc-avr avr-libc
  
The "Hello World" of microcontrollers is a blinking LED.

Most Arduinos come with a LED attached to Pin 5 of Port B, this is Digital Pin 13 on Arduino Nano.

Simple program:
```c
// filename: main.c
// include avr-lib
#include <avr/io.h>
// define speed of controller, 16Mhz
#define F_CPU 16000000UL
// for some pause
#include <util/delay.h>

// c expects the main function to return a int value, 
int main(void) {
	// set Pin 5 of Port B as output
	DDRB |= (1<<PB5);
	// endless loop
	while(1) {
		// set Pin 5 of Port B on high level
		PORTB |= (1<<PB5);		
		// wait a bit
		_delay_ms(250);
		// set to low level
		PORTB &= ~(1<<PB5);
		// wait again
		_delay_ms(250);	
	}
	// to get no compile warning
	return 0;
}
```

compiling is done in two steps:
```
  avr-gcc -Wall -g -Os -mmcu=atmega328p -o main.bin main.c
```
makes a *main.bin* file

```
  avr-objcopy -j .text -j .data -O ihex main.bin main.hex
```
makes a *main.hex file

the .hex file can written to the arduino with:
```
  avrdude -p m328p -b 57600 -c arduino -U flash:w:main.hex:i -F -P /dev/ttyUSB0
```

The .bin file can be analyzed:
```
  avr-size -C main.bin
```
gives the sizes. 

to get the assembler code:
```
  avr-objdump -d -S main.bin > main.asm
```

to get even more information:
```
  avr-gcc -fverbose-asm -save-temps -g -Os -mmcu=atmega328p -o main.bin main.c
```

## See Also..

tbd
* https://github.com/Optiboot/optiboot - Arduino bootloader

# Need an IDE? How to set up eclipse with avr-gcc

The way to go is set up eclipse for c/c++ and install the avr-eclipse plugin

eclipse avr plugin can be found here:
* http://avr-eclipse.sourceforge.net

Unfortunatly it only supports eclipse indigo and some earlier versions. It will not work with the latest eclipse version.

The correct version can be obtained here:
* https://www.eclipse.org/downloads/packages/release/indigo/sr2

Unfortunatly this version of eclipse will not start with latest java versions.

Under debian etc the java version might be changed with:
```
sudo update-alternatives --config java
```

When eclipse is running, the url for the plugin is: http://avr-eclipse.sourceforge.net/updatesite 

