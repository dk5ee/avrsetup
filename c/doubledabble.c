/*
 ============================================================================
 Name        : doubledabble.c
 ============================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>

void dd_uint8_1(uint8_t data) {
	uint8_t u0 = 0; //units
	uint8_t u1 = 0; //tens
	uint8_t u2 = 0; //hundreds
	uint8_t counter = 0;
	for (counter = 0; counter < 8; counter++) {
		if (u0 > 4)
			u0 += 3;
		if (u1 > 4)
			u1 += 3;
		//shift hundreds
		if (u2)
			u2 <<= 1;
		//shift tens
		if (u1)
			u1 <<= 1;
		//carry bit..
		if (u1 & 0x10) {
			u1 &= 0x0f;
			u2++;
		}
		//shift units
		if (u0)
			u0 <<= 1;
		//carry bit..
		if (u0 & 0x10) {
			u0 &= 0x0f;
			u1++;
		}
		if ((data << counter) & 128)
			u0++;
	}
	printf("%d%d%d ", u2, u1, u0);
}
void dd_uint8_2(uint8_t data) {
	uint8_t u[] = { 0, 0, 0 }; //units
	uint8_t counter = 8;
	uint8_t c2 = 0;
	do {
		for (c2 = 0; c2 < 3; c2++) {
			if (u[c2] > 4)
				u[c2] += 3;
		}
		for (c2 = 0; c2 < 3; c2++) {
			uint8_t temp = u[2 - c2];
			temp <<= 1;
			if ((c2 > 0) && temp & 0x10) {
				temp &= 0x0f;
				u[3 - c2]++;
			}
			u[2 - c2] = temp;
		}
		counter--;
		if ((data >> counter) & 1)
			u[0]++;

	} while (counter > 0);
	printf("%d%d%d\n", u[2], u[1], u[0]);
}
void dd_uint16(uint16_t data) {
	uint8_t u[] = { 0, 0, 0, 0, 0 };
	uint8_t counter = 16;
	uint8_t c2 = 0;
	do {
		//first: dabble, add 3 if value is 5 or greater
		for (c2 = 0; c2 < 4; c2++) {
			if (u[c2] > 4)
				u[c2] += 3;
		}
		//second: double, shift all values
		for (c2 = 0; c2 < 5; c2++) {
			uint8_t temp = u[4 - c2];
			temp <<= 1;
			if ((c2 > 0) && temp & 0x10) {
				temp &= 0x0f;
				u[5 - c2]++;
			}
			u[4 - c2] = temp;
			/*
			 //same code without temp var:
			 u[4-c2]<<= 1;
			 if((c2>0)&& u[4-c2] & 0x10) {
			 u[4-c2] &= 0x0f;
			 u[5-c2]++;
			 }

			 */
		}
		counter--;
		if ((data >> counter) & 1)
			u[0]++;

	} while (counter > 0);
	printf("%d%d%d%d%d ", u[4], u[3], u[2], u[1], u[0]);
}
//second variant: using two nibbles..
void dd_uint16_2(uint16_t data) {
	uint8_t u[] = { 0, 0, 0 };
	uint8_t counter = 16;
	uint8_t c2 = 0;
	do {
		//first: dabble aka add 3 if value is 5 or greater
		for (c2 = 0; c2 < 2; c2++) {
			//low nibble
			if ((u[c2] & 0x0f) >= 5)
				u[c2] += 3;
			//high nibble
			if (u[c2] >= (5 << 4))
				u[c2] += (3 << 4);
		}
		//second: double, shift all values
		for (c2 = 0; c2 < 3; c2++) {
			uint8_t temp = u[2 - c2];
			if ((temp & 128) && (c2 > 0)) {
				u[3 - c2]++; //carry bit
			}
			temp <<= 1;
			u[2 - c2] = temp;
			/*
			 //same code without temp var:
			 if ((u[2-c2]&128)&&(c2>0)) {
			 u[3-c2]++; //carry bit
			 }
			 u[2-c2]<<= 1;
			 */
		}
		counter--;
		if ((data >> counter) & 1)
			u[0]++;

	} while (counter > 0);
	printf("%d%d%d%d%d ", u[2] & 0x0f, u[1] >> 4, u[1] & 0x0f, u[0] >> 4, u[0]
			& 0x0f);
}
void dd_uint32_2(uint32_t data) {
	uint8_t u[] = { 0, 0, 0, 0, 0 };
	uint8_t counter = 32;
	uint8_t c2 = 0;
	do {
		//first: dabble aka add 3 if value is 5 or greater
		for (c2 = 0; c2 < 5; c2++) {
			//low nibble
			if ((u[c2] & 0x0f) >= 5)
				u[c2] += 3;
			//high nibble
			if (u[c2] >= (5 << 4))
				u[c2] += (3 << 4);
		}
		//second: double, shift all values
		for (c2 = 0; c2 < 5; c2++) {
			uint8_t temp = u[4 - c2];
			if ((c2 > 0) && (temp & 128)) {
				u[5 - c2]++; //carry bit
			}
			temp <<= 1;
			u[4 - c2] = temp;
			/*
			 //same code without temp var:

			 if ((c2 > 0)&&(u[4 - c2] & 128)) {
			 u[5 - c2]++; //carry bit
			 }
			 u[4 - c2] <<= 1;

			 */
		}
		counter--;
		if ((data >> counter) & 1)
			u[0]++;

	} while (counter > 0);
	printf("%d%d%d%d%d%d%d%d%d%d\n", u[4] >> 4, u[4] & 0x0f, u[3] >> 4, u[3]
			& 0x0f, u[2] >> 4, u[2] & 0x0f, u[1] >> 4, u[1] & 0x0f, u[0] >> 4,
			u[0] & 0x0f);
}

int main(void) {
	int a = 0;
	for (a = 0; a <= 255; a++) {
		dd_uint8_1(a);
		//dd_uint8_2(a);
		dd_uint16(a * 100);
		dd_uint32_2(a * 10000000);
	}
	puts("done");  
	return EXIT_SUCCESS;
}
