#include <avr/io.h>
#include <avr/interrupt.h>
// #include <util/delay.h>
#include "usart.h"

//makro to flush sendbuff, simple use with BR;
#define BR usart0_putc('\n'); while(usart0_data_tosend()) {}

int main(void) {
	usart0_init();
	while (1) {
		usart0_putc('x');
		BR;
	}
	return 0;
}
