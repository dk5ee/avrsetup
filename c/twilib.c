/*
 * twilib.c
 *
 *  Created on: Aug 11, 2013
 *      Author: user
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/twi.h>
#include "twilib.h"

volatile uint8_t SLaddress = 0;
volatile uint8_t TWstatus =0;
volatile uint8_t TWreadcount = 0;

#define TWIdatabufsize 5
#define TWIdatabufmask ((1<<TWIdatabufsize)-1)

static uint8_t TWI_ReadBuf[1 << TWIdatabufsize];
static volatile uint8_t TWI_ReadHead = 0;
static volatile uint8_t TWI_ReadTail = 0;
static uint8_t TWI_WriteBuf[1 << TWIdatabufsize];
static volatile uint8_t TWI_WriteHead = 0;
static volatile uint8_t TWI_WriteTail = 0;

void TWIinit() {
	TWI_ReadHead = 0;
	TWI_ReadTail = 0;
	TWI_WriteHead = 0;
	TWI_WriteTail = 0;
	SLaddress=0;
	TWstatus = TWI_STATUS_IDLE;
	TWreadcount = 0;
	TWCR = (1 <<TWIE);
}

uint8_t TWIrbc() {
	return ((TWI_ReadHead - TWI_ReadTail) & TWIdatabufmask);
}
uint8_t TWIwbc() {
	return ((TWI_WriteHead - TWI_WriteTail) & TWIdatabufmask);
}

uint8_t TWIwrite(uint8_t data) {
	if (((TWI_WriteHead - TWI_WriteTail) & TWIdatabufmask)< (1<<TWIdatabufsize)){
	TWI_WriteHead = (TWI_WriteHead + 1) & TWIdatabufmask;
	TWI_WriteBuf[TWI_WriteHead] = data;
	return 1;
} else return 0;
}
uint8_t TWIread() {
	if (((TWI_ReadHead - TWI_ReadTail) & TWIdatabufmask)< (1<<TWIdatabufsize)){
	TWI_ReadTail = (TWI_ReadTail + 1) & TWIdatabufmask;
	return (TWI_ReadBuf[TWI_ReadTail]);
} else return 0;
}
void TWIsetaddress(uint8_t address) {
	SLaddress = address;
}
void TWIsetcounttoread(uint8_t readcount) {
	TWreadcount = readcount;
}
void TWIstart() {
	TWstatus = TWI_STATUS_STARTING;
	TWCR &= ~(1 << TWSTO);
					TWCR |= (1 << TWSTA);
					TWCR |= (1 << TWINT);
							TWCR |= (1 << TWEN);
}
uint8_t TWIstatus() {
	//if (TWCR & (1<<TWINT))
	if (TWstatus==255) {
		if (TWCR & (1<<TWSTO) ) {
			return 255;
		} else {
			//stop bit not set -> done sending stop signal
			return 0;
		}
	} else return TWstatus;
}

/*void myerror(uint8_t value) {

 }*/

ISR(TWI_vect) {
	uint8_t tmpstatus = TW_STATUS;
	switch (tmpstatus) {
	case TW_START:	 // 0x08
	case TW_REP_START:	 // 0x10
		TWstatus = TWI_STATUS_SENDING_A;
		//start sent, send SLaddress
		if ((TWI_WriteHead - TWI_WriteTail) & TWIdatabufmask) {
			SLaddress &= ~(1 << 0); //we got data to write
		} else {
			SLaddress |= (1 << 0); // we want data to read
		}
		TWDR = SLaddress;
		TWCR &= ~(1 << TWSTA);
		TWCR &= ~(1 << TWSTO);
		TWCR |= (1 << TWINT);
		TWCR |= (1 << TWEN);
		break;
	case TW_MT_SLA_ACK:	 // 0x18
		TWstatus = TWI_STATUS_SENDING_FIRST_D;
		TWI_WriteTail = (TWI_WriteTail + 1) & TWIdatabufmask;
		TWDR = (TWI_WriteBuf[TWI_WriteTail]);
		TWCR &= ~(1 << TWSTA);
		TWCR &= ~(1 << TWSTO);
		TWCR |= (1 << TWINT);
		TWCR |= (1 << TWEN);
		break;
	case TW_MT_DATA_ACK:	 // 0x28

		if ((TWI_WriteHead - TWI_WriteTail) & TWIdatabufmask) {
			TWstatus = TWI_STATUS_SENDING_NEXT_D;
			//first data has been sent, more to send..
			TWI_WriteTail = (TWI_WriteTail + 1) & TWIdatabufmask;
			TWDR = (TWI_WriteBuf[TWI_WriteTail]);
			TWCR &= ~(1 << TWSTA);
			TWCR &= ~(1 << TWSTO);
		} else {
			if (TWreadcount) {
				TWstatus = TWI_STATUS_RESTARTING;
				//start again for reading
				TWCR &= ~(1 << TWSTO);
				TWCR |= (1 << TWSTA);
			} else {
				//all sent, nothing to read, stop.
				TWstatus = TWI_STATUS_STOPPING;
				TWCR &= ~(1 << TWSTA);
				TWCR |= (1 << TWSTO);
			}
		}
		TWCR |= (1 << TWINT);
		TWCR |= (1 << TWEN);
		break;
	case TW_MR_SLA_ACK:	 // 0x40
		if (TWreadcount) {
			TWstatus = TWI_STATUS_RECEIVING_FIRST_D;
			TWCR &= ~(1 << TWSTO);
		} else {
			TWstatus = TWI_STATUS_STOPPING;
			//stop
			TWCR |= (1 << TWSTO);
		}
		TWCR &= ~(1 << TWSTA);
		TWCR |= (1 << TWINT);
		TWCR |= (1 << TWEA);
		TWCR |= (1 << TWEN);
		break;
	case TW_MR_DATA_ACK:	 // 0x50

		TWreadcount--;
		TWI_ReadHead = (TWI_ReadHead + 1) & TWIdatabufmask;
		TWI_ReadBuf[TWI_ReadHead] = TWDR;
		if (TWreadcount) {
			TWstatus = TWI_STATUS_RECEIVING_NEXT_D;
			//get next data
			TWCR &= ~(1 << TWSTO);
		} else {
			TWstatus = TWI_STATUS_STOPPING;
			//stop
			TWCR |= (1 << TWSTO);
		}
		TWCR &= ~(1 << TWSTA);
		TWCR |= (1 << TWINT);
		TWCR |= (1 << TWEA);
		TWCR |= (1 << TWEN);
		break;

	default:
		//myerror(tmpstatus ); //send stop in any other occassion
		TWstatus = TWI_STATUS_ERROR;
		TWCR |= (1 << TWSTO);
		TWCR |= (1 << TWINT);
		TWCR |= (1 << TWEA);
		TWCR |= (1 << TWEN);
		break;
	}
}

/*
 #define TWCR    _SFR_IO8(0x36)
 #define TWIE    0
 #define TWEN    2
 #define TWWC    3
 #define TWSTO   4
 #define TWSTA   5
 #define TWEA    6
 #define TWINT   7

 #define TWBR    _SFR_IO8(0x00)

 #define TWSR    _SFR_IO8(0x01)
 #define TWPS0   0
 #define TWPS1   1
 #define TWS3    3
 #define TWS4    4
 #define TWS5    5
 #define TWS6    6
 #define TWS7    7

 #define TWAR    _SFR_IO8(0x02)
 #define TWGCE   0
 #define TWA0    1
 #define TWA1    2
 #define TWA2    3
 #define TWA3    4
 #define TWA4    5
 #define TWA5    6
 #define TWA6    7

 #define TWDR    _SFR_IO8(0x03)

 *
 */

