
#include <stdio.h>
#define uint16_t unsigned short int
#define uint8_t unsigned char
#define NullZeichen 48;

const unsigned int TEST[]  = { 10, 100, 1000, 10000 };

volatile uint8_t Ziffer;

void dumpchar(uint8_t Zeichen){
  //putchar(Ziffer);
  Ziffer = Zeichen;
  }
 
void outint( uint16_t val )
{
  
  uint8_t d, i;
  uint8_t zero;
  uint16_t uval = val;

  zero = 1;
  i = 4;
  do{
    i--;
    for( d = '0'; uval >= TEST[i]; uval -= TEST[i] ){
      d++;
      zero = 0;
    }
    if( zero == 0 )
      dumpchar( d );
  }while( i );

  dumpchar( (uint8_t)uval + '0' );
}
 
void dumpint(uint16_t wert) {
uint16_t laufend=wert; //diese variable wird vermutlich vom compiler wegoptimiert
uint8_t merker=NullZeichen;
uint8_t erstes=0; // um f�hrende nullen zu eliminieren
while (laufend>=10000) {
	laufend-=10000;
	merker++;
	erstes=1;
}
if (erstes) { dumpchar(merker);}
merker=NullZeichen;
while (laufend>=1000) {
	laufend-=1000;
	merker++;
	erstes=1;
}
if (erstes) { dumpchar(merker);}
merker=NullZeichen;
while (laufend>=100) {
	laufend-=100;
	merker++;
	erstes=1;
}
if (erstes) { dumpchar(merker);}
merker=NullZeichen;
while (laufend>=10) {
	laufend-=10;
	merker++;
	erstes=1;
}
if (erstes) { dumpchar(merker);}
//auf jeden fall letzte ziffer auspucken
dumpchar(laufend);
}

 
void dumpint4(uint16_t wert) {
uint16_t laufend=wert;  
uint8_t merker=0;
if (laufend >= 40000) {
	laufend-= 40000;
	merker+=4;
	
   } 
if (laufend >= 20000) {
	laufend-=20000;
	merker+=2;
	
   } 
if (laufend >= 10000) {
	laufend-=10000;
	merker+=1;
   } 
uint8_t erstes= merker;
if (erstes > 0) 
  { dumpchar(merker+ NullZeichen);}
merker=0;
if (laufend >= 8000) {
    laufend-=8000;
	merker+=8;
	 } 
if (laufend >= 4000) {
	laufend-=4000;
	merker+=4;
	 } 
if (laufend >= 2000) {
	laufend-=2000;
	merker+=2;
	 } 
if (laufend >= 1000) {
	laufend-=1000;
	merker+=1;
	 } 
erstes+=merker;
if (erstes > 0) 
  { dumpchar(merker+ NullZeichen);}
merker=0;
if (laufend >= 800) {
    laufend-=800;
	merker+=8;
 } 
if (laufend >= 400) {
	laufend-=400;
	merker+=4;
 } 
if (laufend >= 200) {
	laufend-=200;
	merker+=2;
	 } 
if (laufend >= 100) {
	laufend-=100;
	merker+=1;
 } 
erstes+=merker;
if (erstes > 0) 
  { dumpchar(merker+ NullZeichen);}
merker=0;
if (laufend >= 80) {
    laufend-=80;
	merker+=8;
	} 
if (laufend >= 40) {
	laufend-=40;
	merker+=4;
	} 
if (laufend >= 20) {
	laufend-=20;
	merker+=2;
	} 
if (laufend >= 10) {
	laufend-=10;
	merker+=1;
	} 
erstes+=merker;
if (erstes > 0) 
  { dumpchar(merker+ NullZeichen);}
dumpchar(laufend + NullZeichen);
}


void dumpint3(uint16_t Wert) {

uint8_t merker;
uint8_t erstes=1; // um f�hrende nullen zu eliminieren

  void check( uint16_t Vergleich, uint8_t i){
    merker=NullZeichen;
    do{
      if (Wert >= Vergleich) {
	    Wert-=Vergleich;
	    merker+=i;
	    erstes=0;} 
	  Vergleich >>= 1;
      if (erstes==0)  
        {dumpchar(merker);}
      i >>= 1;
	}while(i);}

check(40000,4);   
check( 8000,3);   
check(  800,3);   
check(   80,3);   

Wert+=NullZeichen;
dumpchar((uint8_t) Wert); 
}

int main(int argc, char** argv)

{ unsigned short int Wert,i;
  
  for (i = 0 ; i < 1000;i++) {
    for ( Wert = 56789; Wert<56790; Wert++){
	  Ziffer = 0;
      dumpint(Wert);
      dumpint2(Wert);
      dumpint3(Wert);
      outint(Wert);}
   /* printf("%d \n",i);*/ }
  return 0;
}
