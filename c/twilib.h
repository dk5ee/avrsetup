/*
 * twilib.h
 *
 *  Created on: Aug 11, 2013
 *      Author: user
 */

#ifndef TWILIB_H_
#define TWILIB_H_

#define TWI_STATUS_IDLE 0
#define TWI_STATUS_STARTING 1
#define TWI_STATUS_SENDING_A 2
#define TWI_STATUS_SENDING_FIRST_D 3
#define TWI_STATUS_SENDING_NEXT_D 4
#define TWI_STATUS_RESTARTING 5
#define TWI_STATUS_RECEIVING_FIRST_D 6
#define TWI_STATUS_RECEIVING_NEXT_D 7
#define TWI_STATUS_ERROR 8
#define TWI_STATUS_STOPPING 255


void TWIinit();
uint8_t TWIwrite(uint8_t data);
uint8_t TWIread();
uint8_t TWIrbc();
uint8_t TWIwbc();
void TWIsetaddress(uint8_t address);
void TWIsetcounttoread (uint8_t readcount);
void TWIstart();
uint8_t TWIstatus();

#endif /* TWILIB_H_ */
