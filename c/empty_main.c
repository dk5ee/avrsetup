/*
 * main.c
 */
#include <avr/io.h>
#include <avr/interrupt.h>


int main(void) {
	while (1) {

	}
}

/* Interrupt vectors */

ISR ( INT0_vect ) {
}
ISR ( INT1_vect ) {
}
ISR ( TIMER2_COMP_vect ) {
}
ISR ( TIMER2_OVF_vect ) {
}
ISR ( TIMER1_CAPT_vect ) {
}
ISR ( TIMER1_COMPA_vect ) {
}
ISR ( TIMER1_COMPB_vect ) {
}
ISR ( TIMER1_OVF_vect ) {
}
ISR ( TIMER0_OVF_vect ) {
}
ISR ( SPI_STC_vect ) {
}
ISR ( USART_RXC_vect ) {
}
ISR ( USART_UDRE_vect ) {
}
ISR ( USART_TXC_vect ) {
}
ISR ( ADC_vect ) {
}
ISR ( EE_RDY_vect ) {
}
ISR ( ANA_COMP_vect ) {
}
ISR ( TWI_vect ) {
}
ISR ( SPM_RDY_vect ) {
}
