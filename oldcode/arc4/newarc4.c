/*
 * newarc4.c
 *
 */
#include "newarc4.h"


uint8_t count_k = 0;
uint8_t count_l = 0;
uint8_t bittiefe=8;
uint8_t bitsleft=0;
uint8_t bitsremembered=0;
volatile uint8_t S[256];



uint8_t getS(uint8_t num) {

	return ((S[num & (0xff >> (8-bittiefe))]) & (0xff >> (8-bittiefe)));

}

void setS(uint8_t num, uint8_t val) {

	S[num  & (0xff >> (8-bittiefe))]=val;

}

void clearS(){
	uint8_t i=0;
		do {
			setS(i,i);
			i++;
		}while (i);
}

void swapS(uint8_t a, uint8_t b) {
	uint8_t dummy = getS(a);
	setS(a, getS(b));
	setS(b, dummy);
}
void initS(char *key) {

	//clear all..
	count_k = 0;
	count_l = 0;
	//uint8_t count_key= 0; //
 bitsleft=0;
 bitsremembered=0;
	clearS();
	/*
	uint8_t i=0;
	uint8_t j = 0;

	i=0;
	do {
		j = (j + getS(i) + *(count_key+key));
				count_key++;
				if (*(count_key+key) == 0) count_key=0;
		swapS(i,j);
		i++;
	}while (i);
*/
}

uint8_t nextS() {


 count_k++;
 count_l+=getS(count_k);
 swapS(count_l,count_k);
 return (getS(getS(count_l)+getS(count_k)));


}

uint8_t next8bits(){
	if (bittiefe==8) return nextS();
	uint8_t data=0;
	int8_t neededbits=8;
	if (bitsleft>0){
		neededbits-=bitsleft;
		data=bitsremembered;
		bitsleft=0;
	}

	while (neededbits>=bittiefe){
		uint8_t nextbits=nextS();
		data <<=bittiefe;
		data += nextbits;
		neededbits-=bittiefe;
	}
	if (neededbits==0) {
		bitsremembered=0;
		bitsleft=0;
	} else {
		uint8_t nextbits=nextS();
		data <<=neededbits;
		data += (nextbits >> (bittiefe-neededbits));
		bitsleft= (bittiefe-neededbits);
		bitsremembered=nextbits;// (nextbits & (0xFF >> ((8-bittiefe)+neededbits)));
	}
	return data;
}


void firstS(){
uint8_t i=0;
do {
	i++;
	nextS();
} while (i);
}
void setbits(uint8_t bits){
	if (bits<1) bits=1;
	if (bits>8) bits=8;
	bittiefe=bits;
}
