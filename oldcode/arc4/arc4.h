/*
 * arc4.h
 *
 */

#ifndef ARC4_H_
#define ARC4_H_

#include <stdint.h>

// if doitinline is defined => inline get/set functions everywhere
#define doitinline
// if doit4bit is defined => 4bit arc4 instead of 8bit
#define doit4bit
// if compressarray is defined => two 4bit values are packed in one 8bit
//#define compressarray



#ifdef doit4bit
#ifdef compressarray
volatile uint8_t S[8]; //holding two nibbles each
#else
volatile uint8_t S[16];
#endif
#else
volatile uint8_t S[256];
#endif


void initS(char *key);
void firstS();
uint8_t nextS() ;
#endif /* ARC4_H_ */
