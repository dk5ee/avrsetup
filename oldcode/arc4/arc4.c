/*
 * arc4.c
 *
 */
#include "arc4.h"


uint8_t count_k = 0;
uint8_t count_l = 0;

#ifdef doitinline
inline
#endif
uint8_t getS(uint8_t num) {
#ifdef doit4bit
#ifdef compressarray
	uint8_t t1 = S[((num >> 1) & 7)];
	if (num & 1) { //higher bits
		t1 >>= 4;
	} else { //lower bits
		t1 &= 0x0F;
	}
	return t1;
#else
	return S[num & 0x0F];
#endif
#else
	return S[num];
#endif
}
#ifdef doitinline
inline
#endif
void setS(uint8_t num, uint8_t val) {
#ifdef doit4bit
#ifdef compressarray
	uint8_t t1 = S[((num >> 1) & 7)];
	if (num & 1) {
		t1 &= 0x0F; //delete higher bits
		t1 |= (val << 4);
	} else {
		t1 &= 0xF0; //delete lower bits;
		t1 |= (val & 0x0F);
	}
	//writeback
	S[((num >> 1) & 7)] = t1;
#else
	S[num & 0x0F]=val &0x0F;
#endif
#else
	S[num]=val;
#endif
}
void swapS(uint8_t a, uint8_t b) {
	uint8_t dummy = getS(a);
	setS(a, getS(b));
	setS(b, dummy);
}
void initS(char *key) {

	//clear all..
	count_k = 0;
	count_l = 0;
	uint8_t count_key= 0; //
#ifdef doit4bit
	uint8_t i =0;
	for (i = 0; i < 16; i++)
		setS(i, i);
#else
	uint8_t i=0;
	do {
		setS(i,i);
		i++;
	}while (i);
#endif
	uint8_t j = 0;
#ifdef doit4bit
	for (i = 0; i < 16; i++) {
		j = (j + getS(i) + *(count_key+key));
		count_key++;
		if (*(count_key+key) == 0) count_key=0;
		swapS(i, j);
	}
#else
	i=0;
	do {
		j = (j + getS(i) + *(count_key+key));
				count_key++;
				if (*(count_key+key) == 0) count_key=0;
		swapS(i,j);
		i++;
	}while (i);
#endif
}
#ifdef doit4bit
uint8_t nexthalfS() {

	 count_k++;
	 count_l+=getS(count_k);
	 swapS(count_l,count_k);
	 return (getS(getS(count_l)+getS(count_k)));

}
uint8_t nextS() {
	return (nexthalfS() << 4) + nexthalfS();
}
#else
uint8_t nextS() {


 count_k++;
 count_l+=getS(count_k);
 swapS(count_l,count_k);
 return (getS(getS(count_l)+getS(count_k)));


}

#endif

void firstS(){
uint8_t i=0;
do {
	i++;
	nextS();
} while (i);
}

