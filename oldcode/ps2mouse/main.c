/*
 * main.c
 *
 */
#include "ps2lib.h"

int main(void) {
	ps2boot();
	for (;;) {
		//here: otherstuff
		uint8_t flag=ps2flags();
		if (flag !=0){
			if (flag & 0b00000111) {
				//	a mousebottom was pressed
			}
			if (flag & (1 << 4)) {
				//movement in x axis occured	int16_t value in ps2x();
			}
			if (flag & (1 << 5)) {
				//movement in y axis occured	int16_t value in ps2y();
			}
			if (flag & (1 << 6)) {
				//movement in z axis occured 	int16_t value in ps2z();
			}
			ps2resetflags();
			// instead of reset only the flags: maybe reset values with ps2reset();
		}

		//here: otherstuff
	}
}
