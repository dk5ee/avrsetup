/*
 * ps2lib.c
 *
 */

#include "ps2lib.h"

/*
 * pd3/ int1: clockpin	(arduino: 3)
 * pd4 datapin			(arduino: 4)
 * das ganze spiel geht über external int1
 */

//ein paar defines für die pins:
#define ps2dataDDR DDRD
#define ps2dataPORT PORTD
#define ps2dataPIN PIND
#define ps2dataPINNR PD4
#define ps2clockPIN PIND
#define ps2clockDDR DDRD
#define ps2clockPORT PORTD
#define ps2clockPINNR PD3
//der ISR vector:
#define ps2int_vect INT1_vect

//ein paar makros...
#define ps2dataIN ps2dataDDR &= ~(1<<ps2dataPINNR)
#define ps2dataOUT ps2dataDDR |= (1<<ps2dataPINNR)
#define ps2dataLOW ps2dataPORT &= ~(1<<ps2dataPINNR)
#define ps2dataHIGH ps2dataPORT |= (1<<ps2dataPINNR)
#define ps2dataISHIGH ((ps2dataPIN & (1<<ps2dataPINNR)) != 0)
#define ps2dataISLOW ((ps2dataPIN & (1<<ps2dataPINNR)) == 0)

#define ps2DH ps2dataIN; ps2dataHIGH
#define ps2DL ps2dataOUT; ps2dataLOW

#define ps2clockIN ps2clockDDR &= ~(1<<ps2clockPINNR)
#define ps2clockOUT ps2clockDDR |= (1<<ps2clockPINNR)
#define ps2clockLOW ps2clockPORT &= ~(1<<ps2clockPINNR)
#define ps2clockHIGH ps2clockPORT |= (1<<ps2clockPINNR)
#define ps2clockISHIGH ((ps2clockPIN & (1<<ps2clockPINNR)) != 0)
#define ps2clockISLOW ((ps2clockPIN & (1<<ps2clockPINNR)) == 0)

#define ps2CH ps2clockIN; ps2clockHIGH
#define ps2CL ps2clockLOW; ps2clockOUT

//External Interrupt Control Register A, früher mit: MCUCR, nun EICRA
//makro um den int1 zu intialisieren: fallende flanke ist gefragt
#define ps2initint EICRA &=~(1<<ISC10); EICRA |= (1<<ISC11)
//enable/dissable, früher: GICR, nun
//todo: ist EIFR löschen hier nötig?
#define ps2enableINT EIMSK |= (1<<INT1);EIFR |= (1 << INTF1)
#define ps2dissableINT EIMSK &= ~(1<<INT1)

//nun laufende variablen für ISR
volatile uint8_t ps2bitcount = 0; // zählt gesendete/empfangen bits mit
volatile uint8_t ps2flag = 0; // 0=idle; 1= recieving now; >1: free running ps2 scroll dings
volatile uint8_t ps2paritycount = 0; //zählt für odd parity mit
volatile uint8_t ps2INdata = 0; //hält das gerade emfangene byte


#define ps2_BUFSIZE 64
static uint8_t ps2_Buf[ps2_BUFSIZE];
static volatile uint8_t ps2_Buf_Head = 0;
static volatile uint8_t ps2_Buf_Tail = 0;
static volatile uint8_t ps2_Buf_Count = 0;
//todo: timeouterror auch verwenden
#define __TIMEOUT 60000
volatile uint8_t timeouterror = 0;
static volatile uint8_t ps2_1 = 0;
static volatile uint8_t ps2_5 = 0;
static volatile int16_t ps2_2 = 0;
static volatile int16_t ps2_3 = 0;
static volatile int16_t ps2_4 = 0;

void waitforclock() {
	uint16_t timeout = __TIMEOUT;
	//first: wait while clock is LOW
	do {
		timeout--;
		if (timeout == 0) {
			timeouterror = 1;
		}
	} while (timeout > 0 && ps2clockISLOW);
	//then: wait while clock is HIGH
	timeout = __TIMEOUT;
	do {
		timeout--;
		if (timeout == 0) {
			timeouterror = 1;
		}
	} while (timeout > 0 && ps2clockISHIGH);
}
/* unused:
 uint8_t ps2isactive() {
 uint16_t timeout = __TIMEOUT;
 while (ps2flag == 1) {
 _delay_us(1);
 timeout--;
 if (timeout == 0) {
 timeouterror = 1;
 return 1;
 }
 }; //warten mit timeout wenn gerade gesendet wird
 return 0;
 }
 */
void ps2run() {
	ps2flag = 2;
}
void ps2init() {
	cli();
	//falls schon andere dinge aktiv
	ps2initint; //vorbereiten..
	//clock+data auf high
	ps2DH;
	ps2CH;
	ps2enableINT;
	//int on falling edge
	sei();
}
uint8_t ps2incount() {
	return ps2_Buf_Count;
}
//ps2get: liefert zeichen aus puffer oder 0 wenn leer.
uint8_t ps2get() {
	if (ps2_Buf_Count > 0) {
		uint8_t tmp;
		tmp = (ps2_Buf_Tail + 1) & (ps2_BUFSIZE - 1);
		ps2_Buf_Count--;
		ps2_Buf_Tail = tmp;
		return (ps2_Buf[tmp]);
	} else
		return 0;
}
void ps2clearbuf() {
	while (ps2incount())
		ps2get();
}
//sends one data, waits for couple of responses
void ps2pollsend(uint8_t data, uint8_t responses) {

	// ps2isactive();

	//dissable int on falling edge
	ps2dissableINT; // wir wollen nicht durch nachstehendes einen interrupt auslösen

	//clock to low - block device
	ps2DH;
	ps2CL;
	//wait 100µs
	_delay_us(100);
	//data to low - request to send
	ps2DL;
	_delay_us(2);
	//clock to high
	ps2CH;
	uint8_t j = 0; //bitcounter..
	uint8_t parity = 0;

	//first: send data
	//startbit..
	do {
	} while (ps2clockISHIGH);
	//8 bits..
	for (j = 0; j < 8; j++) {
		waitforclock();
		if (data & 1) {
			parity++;
			//set data=high
			ps2DH;
		} else {
			//set data=low
			ps2DL;
		}
		data >>= 1; //schieb schieb..
	}
	//parity..
	waitforclock();
	if (parity & 1) {
		//odd parity: set data low now
		ps2DL;

	} else {
		//odd parity: set data high now
		ps2DH;
	}
	//stopbit
	waitforclock();
	//now: stop bit
	//set data high
	ps2DH;
	//ack..
	waitforclock();
	//todo: check: data should be low now


	//then: recieve some data
	uint8_t i = 0;
	for (i = 0; i < responses; i++) {
		waitforclock();
		parity = 0;//reset parity
		//if (ps2dataISLOW)

		//todo: start bit, should be low, now ignored

		uint8_t indata = 0;
		for (j = 0; j < 8; j++) {
			//8 bits..
			waitforclock();
			if (ps2dataISHIGH) {
				parity++;
				indata += (1 << j);
			}
		}
		//one parity..

		waitforclock();
		if (ps2dataISHIGH)
			parity++;
		if (parity & 1) { //ok, daten speichern..
			uint8_t tmp;
			tmp = (ps2_Buf_Head + 1) & (ps2_BUFSIZE - 1);
			ps2_Buf_Head = tmp;
			ps2_Buf[tmp] = indata;
			ps2_Buf_Count++;
			if (ps2_Buf_Count == ps2_BUFSIZE) {
				//fix overflow, remove item from Tail
				ps2_Buf_Tail = (ps2_Buf_Tail + 1) & (ps2_BUFSIZE - 1);
				ps2_Buf_Count--;
			}

		} else { //todo: falsches parity

		}
		//one stop bit
		waitforclock();

		//todo: check for high;


	}
	//enable int on falling edge - ab jetzt übernimmt das ps/2 gerät und die ISR
	ps2enableINT;
}

void ps2boot() {
	ps2init();
	// reset..
	ps2pollsend(0xFF, 3);
	ps2pollsend(0xFF, 3);
	ps2pollsend(0xFF, 3);
	// intellimouse init
	ps2pollsend(0xF3, 1);
	ps2pollsend(200, 1);
	ps2pollsend(0xF3, 1);
	ps2pollsend(100, 1);
	ps2pollsend(0xF3, 1);
	ps2pollsend(80, 1);
	//read version
 	ps2pollsend(0xf2, 2);
 	//fast sample.. 200hz
 	ps2pollsend(0xF3, 1); ps2pollsend(200, 1);
 	//high res: 8counts/mm
 	ps2pollsend(0xe8, 1); ps2pollsend(03, 1);
 	//1:1 scaling
 	ps2pollsend(0xe6, 1);

 	//enable reporting and let ISR take over..
	ps2clearbuf();
 	ps2pollsend(0xF4, 1);
	ps2clearbuf();
	//ps2run: signal ISR to do more stuff
	ps2run();
}


//blick aufs erste empfangene byte werfen..
/*
uint8_t ps2peek() {
	if (ps2_Buf_Count > 0) {
		return (ps2_Buf[(ps2_Buf_Tail + 1) & (ps2_BUFSIZE - 1)]);
	} else
		return 0;
}
*/
//enable automatic mode..

uint16_t ps2x() {
	cli();
	uint16_t wert = ps2_2;
	sei();
	return wert;
}
uint16_t ps2y() {
	cli();
	uint16_t wert = ps2_3;
	sei();
	return wert;
}
uint16_t ps2z() {
	cli();
	uint16_t wert = ps2_4;
	sei();
	return wert;
}
uint8_t ps2b() {
	return (ps2_1 & 0x7);
}
uint8_t ps2flags() {
	return (ps2_5);
}
void ps2resetflags() {
	ps2_5 = 0;
}
void ps2reset() {
	cli();
	ps2_1 = 0;
	ps2_2 = 0;
	ps2_3 = 0;
	ps2_4 = 0;
	ps2_5 = 0;
	sei();
}

ISR(ps2int_vect)
{

	if (ps2bitcount == 0) {
		if (ps2flag == 0)
			ps2flag = 1;
		//wenn data==low gehen daten los, ansonsten rausspringen aus int
		if (ps2dataISHIGH)
			return;
		//ansonsten: init..
		ps2INdata = 0; //in data
		ps2paritycount = 0;//parity
	} else if (ps2bitcount < 9) {
		if (ps2dataISHIGH) {
			ps2paritycount++;
			ps2INdata += (1 << (ps2bitcount - 1));
		}
	} else if (ps2bitcount == 9) {
		if (ps2dataISHIGH)
			ps2paritycount++;
		if (ps2paritycount & 1) { //ok, daten speichern..
			if (ps2flag == 1) {
				uint8_t tmp;
				tmp = (ps2_Buf_Head + 1) & (ps2_BUFSIZE - 1);
				ps2_Buf_Head = tmp;
				ps2_Buf[tmp] = ps2INdata;
				ps2_Buf_Count++;
				if (ps2_Buf_Count == ps2_BUFSIZE) {
					//fix overflow, remove item from Tail
					ps2_Buf_Tail = (ps2_Buf_Tail + 1) & (ps2_BUFSIZE - 1);
					ps2_Buf_Count--;
				}
			} else {
				//magic check byte..
				//first byte: bit3 is set, at least one bit of upper 4 bits not set
				uint8_t data = ps2INdata;
				if ((ps2flag > 4) && (data & (1 << 3)) && ((data & (0xf0))
						!= 0xf0))
					ps2flag = 2; //found first byte!
				if (ps2flag == 2) {
					ps2_1 = data;
					ps2_5 |= (data & 0x07);
				} else if (ps2flag == 3) { //x
					 //if (ps2_1 & (1 << 6))data = 255; // ignore overflow..
					if (ps2_1 & (1 << 4)) {
						ps2_2 += data-256;
					} else {
						ps2_2 += data;
					}
					if (data)
						ps2_5 |= (1 << 4);
				} else if (ps2flag == 4) {
					//if (ps2_1 & (1 << 7))	data = 255; // ignore overflow..
					if (ps2_1 & (1 << 5)) {
						ps2_3 += data-256;
					} else {
						ps2_3 += data;
					}
					if (data)
						ps2_5 |= (1 << 5);
				} else {
					int8_t z = data;
					ps2_4 += z;
					if (data)
						ps2_5 |= (1 << 6);
				}
				ps2flag++;
			}

		} else { //falsches parity
		}

	} else if (ps2bitcount == 10) { //stop bit, muss high sein.
		if (ps2flag == 1)
			ps2flag = 0;
		ps2bitcount = 0;
		return;
	}
	ps2bitcount++;
}
