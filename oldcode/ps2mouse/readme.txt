
Ich präsentiere:

ps2lib

avr-gcc library for atmel avrs and ps2/microsoft intellimouse compatible optical mice.

sorry for the bloated ISR();

what the library does:
-expects said intellimouse compatible device, data and clock line connected to atmega
-initializes mouse to stream mode
-features interrupt service routine to fetch data from ps2 device
pd3/int1	clockpin	(arduino: 3)
pd4 		datapin		(arduino: 4)

optical mouse features:
-updated data up to 200 times/seconds
-x/y accuracy up to 1/8 mm
-three buttons
-rotary encoder (scroll wheel) 

ps2lib.h	-	header file
ps2lib.c	-	source file

usage: example code in main.c

 
avr-size  
   text	   data	    bss	    dec	    hex	filename
   1528	      0	     80	   1608	    648	ps2mouse.elf