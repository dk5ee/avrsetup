/*
 * main.c
 *
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define LEDON PORTB |= 1 << PB5
#define LEDOFF PORTB &= ~(1<< PB5)
#define LEDINIT DDRB |= 1 << PB5

#include "usart.h"
#include "adcint.h"

//makro um daten auszuspucken, flusht auch den sendepuffer vom usart
#define BR usart0_putc('\n'); while(usart0_data_tosend()) {}

void dumpuint(uint16_t wert) {
	uint16_t laufend = wert; //diese variable wird vermutlich vom compiler wegoptimiert
	uint8_t merker = NullZeichen;
	uint8_t erstes = 0; // um führende nullen zu eliminieren
	while (laufend >= 10000) {
		laufend -= 10000;
		merker++;
		erstes = 1;
	}
	if (erstes) {
		usart0_putc((uint8_t) merker + '0');
	}
	merker = NullZeichen;
	while (laufend >= 1000) {
		laufend -= 1000;
		merker++;
		erstes = 1;
	}
	if (erstes) {
		usart0_putc((uint8_t) merker + '0');
	}
	merker = NullZeichen;
	while (laufend >= 100) {
		laufend -= 100;
		merker++;
		erstes = 1;
	}
	if (erstes) {
		usart0_putc((uint8_t) merker + '0');
	}
	merker = NullZeichen;
	while (laufend >= 10) {
		laufend -= 10;
		merker++;
		erstes = 1;
	}
	if (erstes) {
		usart0_putc((uint8_t) merker + '0');
	}
	//auf jeden fall letzte ziffer auspucken
	usart0_putc((uint8_t) laufend + '0');
	BR;
}
void dumpint(int16_t d){
	if (d<0){
		d=0-d;
		usart0_putc('-');
		dumpuint(d);
	} else dumpuint(d);
}
const char hex[] = "0123456789ABCDEF";
void outword(uint16_t w) {
	usart0_putc('0');
	usart0_putc('x');
	usart0_putc(hex[(w >> 12) & 15]);
	usart0_putc(hex[(w >> 8) & 15]);
	usart0_putc(hex[(w >> 4) & 15]);
	usart0_putc(hex[w & 15]);

}

void outbyte(uint8_t a) {
	usart0_putc('0');
	usart0_putc('x');
	usart0_putc(hex[(a >> 4) & 15]);
	usart0_putc(hex[a & 15]);
	BR;
}

void outdings(int16_t d) {
	if (d < 0) {
		usart0_putc('-');
		outword(0 - d);
	}

	else
		outword(d);
}

void outbin(uint8_t a) {
	for (int i = 8; i > 0; i--)
		if (a >> (i - 1) & 1)
			usart0_putc('1');
		else
			usart0_putc('0');
	BR;

}

void myinit() {
	adcinit();
	usart0_init();
	//usart0_putc('x');BR;

	adcstart();

}

void blinki(uint8_t b) {
	LEDINIT;
	cli();
	_delay_ms(100);
	uint8_t i = 0;
	for (i = 0; i < b; i++) {
		LEDON;
		_delay_ms(200);
		LEDOFF;
		_delay_ms(200);
	}
	_delay_ms(300);
	sei();

}

int main(void) {
	myinit();
	//	uint8_t i=0;
	for (;;) {
		//outbin(i);i++;
		//outbyte(myadccounter());
		blinki(1);
		adcstop();
		blinki(2);
		adcstart();
		_delay_ms(2500);
		//outword(adcfield[8]);
	}

}
