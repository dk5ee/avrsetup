/*
 * main.c
 *
 */
#include <avr/io.h>
#include <avr/interrupt.h>

/*
 * 74hc4017
 * pin 1	Q5
 * pin 2	Q1
 * pin 3	Q0
 * pin 4	Q2
 * pin 5	Q6
 * pin 6	Q7
 * pin 7	Q3
 * pin 8	GND
 * pin 9	Q8
 * pin 10	Q4
 * pin 11	Q9
 * pin 12	~Q5-9
 * pin 13	~CP1 - clock on falling edge
 * pin 14	CP0 - clock on rising edge
 * pin 15	MR - Rmaster reset input (active HIGH)
 * pin 16	Vcc
 *
 * Q[0:9] 3, 2, 4, 7, 10, 1, 5, 6, 9, 11 decoded output
 */

#define CLOCKPIN PB1  // cp0, clock on rising edge
#define RESETPIN PB2  // master reset,
#define ENABLEPIN PB3 //on 74hc4017 -> ~cp1
#define DINGPORT PORTB
#define DINGDIR DDRB
#define INITDING DINGPORT |= (1<<CLOCKPIN) |(1<<RESETPIN) |(1<<ENABLEPIN);

#define DINGCLOCKHIGH DINGPORT |= (1<<CLOCKPIN);
#define DINGCLOCKLOW DINGPORT &= ~(1<<CLOCKPIN);
#define DINGENABLE DINGPORT &= ~(1<<ENABLEPIN);
#define DINGDISSABLE DINGPORT |= (1<<ENABLEPIN);
#define DINGSTARTRESET DINGPORT |= (1<<RESETPIN);
#define DINGSTOPRESET DINGPORT &= ~(1<<RESETPIN);


#define DINGCOUNTS 20

volatile uint16_t nextDingValue=0; //holds next value for fastestest preload
volatile uint8_t thisDingCount=0;
volatile uint16_t dingCounters[DINGCOUNTS];

void setDingCounter(uint8_t number,uint16_t value) {
	if (number<DINGCOUNTS) {
		cli();
		dingCounters[number]=value;
		sei();
	}
}
void initDingCounter(){
	DINGDIR |= (1<<CLOCKPIN);
	uint8_t c=0;
	for (c=0;c<20;c++) setDingCounter(c,1000);

	//0 => no prescaler
	TCCR1B |= (0<<CS12) | (0<<CS11) | (0<<CS10);
	//Overflow-Interrupt aktiviert
	TIMSK |= (1<<TOIE1);
	TCNT1=dingCounters[0];
	nextDingValue=dingCounters[1];
	thisDingCount=2;
}
void startDingCounter(){
	TIMSK |= (1 << TOIE1);

	INITDING;
	DINGSTARTRESET;
	 DINGENABLE;
	 DINGCLOCKLOW;
	 DINGSTOPRESET;
	//
	DINGDIR |= (1<<ENABLEPIN);
	//
	sei(); // Enable global interrupts
}
void stopDingCounter(){
	TIMSK &= ~(1 << TOIE1);
}

int main(void) {
	initDingCounter();
	startDingCounter();
	while (1) {

	}
}

/* Interrupt vectors */

ISR ( INT0_vect ) {
}
ISR ( INT1_vect ) {
}
ISR ( TIMER2_COMP_vect ) {
}
ISR ( TIMER2_OVF_vect ) {
}
ISR ( TIMER1_CAPT_vect ) {
}
ISR ( TIMER1_COMPA_vect ) {
}
ISR ( TIMER1_COMPB_vect ) {
}
ISR ( TIMER1_OVF_vect ) {
	// Vorfüllen des Timers
	TCNT1=nextDingValue;

	PORTB ^= (1 << 0); // Toggle a LED at pb0

	 //preload next value
	nextDingValue=dingCounters[thisDingCount];
	thisDingCount++;
	if (thisDingCount>DINGCOUNTS) thisDingCount=0;

}
ISR ( TIMER0_OVF_vect ) {
}
ISR ( SPI_STC_vect ) {
}
ISR ( USART_RXC_vect ) {
}
ISR ( USART_UDRE_vect ) {
}
ISR ( USART_TXC_vect ) {
}
ISR ( ADC_vect ) {
}
ISR ( EE_RDY_vect ) {
}
ISR ( ANA_COMP_vect ) {
}
ISR ( TWI_vect ) {
}
ISR ( SPM_RDY_vect ) {
}
