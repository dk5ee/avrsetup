#include <avr/io.h>

#define F_CPU 16000000UL
#include <util/delay.h>

int main(void) {
	DDRB |= (1<<PB5);
	while(1) {
		PORTB |= (1<<PB5);			
		_delay_ms(250);
		PORTB &= ~(1<<PB5);
		_delay_ms(250);	
	}
	return 0;
}

/*
avr-gcc -Wall -g -Os -mmcu=atmega328p -o main.bin main.c
avr-gcc -fverbose-asm -save-temps -g -Os -mmcu=atmega328p -o main.bin main.c
 
avr-size -C main.bin
avr-objcopy -j .text -j .data -O ihex main.bin main.hex
avr-objdump -d -S main.bin > main.asm
avrdude -p m328p -b 57600 -c arduino -U flash:w:main.hex:i -F -P /dev/ttyUSB3
*/
