/*
 * display_basic.c
 *
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include "display_setting.h"

void display_write_cmd(uint16_t command, uint16_t data) {
	DISPLAY_CS_LOW
	cli();
	DISPLAY_WRITE_COMMAND(command);
	DISPLAY_WRITE_DATA(data);
	sei();
	DISPLAY_CS_HIGH
}

void display_init() {
	DISPLAY_CONTROL_INIT
	;
	DISPLAY_SET_WRITE
	;
	DISPLAY_CS_HIGH
	;
	DISPLAY_READ_HIGH
	;
	DISPLAY_WRITE_HIGH
	;
	DISPLAY_RESET_HIGH
	;
	_delay_ms(50);
	DISPLAY_RESET_LOW
	;
	_delay_ms(100);
	DISPLAY_RESET_HIGH
	;
	_delay_ms(100);
	//magic?
	/*//magic one_ rotation
	 display_write_cmd(0x01,(1 << 8) );
	 display_write_cmd(0x60,0x2700);
	 display_write_cmd(0x03,0x1030);
	 //magic two_ address
	 display_write_cmd(DISPLAY_PARAM_X, 0);
	 display_write_cmd(DISPLAY_PARAM_Y, 0);
	 display_write_cmd(DISPLAY_PARAM_A, 0);
	 display_write_cmd(DISPLAY_PARAM_C, 0);
	 display_write_cmd(DISPLAY_PARAM_B, DISPLAY_WIDTH);
	 display_write_cmd(DISPLAY_PARAM_D, DISPLAY_HEIGHT);*/

	//todo: fix that!
	uint16_t a, d;
	for (uint8_t i = 0; i < sizeof(DISPLAY_INIT_regValues) / 4; i++) {
		a = pgm_read_word(DISPLAY_INIT_regValues + i * 2);
		d = pgm_read_word(DISPLAY_INIT_regValues + i * 2 + 1);
		// a = pgm_read_word(&_regValues[i++]);
		//d = pgm_read_word(&_regValues[i++]);

		display_write_cmd(a, d);
		//Serial.print("addr: "); Serial.print(a);
		//Serial.print(" data: "); Serial.println(d, HEX);

	}
}

void display_write_cmd_N(uint16_t command, int8_t count, uint8_t * data) {
	DISPLAY_CS_LOW
	DISPLAY_WRITE_COMMAND(command);
	DISPLAY_RS_HIGH
	while (count-- > 0) {
		uint8_t ubyte = *data++;
		DISPLAY_WRITE_UINT8(ubyte);
	}
	DISPLAY_CS_HIGH
}
void display_write_cmd_4bytes(uint16_t command, uint8_t data0, uint8_t data1,
		uint8_t data2, uint8_t data3) {
	DISPLAY_CS_LOW
	;
	DISPLAY_WRITE_COMMAND(command);
	DISPLAY_RS_HIGH
	;
	DISPLAY_WRITE_UINT8(data0);
	DISPLAY_WRITE_UINT8(data1);
	DISPLAY_WRITE_UINT8(data2);
	DISPLAY_WRITE_UINT8(data3);
	DISPLAY_CS_HIGH
	;
}
uint16_t display_read_register(uint16_t myregister) {
	uint16_t data = 0;
	DISPLAY_CS_LOW
	;
	DISPLAY_WRITE_COMMAND(myregister);
	DISPLAY_SET_READ
	;
	DISPLAY_RS_HIGH
	;
	DISPLAY_READ_DATA(data);
	DISPLAY_SET_WRITE
	DISPLAY_CS_HIGH
	;
	return data;
}
uint16_t display_read_register32(uint16_t myregister) {
	uint16_t datah = 0;
	uint16_t datal = 0;
	DISPLAY_CS_LOW
	;
	DISPLAY_WRITE_COMMAND(myregister);
	DISPLAY_SET_READ
	;
	DISPLAY_RS_HIGH
	;
	DISPLAY_READ_DATA(datah);
	DISPLAY_READ_DATA(datal);
	DISPLAY_SET_WRITE
	DISPLAY_CS_HIGH
	;
	return ((uint32_t) datah << 16) | (datal);
}
uint16_t display_read_ID() {
	//todo MCUFRIEND
	uint16_t ret;
	uint8_t msb;

	ret = display_read_register(0);
	msb = ret >> 8;
	if ((ret & 0xFF) == 0x89)
		return 0x1289;
	if (ret == 0 || msb == (ret & 0xFF)) {
		ret = display_read_register32(0xD3);  //0x9488 for ILI9488
		msb = ret >> 8;
		if (ret == 0 || msb == (ret & 0xFF))
			ret = display_read_register32(0xEF);      //0x0493 for ILI9327
		if (ret == 0x493)
			ret = 0x9327;
		if (ret == 0)
			ret = display_read_register32(0xBF);      //0x0494 for ILI9481
		if (ret == 0x494)
			ret = 0x9481;
	}
	return ret;
}
//todo: setrotation
void display_pixel(int16_t x, int16_t y, uint16_t col) {
	if (x < 0 || y < 0 || x >= DISPLAY_WIDTH || y >= DISPLAY_HEIGHT)
		return;
	display_write_cmd(DISPLAY_PARAM_X, x);
	display_write_cmd(DISPLAY_PARAM_Y, y);
	display_write_cmd(DISPLAY_PARAM_Z, col);
}
uint16_t display_color565(uint8_t r, uint8_t g, uint8_t b) {
	uint16_t c;
	c = r >> 3;
	c <<= 6;
	c |= g >> 2;
	c <<= 5;
	c |= b >> 3;
	return c;
}

void display_vline(uint16_t x, uint16_t y, uint16_t len, uint16_t color) {
	display_write_cmd(3, 0x1028);
	display_write_cmd(0x20, x);
	display_write_cmd(0x21, y);
	DISPLAY_CS_LOW
	DISPLAY_WRITE_COMMAND(0x22);
	DISPLAY_RS_HIGH
	while (len--) {
		DISPLAY_WRITE_DATA(color);
	}
	DISPLAY_CS_HIGH
	display_write_cmd(0x0003, 0x1030);
}
void display_hline(uint16_t x, uint16_t y, uint16_t len, uint16_t color) {
	display_write_cmd(3, 0x1030);
	display_write_cmd(0x20, x);
	display_write_cmd(0x21, y);
	DISPLAY_CS_LOW
	DISPLAY_WRITE_COMMAND(0x22);
	DISPLAY_RS_HIGH
	while (len--) {
		DISPLAY_WRITE_DATA(color);
	}
	DISPLAY_CS_HIGH
	display_write_cmd(0x0003, 0x1030);
}
void display_char(uint16_t x, uint16_t y, uint8_t c, uint16_t color,
		uint16_t bg_col) {
	if (bg_col == color) {
		for (uint8_t i = 0; i < 5; i++) {
			uint8_t line = pgm_read_byte(DISPLAY_font + (c * 5) + i);
			for (uint8_t j = 0; j < 8; j++) {
				if (line & 0x1) {
					display_pixel(x + i, y + j, color);
				}
				line >>= 1;
			}
		}
	} else {
		for (uint8_t i = 0; i < 5; i++) {
			uint8_t line = pgm_read_byte(DISPLAY_font + (c * 5) + i);
			for (uint8_t j = 0; j < 8; j++) {
				if (line & 0x1) {
					display_pixel(x + i, y + j, color);
				} else {
					display_pixel(x + i, y + j, bg_col);
				}
				line >>= 1;
			}
			//display_pixel(x + 5, y, bg_col);
		}
		display_vline(x+5,y,8,bg_col);
	}
}

