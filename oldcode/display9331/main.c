/*
 * main.c
 *
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include "display_basic.h"
#include <util/delay.h>
#include <avr/pgmspace.h>
#include "usart.h"

void clearline(uint8_t line) {
	while (line > 39)
		line -= 40;
	for (uint8_t j = 0; j < 8; j++) {
		display_hline(0, line * 8 + j, DISPLAY_WIDTH, 0);
	}
}
uint8_t px = 0;
uint8_t py = 0;
uint16_t color = 0;
uint16_t bgcolor = 0;

const char str1[] PROGMEM = "  http://www.bedah.de - - - DK5EE   \n  how to use:                       \n  # stty -F /dev/ttyUSB0 -hupcl     \n  # echo hi >/dev/ttyUSB0           \n  Test string here                  \n";
const char str2[] PROGMEM = "     ___________                           @(_)-------(_)                         @   /  ###  \\                          @   /   ###   \\                         @  |    ###    |                        @ @|01791366194|                       ________________________________________";
const char str3[] PROGMEM = "1                                       2                                       3                                       4                                       5                                       ";


void echo(uint8_t ch) {
	if (ch == 13) return;
	if (ch == 10) {
		px = 0;
		py++;
		if (py >= 40) {
			py = 0;
		}
		color = 0xFFFF;
		clearline(py);
	} else {
		display_char(px * 6, py * 8, ch, color, bgcolor);
		px++;
		if (px > 39) {
			px = 0;
			py++;
			if (py >= 40) {
				py = 0;
			}
			if (color == 0xFFFF) {
				color = display_color565(196, 196, 255);
			} else {
				color = display_color565(128, 128, 255);
			}
			clearline(py);
		}
	}
}
void echo_str( char* str) {
    while (*str) {
    	echo(*str++);
    }
}
void echo_str_pgm(const char* str) {

	char c;
	    while (0 != (c = pgm_read_byte(str))) {
	    	color=0;
	    	bgcolor=0xFFFF;
	    	echo(c);
	        str++;


	    }
}
uint8_t ca=0;
uint8_t cb=85;
uint8_t cc=170;
void echo_str_pgm_col(const char* str) {
	uint8_t ca2=0;
	uint8_t cb2=0;
	uint8_t cc2=0;

	char c;
	    while (0 != (c = pgm_read_byte(str))) {
	    	if (ca>127) ca2=ca*2; else ca2=511-(ca*2);
	    	if (cb>127) cb2=cb*2; else cb2=511-(cb*2);
	    	if (cc>127) cc2=cc*2; else cc2=511-(cc*2);
	    	color=display_color565(ca2, cb2, cc2);
	    	ca+=85; cb-=85;cc+=85;
	    	if (ca>127) ca2=ca*2; else ca2=511-(ca*2);
	    	if (cb>127) cb2=cb*2; else cb2=511-(cb*2);
	    	if (cc>127) cc2=cc*2; else cc2=511-(cc*2);
	    	bgcolor=display_color565(ca2, cb2, cc2);
	    	echo(c);
	        str++;
	        ca-=83;cb+=80;cc-=84;

	    }
	    bgcolor=0;
}

int main(void) {
	_delay_ms(100);
	usart0_init();
	_delay_ms(100);
	display_init();
	_delay_ms(100);
	usart0_putc('e');
	usart0_putc('c');
	usart0_putc('h');
	usart0_putc('o');
	usart0_putc(10);


	for (uint16_t j = 0; j < DISPLAY_WIDTH; j++) {
		display_vline(j, 0, DISPLAY_HEIGHT, 0);
	}
	color = display_color565(255, 0, 0);
	echo('d');
	echo('i');
	echo('s');
	echo('p');
	echo('l');
	echo('a');
	echo('y');
	echo('!');
	echo(10);
	echo_str_pgm(str1);
	echo(10);
	echo_str_pgm_col(str2);
	echo_str_pgm_col(str3);
	echo(10);
	uint8_t ch=0;
	for (ch=0; ch<16; ch++) {
		uint8_t a='0';
		if (ch>9) a='A'-10;
		display_char(10+9*(ch), 160, a+ch, display_color565(0,255,0), 0);
		display_char(2, 170+9*(ch), a+ch, display_color565(0,255,0), 0);
	}
	ch=0;
	do{
		ch-=1;
		display_char(10+9*(ch&0xf), 170+9*(ch>>4), ch, 0xFFFF, 0);
	} while(ch);

	while (!usart0_data_received()) {
		display_char(200, 200, ch, 0xFFFF, 0);
		_delay_ms(100);
		ch++;
	}
	while (1) {
		while (usart0_data_received()) {
			echo(usart0_getc());
		}
		//		_delay_ms(10);
	}
	return 0;

}
