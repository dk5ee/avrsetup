/*
 * display_basic.h
 *
 */

#ifndef DISPLAY_BASIC_H_
#define DISPLAY_BASIC_H_
#include <avr/io.h>
#include <avr/interrupt.h>
#include "display_setting.h"

void display_init();
void display_write_cmd(uint16_t command, uint16_t data);
void display_write_cmd_N(uint16_t command, int8_t count, uint8_t * data);
void display_pixel(int16_t x, int16_t y, uint16_t col);
uint16_t display_color565(uint8_t r, uint8_t g, uint8_t b);
void display_char(uint16_t x, uint16_t y, uint8_t c, uint16_t color, uint16_t bg_col);
void display_vline(uint16_t x, uint16_t y, uint16_t len, uint16_t color);
void display_hline(uint16_t x, uint16_t y, uint16_t len, uint16_t color);
#endif /* DISPLAY_BASIC_H_ */
