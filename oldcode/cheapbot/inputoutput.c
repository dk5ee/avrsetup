/*
 * inputoutput.c
 *
 */

#include "inputoutput.h"

uint32_t getinput(uint8_t outputnumber) {
	cli();
	uint32_t value = inputdata[outputnumber].time_high;
	sei();
	return value;
}
uint32_t getinputlow(uint8_t outputnumber){
	cli();
	uint32_t value = inputdata[outputnumber].time_low;
	sei();
	return value;
}
void init_inputdata() {
	// inputtimer is timer0
	TCCR0A |= 0;
	/* clock source:
	 * 	CS02	CS01	CS00 	Desc
	 * 	0		0		0		No Clock
	 * 	0		0		1		1:1 = system clock - 64.5 kHz @ 16MHz - way to slow, start again
	 * 	0		1		0		1:8
	 * 	0		1		1		1:64
	 * 	1		0		0		1:256
	 * 	1		0		1		1:1024
	 * 	1		1		0		external T0 pin falling edge
	 * 	1		1		1		rising edge
	 */
	TCCR0B |= (0 << CS02) | (0 << CS01) | (1 << CS00);
	//enable overflow of TCNT
	//disabled.. TIMSK0 |= (1<<TOIE0);
	inputdata[0].time_high = 0ul;
	inputdata[0].time_low = 0ul;
	inputdata[0].counter = 0ul;
	inputdata[1].time_high = 0ul;
	inputdata[1].time_low = 0ul;
	inputdata[1].counter = 0ul;
}
void init_timer1() {
	TCCR1A |= 0;
	TCCR1B |= (1<<CS10);
	TIMSK1 |= (1 << TOIE1);
	//we use external interrupts int0 and int1
	EICRA |= (1 << ISC10) | (1 << ISC00);
	EIMSK |= (1 << INT1) | (1 << INT0);
}
/*
void initoutput(uint8_t outputnumber, uint8_t pin) {

	outputdata[outputnumber].time_high = 0;
	outputdata[outputnumber].time_low = 0;
	outputdata[outputnumber].counter = 0;
	outputdata[outputnumber].state = 0;
	outputdata[outputnumber].outputpin = 1 << pin;

}
*/
/*
void init_outputdata() {
	//outputtimer is timer2
	TCCR2A |= 0;
	//same speed as inputtimer
	TCCR2B |= (0 << CS22) | (0 << CS21) | (1 << CS20);
	//enable overflow of TCNT
	TIMSK2 |= (1 << TOIE2);
	initoutput(0, 4);
	initoutput(1, 5);
	initoutput(2, 6);
	initoutput(3, 7);
	DDRD |= (1 << 4) | (1 << 5) | (1 << 6) | (1 << 7);
	initoutput(4, 0);
	initoutput(5, 1);
	initoutput(6, 2);
	initoutput(7, 3);
	DDRB |= (1 << 0) | (1 << 1) | (1 << 2) | (1 << 3);
	//..

}
*/
/*
void setoutput(uint8_t outputnumber, uint8_t hightime, uint8_t lowtime) {
	cli();
	outputdata[outputnumber].time_high = hightime;
	outputdata[outputnumber].time_low = lowtime;
	sei();
}
*/
ISR(INT0_vect) {
	uint16_t timerl = TCNT1;
	uint16_t timerh=globaltimercount;
	uint32_t timelong= (((uint32_t) timerh)<<16)+ timerl;
	uint32_t timeold=inputdata[0].counter;
	uint32_t newval = timelong - timeold;
	inputdata[0].counter = timelong;
	if (PIND & (1 << PD2)) {
		inputdata[0].time_low = newval;
	} else {
		inputdata[0].time_high =newval;
	}

}
ISR(INT1_vect) {
	uint16_t timerl = TCNT1;
	uint16_t timerh=globaltimercount;
	uint32_t timelong= (((uint32_t) timerh)<<16)+ timerl;
	uint32_t timeold=inputdata[1].counter;
	uint32_t newval = timelong - timeold;
	inputdata[1].counter = timelong;
	if (PIND & (1 << PD3)) {
		inputdata[1].time_low = newval;
	} else {
		inputdata[1].time_high =newval;
	}
}
ISR(TIMER1_OVF_vect) {
	globaltimercount++;
}

ISR(TIMER0_OVF_vect) {
//deleted
}

ISR(TIMER2_OVF_vect) {
/*
#ifdef SMALLER_ISRs
	uint8_t output_i = global_output_i;
	output_i++;
	if (output_i > OUTPUTCOUNT) {
		output_i = 0;
	}
	global_output_i = output_i;
	{
#else
		uint8_t output_i;
		for (output_i = 0; output_i < OUTPUTCOUNT; output_i++) {
#endif
		uint8_t t_counter = outputdata[output_i].counter;
		uint8_t t_state = outputdata[output_i].state;
		uint8_t t_low = outputdata[output_i].time_low;
		uint8_t t_high = outputdata[output_i].time_high;

		uint8_t t_pin = outputdata[output_i].outputpin;
		t_counter--;
		if (t_counter == 0) {
			if (t_state == 1) {
				t_counter = t_low;
				t_state = 0;
				if (output_i < 4) {
					PORTD &= (~t_pin);
				} else {
					PORTB &= (~t_pin);
				}
			} else {
				if (t_high > 0) {
					t_state = 1;
					if (output_i < 4) {
						PORTD |= t_pin;
					} else {
						PORTB |= t_pin;
					}

				} else {
					//else nothing - stay low if no uptime is set
				}
				t_counter = t_high;
			}
		}
		outputdata[output_i].counter = t_counter;
		outputdata[output_i].state = t_state;
		outputdata[output_i].time_low = t_low;
		outputdata[output_i].time_high = t_high;
	}
*/
}

