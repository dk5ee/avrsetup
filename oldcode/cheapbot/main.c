#include <inttypes.h>

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "ringuart.h"
#include "inputoutput.h"
#include "twilib.h"
#include "adcint.h"

void initPCA9685() {
	sei();
	TWIsetaddress(0xe0);
	ring_outchar('f');
	ring_outint(TWIstatus());
	while (TWIstatus()) {
	}; //wait till idle;
	TWIwrite(0);
	TWIwrite(0 << 7 //0=RESTART off - reset values after sleep
	& 0 << 6 //1=external clock, off
	& 0 << 5 //1=register autoincrement
	& 0 << 4 //0=normal mode, 1=sleep mode
	& 0 << 3 //subaddress 1
	& 0 << 2 //subaddress 2
	& 0 << 1 //subaddress 3
	& 0 << 0 //0=ALL call disabled
			);
	TWIstart();
	ring_outchar('g');
	ring_outint(TWIstatus());
	while (TWIstatus()) {
	};
	TWIwrite(1);
	TWIwrite(0 << 4 //0=normal mode, 1=output logic inverted
	& 1 << 3 //0=output change on STOP 1=output change on ACK
	& 1 << 2 //0=open drain 1=totem pole
	& 0 << 1 //OE settings
	& 1 << 0 //OE settings
			);
	TWIstart();
	ring_outchar('h');
	ring_outint(TWIstatus());
	while (TWIstatus()) {
	};
	TWIwrite(0xFE);
	TWIwrite(0x03);
	TWIstart();
}
void PCA9685_data(uint8_t pin, uint16_t onval, uint16_t offval) {

	if (pin > 15) {
		return;
	}
	while (TWIstatus()) {
	}; //wait till idle;
	TWIwrite(pin * 4 + 6);
	TWIwrite(onval & 0xFF);
	TWIstart();
	ring_outchar('a');
	ring_outint(TWIstatus());
	while (TWIstatus()) {
	}; //wait till idle;
	TWIwrite(pin * 4 + 6 + 1);
	TWIwrite(onval >> 8);
	TWIstart();
	ring_outchar('b');
	ring_outint(TWIstatus());
	while (TWIstatus()) {
	}; //wait till idle;
	TWIwrite(pin * 4 + 6 + 2);
	TWIwrite(offval & 0xFF);
	TWIstart();
	ring_outchar('c');
	ring_outint(TWIstatus());
	while (TWIstatus()) {
	}; //wait till idle;
	TWIwrite(pin * 4 + 6 + 3);
	TWIwrite(offval >> 8);
	TWIstart();
}
void scani2c() {
	sei();
	uint8_t address = 0;
	do {
		address--;
		sei();
		uint8_t a = 255 - address;
		TWIinit();
		TWIsetaddress(a);
		TWIwrite(0);
		TWIstart();

		ring_newline
		ring_outchar('a');
		ring_outint(a);
		ring_outchar(' ');
		_delay_ms(100);
		ring_outchar('r');
		ring_outint(TWIstatus());
		if (TWIstatus()==0) {
			_delay_ms(600);
		}
		_delay_ms(100);

	} while (address);
}
void PCA9685_simple(uint8_t pin, uint16_t val) {
	uint16_t onval = 0;
	uint16_t offval = 0;
	if (val > 4095) {
		onval = 1 << 12;
	} else if (val == 0) {
		offval = 1 << 12;
	} else {
		onval = 0;
		offval = val;
	}
	PCA9685_data(pin, onval, offval);
}

int16_t servotoint(uint32_t value) {
	//24000 is middle
	int32_t v1 = value - 24000l;
	if (v1 > 20000)
		return 0;
	if (v1 < (-20000))
		return 0;
	return v1;
}

void setwheel(uint8_t wheel, int16_t value) {
	if (wheel > 3)
		return;
	value = value >> 5;
	if (value > 255) {
		value = 255;
	}
	if (value < -255) {
		value = -255;
	}
	uint8_t forward = 8;
	uint8_t backward = 8;
	switch (wheel) {
	case 0:
		forward = 1;
		backward = 0;
		break;
	case 1:
		forward = 3;
		backward = 2;
		break;
	case 2:
		forward = 4;
		backward = 5;
		break;
	case 3:
		forward = 6;
		backward = 7;
		break;
	default:
		return;
	}
	if ((forward < 8) && (backward < 8)) {
		if (value < 0) {
			uint8_t temp = forward;
			forward = backward;
			backward = temp;
			value = 0 - value;
		}

		//	setoutput(forward,value,255-value);
		//	setoutput(backward,0,200);

	}
}
int main(void) {
	//5tr5äöüadcinit();
	TWIinit();

	init_inputdata();
//	init_outputdata();
	init_timer1();
	ringuartinit();
	ring_outchar('r');
	ring_outint(TWIstatus());
	initPCA9685();
	ring_outchar('s');
	ring_newline
//	uint8_t val = 1;
	DDRB |= 1 << PB5;
	uint8_t count = 0;
	while(1){
		scani2c();
	}
	while (1) {
		_delay_ms(20);
		ring_newline
		ring_outchar('x');
		ring_outint(count);
		PCA9685_simple(count, 2000);
		_delay_ms(400);
		PCA9685_simple(count, 0);
		count++;
		if (count == 8) {
			count = 0;
		};
		/*
		 int16_t s0 = servotoint(getinput(0));
		 int16_t s1 = servotoint(getinput(1));
		 if (count==0) {
		 ring_newline
		 ring_outchar('b');
		 ring_outint(s0);
		 ring_outchar(' ');
		 ring_outint(s1);
		 }
		 if ((s0<100) && (s0>-100)) s0=0;
		 if ((s1<100) && (s1>-100)) s1=0;

		 setwheel(0,s0+s1);
		 setwheel(1,s0+s1);
		 setwheel(2,s0-s1);
		 setwheel(3,s0-s1);


		 if ((s1<100) && (s1>-100)) s1=0;
		 uint16_t s0a=(s0<0)?-s0:s0;
		 s0a>>=5;
		 if (s0a>255) s0a=255;
		 //s0a=0;
		 uint16_t s1a=(s1<0)?-s1:s1;
		 s1a>>=5;
		 if (s1a>255) s1a=255;
		 //s1a=0;
		 if (s0>0) {
		 setoutput(0,s0a,255-s0a);
		 setoutput(1,0,100);
		 }
		 if (s0==0) {
		 setoutput(0,0,100);
		 setoutput(1,0,100);
		 }
		 if (s0<0) {
		 setoutput(0,0,100);
		 setoutput(1,s0a,255-s0a);

		 }
		 if (s1>0) {
		 setoutput(2,s1a,255-s1a);
		 setoutput(3,0,100);
		 }
		 if (s1==0) {
		 setoutput(2,0,100);
		 setoutput(3,0,100);
		 }
		 if (s1<0) {
		 setoutput(2,0,100);
		 setoutput(3,s1a,255-s1a);

		 }

		 if (count==0) {
		 ring_newline
		 ring_outint(s0);
		 ring_outchar(' ');
		 ring_outint(s1);
		 ring_newline
		 val = 1 - val;
		 txring_enqueue('_');
		 while (rxring_count()) {
		 txring_enqueue(rxring_dequeue() + 1);
		 }
		 if (val) {
		 ring_outchar('H');
		 ring_outchar('i');
		 ring_outchar(' ');
		 PORTB |= 1 << PB5;
		 } else {
		 ring_outchar('L');
		 ring_outchar('o');

		 PORTB &= ~(1 << PB5);
		 }
		 ring_newline
		 ring_outlong(getinput(0));
		 ring_outchar(' ');
		 ring_outlong(getinputlow(0));
		 ring_outchar(' ');

		 ring_outlong(getinput(1));
		 ring_outchar(' ');
		 ring_outlong(getinputlow(1));
		 ring_outchar(' ');
		 ring_outint(servotoint(getinput(1)));
		 ring_newline
		 _delay_ms(100);

		 }
		 */
	}
}
