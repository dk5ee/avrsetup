/*
 * inputoutput.h
 *
 */

#ifndef INPUTOUTPUT_H_
#define INPUTOUTPUT_H_

#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#define INPUTPORT PIND
#define INPUTDDRPORT DDRD
#define INPUTCOUNT 2
#define INPUTPIN0 PD2
#define INPUTPIN1 PD3
// #define OUTPUTCOUNT 8

typedef struct {
	uint32_t dummy1;
	uint32_t time_high; //measured uptime in ISR ticks, "0" is downtime overflow, 0xFFFF is uptime overflow
	uint32_t time_low; //measured downtime in ISR ticks, overflow is vice versa
	uint32_t counter; // counter for last input
} inputdatatype;
/*
typedef struct {
	uint8_t dummy1;
	uint8_t dummy2;
	uint8_t time_high; //amount of ticks in uptime. if zero, no uptime
	uint8_t time_low; //amount of ticks in downtime. if minimum is 1, zero is max downtime
	uint8_t counter; //counter for each output
	uint8_t state; //for remembering state
	uint8_t outputpin; //pinnumber for output
} outputdatatype;
*/
//volatile outputdatatype outputdata[OUTPUTCOUNT];

volatile inputdatatype inputdata[INPUTCOUNT];

// if SMALLER_ISRs is defined, in every timerinterrupt only one input or output pin is checked or set
#define SMALLER_ISRs
#ifdef SMALLER_ISRs
//volatile uint8_t global_output_i;
volatile uint8_t global_input_i;
#endif


void init_inputdata();
void init_outputdata();
volatile uint8_t globaltimercount;
void init_timer1();
//void initoutput(uint8_t outputnumber, uint8_t pin);

uint32_t getinput(uint8_t outputnumber);
uint32_t getinputlow(uint8_t outputnumber);
//void setoutput(uint8_t outputnumber, uint8_t hightime, uint8_t lowtime);


#ifdef USE_GLOBAL_COUNTS
//void setoutputcounter(uint32_t value);
//uint32_t getoutputcounter();
void setinputcounter(uint32_t value);
uint32_t getinputcounter();
#endif
#endif /* INPUTOUTPUT_H_ */
