/*
 * adcint.c
 *
 */
#include "adcint.h"
volatile uint8_t adc_channel = 0;
volatile uint8_t adc_run = 0;
volatile uint8_t adccounter=0;
volatile uint16_t adctemp=0;
/*#define LEDON PORTB |= 1 << PB5
#define LEDOFF PORTB &= ~(1<< PB5)
#define LEDINIT DDRB |= 1 << PB5
*/
void adcinit() {
	//LEDINIT;
	uint8_t i = 0;
	DIDR0 = 0b00001111;//disable digital input
	for (i = 0; i < 8; i++) {
		adcfield[i] = 0;
	}
	adctemp =0;
	//ADMUX |= 1 << ADLAR; //left
	ADMUX |= 1 << REFS0;
	ADMUX |= 1 << REFS1;

	//ref0 ref1
	//	0	0	extern vref
	//	1	0	avcc external
	//	0	1	reserved
	//	1	1	internal 1.1v
	ADCSRB =0;
	ADCSRA |= 1 << ADPS0;
	ADCSRA |= 1 << ADPS1;
	ADCSRA |= 1 << ADPS2;
	//ADCSRA |= 1 << ADATE;
	//adps[8]=2,2,4,8,16,32,64,128
	//ADCSRA |= 1 << ADEN; //enable ADC
  //enable isr

}
void adcstart() {
//	LEDOFF;
	ADMUX &= 0b11110000;//deselect channel
	adc_channel=0;
	adc_run = 1;
	ADCSRA |= 1 << ADEN;
	ADCSRA |= 1 << ADSC | 1<<ADIE; //start conversion
}
void adcstop() {
//	LEDOFF;
	adc_run = 0;
	//ADCSRA &= ~(1<<ADEN);
	//ADCSRA &= ~(1<<ADIE);
}
uint8_t myadccounter(){
	return adccounter;
}
uint16_t adcinternaltemp(){
	return adctemp;
}
uint16_t getadcdata(uint8_t channel){
	uint16_t data;
	cli();
	data= (adcfield[channel]);
	sei();
	return data;
}
ISR(ADC_vect)
{

	uint16_t adcval=ADC; //remember ADC

	if (adc_channel==8) {	//temp
#ifdef ADCsmooth
		adctemp=((adctemp*((1<<ADCsmooth)-1))+adcval)>>ADCsmooth;
#else
		adctemp=adcval;
#endif
	} else {	//else..
		uint8_t now=(adc_channel&0xf)+(adc_channel&0x10?6:0);
#ifdef ADCsmooth
		/*
		 * smoothing:
		 * simply (oldvalue * (n-1) + newvalue )/n
		 * n= (
		 */
		adcval=((adcfield[now]*((1<<ADCsmooth)-1))+adcval)>>ADCsmooth;
		adcfield[now] = adcval;
#else
		adcfield[now] =adcval;
#endif
	}
	//enter next round..
	adc_channel++;
	if (adc_channel==4) adc_channel=6;	//we don't need channel 4&5
	if (adc_channel==9)	{
		//done round, start from beginning

		adc_channel=0;
	}
	ADMUX &= 0b11110000;//deselect channel
	ADMUX |= adc_channel & 0x0f;

	if (adc_run !=0)
		ADCSRA |= 1 << ADSC;
	if (adc_channel==0){
		adccounter++;
	}

}
/**annotations
 *
 * ATmega48A/PA/88A/PA/168A/PA/328/P adc channel list
 * 0	ADC0
 * 1	ADC1
 * 2	ADC2
 * 3	ADC3
 * 4	ADC4
 * 5	ADC5
 * 6	ADC6	only smd
 * 7	ADC7	only smd
 * 8	ADC8	internal temperature
 * 9..13	reserved
 * 14	1.1V
 * 15	0V
 *
 */
