/*
 * myuart.c
 *
 */

#include "myuart.h"
static uint8_t myuart_RXBuf[MYUART_RX_BUFFER];
static uint8_t myuart_TXBuf[MYUART_TX_BUFFER];
static volatile uint8_t myuart_RXstart =0;
static volatile uint8_t myuart_RXend =0;
static volatile uint8_t myuart_RXoverflow=0;
static volatile uint8_t myuart_TXstart =0;
static volatile uint8_t myuart_TXend =0;

void myuart_init(void){
	cli();
#if defined BAUD
#undef BAUD
#endif
#define BAUD MYUART_BAUD_RATE
#include <util/setbaud.h>
	myUBRRL = UBRRL_VALUE;
	myUBRRH = UBRRH_VALUE;
	myUCSRB = ((1 << myRXCIE) | (1 << myRXEN) | (1 << myTXEN));
    myUCSRC = (1 << myUCSZ1) | (1 << myUCSZ0);
#if USE_2X
   myUCSRA |= (1 << myU2X);
   #else
   myUCSRA &= ~(1 << myU2X);
   #endif
   sei();
}

//returns 255 if txbuffer is full, else returns remaining buffersize
uint8_t myuart_sendbyte(uint8_t data){
	uint8_t temp= (myuart_TXstart +1 )& (MYUART_TX_BUFFER -1);
	if (temp==myuart_TXend) {return 255;} else {
		myuart_TXBuf[temp]=data;
		myuart_TXstart=temp;
		//now: allways activate interupt for sending
		myUCSRB |= (1 << myUDRIE);
		return MYUART_TX_BUFFER-((myuart_TXend - myuart_TXstart) & (MYUART_TX_BUFFER -1));
	}

}
//returns 0 if no data in rxbuffer, else returns first byte + (1+remaining_bytes <<8)
uint16_t myuart_getbyte() {
	if (myuart_RXstart == myuart_RXend) {return 0;} else
	{
		uint8_t temp= (myuart_RXend +1) & (MYUART_RX_BUFFER -1);
		uint8_t count= (temp - myuart_RXstart) & (MYUART_RX_BUFFER -1);
		myuart_RXend = temp;
		uint8_t data= myuart_RXBuf[temp];
		return (count <<8)+data;

	}
}
//returns bytes in RX buffer
uint8_t myuart_incount() {
	return ((myuart_RXend - myuart_RXstart) & (MYUART_RX_BUFFER -1));
}
//returns free bytes in RX buffer
uint8_t myuart_infree(){
	return MYUART_RX_BUFFER-((myuart_RXend - myuart_RXstart) & (MYUART_RX_BUFFER -1));
}
//return bytes in TX buffer
uint8_t myuart_outcount() {
	return ((myuart_TXend - myuart_TXstart) & (MYUART_TX_BUFFER -1));
}
//returns free bytes in TX buffer
uint8_t myuart_outfree(){
	return MYUART_TX_BUFFER-((myuart_TXend - myuart_TXstart) & (MYUART_TX_BUFFER -1));
}
uint8_t myuart_isoverflow() {return myuart_RXoverflow;}
void myuart_resetoverflow() {myuart_RXoverflow=0;}
ISR(USART_RX_vect) {
	 uint8_t data;
	 data = UDR0;
	 uint8_t temp;
	 temp= ((myuart_RXstart+1) & (MYUART_RX_BUFFER -1));
	 if (temp==myuart_RXend) {
		 myuart_RXoverflow =1; //overflow happened.. OMG!
	 } else {
		 myuart_RXstart=temp;
		 myuart_RXBuf[temp]=data;
	 }
}
ISR(USART_UDRE_vect) {
	if ((myuart_TXend - myuart_TXstart) & (MYUART_TX_BUFFER -1)) {
	uint8_t temp;
	temp = ((myuart_TXend +1)& (MYUART_RX_BUFFER -1));
	myuart_TXend = temp;
	myUDR = myuart_TXBuf[temp];
	} else {
		myUCSRB &= ~(1 << myUDRIE);
	}

}
