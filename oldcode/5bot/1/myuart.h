/*
 * myuart.h
 *
 */

#ifndef MYUART_H_
#define MYUART_H_
#include <avr/io.h>
#include <avr/interrupt.h>

//bitsize should be between 2 and 7
#define MYUART_RX_BUFFER_BITSIZE 7
#define MYUART_TX_BUFFER_BITSIZE 7
#define MYUART_BAUD_RATE 9600

#define MYUART_RX_BUFFER (1<<MYUART_RX_BUFFER_BITSIZE)
#define MYUART_TX_BUFFER (1<<MYUART_TX_BUFFER_BITSIZE)

#if defined (UDR)
#define myU2X U2X
#define myUBRRL UBRRL
#define myUBRRH UBRRH
#define myUCSRA UCSRA
#define myUCSRB UCSRB
#define myUCSRC UCSRC
#define myUCSZ0 UCSZ0
#define myUCSZ1 UCSZ1
#define myUDR UDR
#define myUDRIE UDRIE
#define myRXCIE RXCIE
#define myRXEN RXEN
#define myTXCIE TXCIE
#define myTXEN TXEN
#endif
#if defined (UDR0)
#define myU2X U2X0
#define myUBRRL UBRR0L
#define myUBRRH UBRR0H
#define myUCSRA UCSR0A
#define myUCSRB UCSR0B
#define myUCSRC UCSR0C
#define myUCSZ0 UCSZ00
#define myUCSZ1 UCSZ01
#define myUDR UDR0
#define myUDRIE UDRIE0
#define myRXCIE RXCIE0
#define myRXEN RXEN0
#define myTXCIE TXCIE0
#define myTXEN TXEN0
#endif
/*
// for second uart comment block above, use this block instead:
 #if defined (UDR1)
#define myU2X U2X1
#define myUBRRL UBRR1L
#define myUBRRH UBRR1H
#define myUCSRA UCSR1A
#define myUCSRB UCSR1B
#define myUCSRC UCSR1C
#define myUCSZ0 UCSZ10
#define myUCSZ1 UCSZ11
#define myUDR UDR1
#define myUDRIE UDRIE1
#define myRXCIE RXCIE1
#define myRXEN RXEN1
#define myTXCIE TXCIE1
#define myTXEN TXEN1
#endif
 */
void myuart_init(void);

//returns false if txbuffer is full, else returns remaining buffersize
uint8_t myuart_sendbyte(uint8_t data);
//returns 255 if no data in rxbuffer, else returns first byte + (1+remaining_bytes <<8)
uint16_t myuart_getbyte();
//returns bytes in RX buffer
uint8_t myuart_incount();
//returns free bytes in RX buffer
uint8_t myuart_infree();
//return bytes in TX buffer
uint8_t myuart_outcount();
//returns free bytes in TX buffer
uint8_t myuart_outfree();
uint8_t myuart_isoverflow();
void myuart_resetoverflow();

#endif /* MYUART_H_ */
