/*
 * adcint.h
 *
 */

#ifndef ADCINT_H_
#define ADCINT_H_
#include <avr/io.h>
#include <avr/interrupt.h>

#define ADCsmooth 2

volatile uint16_t adcfield[32];

void adcinit();
void adcstart();
void adcstop();
uint16_t getadcdata(uint8_t channel);

uint8_t myadccounter();

#endif /* ADCINT_H_ */
