/*
 * hardware.c
 *
 */
#include "hardware.h"

void hardwareinit() {
	adcinit();
	adcstart();
	TWIinit();
	ringuartinit();
	initservos();
}
