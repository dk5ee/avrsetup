/*
 * mydata.h
 *

 script for generating sin table:
 #!/usr/bin/python
 import math
 print "uint8_t mysintable[256] PROGMEM = {"
 for x in range(0,256):
 y= (round(256.0*math.sin((x/512.0)*math.pi)));
 if y>255:
 y=0
 print "    %d, // value for  %d" % (y,x)
 print "};"
 ATTENTION: rang is 0..256, special handling necessary!
 */

#ifndef MYDATA_H_
#define MYDATA_H_
#include <stdint.h>
#include <avr/pgmspace.h>
#include "ringuart.h"

#define STRINGCOUNT 32
#define MAXSTRINGLEN 16
#define STRINGBUFCOUNT 4

static const char str00[] PROGMEM = "OK";
static const char str01[] PROGMEM = "ERROR";
static const char str02[] PROGMEM = "CONNECTED";
static const char str03[] PROGMEM = "DISCONNECTED";
static const char str04[] PROGMEM = "AT";
static const char str05[] PROGMEM = "SET";
static const char str06[] PROGMEM = "STATUS";
static const char str07[] PROGMEM = "GET";
static const char str08[] PROGMEM = ""; //empty string
static const char str09[] PROGMEM = "TEMP2";
static const char str10[] PROGMEM = "TEMP3";
static const char str11[] PROGMEM = "TEMP4";
static const char str12[] PROGMEM = "TEMP5";
static const char str13[] PROGMEM = "TEMP6";
static const char str14[] PROGMEM = "TEMP7";
static const char str15[] PROGMEM = "TEMP8";
static const char str16[] PROGMEM = "TEMP9";
static const char str17[] PROGMEM = "TEMPA";
static const char str18[] PROGMEM = "TEMPB";
static const char str19[] PROGMEM = "TEMPC";
static const char str20[] PROGMEM = "TEMPD";
static const char str21[] PROGMEM = "SYNTAX ERROR";
static const char str22[] PROGMEM = "CHAR ERROR";
static const char str23[] PROGMEM = "AUTH ERROR";
static const char str24[] PROGMEM = "LOW ENERGY";
static const char str25[] PROGMEM = "BY BEDAH.DE";
static const char str26[] PROGMEM = "STOP";
static const char str27[] PROGMEM = "ATE0"; //no local echo
static const char str28[] PROGMEM = "ATQ1"; //disable blabla
static const char str29[] PROGMEM = "ATR1"; //Slave mode for module
static const char str30[] PROGMEM = "ATN=SEM"; //new bot name
static const char str31[] PROGMEM = "ATP=8008"; //new pin: 8008

volatile char stringbuf1[MAXSTRINGLEN];
volatile char stringbuf2[MAXSTRINGLEN];
volatile char stringbuf3[MAXSTRINGLEN];
volatile char stringbuf4[MAXSTRINGLEN];
volatile char *stringbufs[] = { stringbuf1, stringbuf2, stringbuf3, stringbuf4 };

uint8_t loadbuf(uint8_t strnum, uint8_t bufnum);
uint8_t buf2uart(uint8_t bufnum);

const char * const stringsP[] PROGMEM = { str00, str01, str02, str03, str04,
		str05, str06, str07, str08, str09, str10, str11, str12, str13, str14,
		str15, str16, str17, str18, str19, str20, str21, str22, str23, str24,
		str25, str26, str27, str28, str29, str30, str31

};

int16_t getsin(int16_t degree);

const uint8_t mysintable[256] PROGMEM = { 0, // value for  0
		2, // value for  1
		3, // value for  2
		5, // value for  3
		6, // value for  4
		8, // value for  5
		9, // value for  6
		11, // value for  7
		13, // value for  8
		14, // value for  9
		16, // value for  10
		17, // value for  11
		19, // value for  12
		20, // value for  13
		22, // value for  14
		24, // value for  15
		25, // value for  16
		27, // value for  17
		28, // value for  18
		30, // value for  19
		31, // value for  20
		33, // value for  21
		34, // value for  22
		36, // value for  23
		38, // value for  24
		39, // value for  25
		41, // value for  26
		42, // value for  27
		44, // value for  28
		45, // value for  29
		47, // value for  30
		48, // value for  31
		50, // value for  32
		51, // value for  33
		53, // value for  34
		55, // value for  35
		56, // value for  36
		58, // value for  37
		59, // value for  38
		61, // value for  39
		62, // value for  40
		64, // value for  41
		65, // value for  42
		67, // value for  43
		68, // value for  44
		70, // value for  45
		71, // value for  46
		73, // value for  47
		74, // value for  48
		76, // value for  49
		77, // value for  50
		79, // value for  51
		80, // value for  52
		82, // value for  53
		83, // value for  54
		85, // value for  55
		86, // value for  56
		88, // value for  57
		89, // value for  58
		91, // value for  59
		92, // value for  60
		94, // value for  61
		95, // value for  62
		97, // value for  63
		98, // value for  64
		99, // value for  65
		101, // value for  66
		102, // value for  67
		104, // value for  68
		105, // value for  69
		107, // value for  70
		108, // value for  71
		109, // value for  72
		111, // value for  73
		112, // value for  74
		114, // value for  75
		115, // value for  76
		117, // value for  77
		118, // value for  78
		119, // value for  79
		121, // value for  80
		122, // value for  81
		123, // value for  82
		125, // value for  83
		126, // value for  84
		128, // value for  85
		129, // value for  86
		130, // value for  87
		132, // value for  88
		133, // value for  89
		134, // value for  90
		136, // value for  91
		137, // value for  92
		138, // value for  93
		140, // value for  94
		141, // value for  95
		142, // value for  96
		144, // value for  97
		145, // value for  98
		146, // value for  99
		147, // value for  100
		149, // value for  101
		150, // value for  102
		151, // value for  103
		152, // value for  104
		154, // value for  105
		155, // value for  106
		156, // value for  107
		157, // value for  108
		159, // value for  109
		160, // value for  110
		161, // value for  111
		162, // value for  112
		164, // value for  113
		165, // value for  114
		166, // value for  115
		167, // value for  116
		168, // value for  117
		170, // value for  118
		171, // value for  119
		172, // value for  120
		173, // value for  121
		174, // value for  122
		175, // value for  123
		177, // value for  124
		178, // value for  125
		179, // value for  126
		180, // value for  127
		181, // value for  128
		182, // value for  129
		183, // value for  130
		184, // value for  131
		185, // value for  132
		186, // value for  133
		188, // value for  134
		189, // value for  135
		190, // value for  136
		191, // value for  137
		192, // value for  138
		193, // value for  139
		194, // value for  140
		195, // value for  141
		196, // value for  142
		197, // value for  143
		198, // value for  144
		199, // value for  145
		200, // value for  146
		201, // value for  147
		202, // value for  148
		203, // value for  149
		204, // value for  150
		205, // value for  151
		206, // value for  152
		207, // value for  153
		207, // value for  154
		208, // value for  155
		209, // value for  156
		210, // value for  157
		211, // value for  158
		212, // value for  159
		213, // value for  160
		214, // value for  161
		215, // value for  162
		215, // value for  163
		216, // value for  164
		217, // value for  165
		218, // value for  166
		219, // value for  167
		220, // value for  168
		220, // value for  169
		221, // value for  170
		222, // value for  171
		223, // value for  172
		224, // value for  173
		224, // value for  174
		225, // value for  175
		226, // value for  176
		227, // value for  177
		227, // value for  178
		228, // value for  179
		229, // value for  180
		229, // value for  181
		230, // value for  182
		231, // value for  183
		231, // value for  184
		232, // value for  185
		233, // value for  186
		233, // value for  187
		234, // value for  188
		235, // value for  189
		235, // value for  190
		236, // value for  191
		237, // value for  192
		237, // value for  193
		238, // value for  194
		238, // value for  195
		239, // value for  196
		239, // value for  197
		240, // value for  198
		241, // value for  199
		241, // value for  200
		242, // value for  201
		242, // value for  202
		243, // value for  203
		243, // value for  204
		244, // value for  205
		244, // value for  206
		245, // value for  207
		245, // value for  208
		245, // value for  209
		246, // value for  210
		246, // value for  211
		247, // value for  212
		247, // value for  213
		248, // value for  214
		248, // value for  215
		248, // value for  216
		249, // value for  217
		249, // value for  218
		249, // value for  219
		250, // value for  220
		250, // value for  221
		250, // value for  222
		251, // value for  223
		251, // value for  224
		251, // value for  225
		252, // value for  226
		252, // value for  227
		252, // value for  228
		252, // value for  229
		253, // value for  230
		253, // value for  231
		253, // value for  232
		253, // value for  233
		254, // value for  234
		254, // value for  235
		254, // value for  236
		254, // value for  237
		254, // value for  238
		255, // value for  239
		255, // value for  240
		255, // value for  241
		255, // value for  242
		255, // value for  243
		255, // value for  244
		255, // value for  245
		0, // value for  246
		0, // value for  247
		0, // value for  248
		0, // value for  249
		0, // value for  250
		0, // value for  251
		0, // value for  252
		0, // value for  253
		0, // value for  254
		0 // value for  255
		};

#endif /* MYDATA_H_ */
