/*
 * ringuart.h
 *
 */

#ifndef RINGUART_H_
#define RINGUART_H_
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

//bitsize should be between 2 and 7
#define RINGUART_RX_BUFFER_BITSIZE 7
#define RINGUART_TX_BUFFER_BITSIZE 7
#define RINGUART_BAUD_RATE 19200




void ringuartinit();
void txring_enqueue(uint8_t data); // - am ende einfügen. automatisch senden starten.
uint8_t txring_count(); // - noch zu sendende zeichen im buffer
uint8_t txring_space(); // - noch verfügbarer space im buffer

uint8_t rxring_count(); // - noch zu sendende zeichen im buffer
uint8_t rxring_space(); // - noch verfügbarer space im buffer
uint8_t rxring_dequeue(); // - erstes zeichen entfernen und ausliefern
uint8_t rxring_peek(uint8_t offset); // - kommendes zeichen entfernen
uint8_t rxstatus();
uint8_t rxresetstatus();
uint8_t firstmsglen();
void ring_outchar(uint8_t data); //wrapper
//void ring_outPstr(); //from progmem;
#define ring_space ring_outchar(' ');
#define ring_newline ring_outchar('\n');ring_outchar('\r');

void ring_outbyte(uint8_t data);
void ring_outword(uint16_t data);


#endif /* RINGUART_H_ */
