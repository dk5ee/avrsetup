/*
 * hardware.h
 *
in hardware.c/hardware.h : only low level hardware
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_
#include <avr/io.h>
#include <util/delay.h>
#include "adcint.h"
#include "twilib.h"
#include "ringuart.h"
#include "servos.h"

void hardwareinit();


#endif /* HARDWARE_H_ */
