/*
 * mydata.c
 *
 */

#include "mydata.h"

uint8_t loadbuf(uint8_t strnum, uint8_t bufnum) {
	uint8_t i = 0;

	if (!(bufnum < STRINGBUFCOUNT)) {
		return 0;
	}
	if (!(strnum < STRINGCOUNT)) {
		return 0;
	}
	char *stringPtr ;
	stringPtr = (char *) stringbufs[bufnum];

	for (i = 0; i < MAXSTRINGLEN; i++) {
		stringPtr[i] = 0;
	}
	i = 0;
	const char *parray = (const char*) pgm_read_word (&stringsP[i]);
	for (i = 0; i < MAXSTRINGLEN; i++) {
		uint8_t mbyte = pgm_read_byte(parray[i]);
		if (mbyte == 0) {
			i = MAXSTRINGLEN;
		} else {
			stringPtr[i] = mbyte;
		}
	}
	return 1;
}
uint8_t buf2uart(uint8_t bufnum) {
	if (!(bufnum < STRINGBUFCOUNT)) {
		return 0;
	}
	char *stringPtr ;
	stringPtr =(char*) stringbufs[bufnum];
	while (txring_space() < MAXSTRINGLEN) {
		//do nothing;
	}
	uint8_t i = 0;
	uint8_t mbyte = 0;
	do {
		mbyte = stringPtr[i];
		txring_enqueue(mbyte);
		i++;
	} while ((i < MAXSTRINGLEN) && (mbyte != 0));

	return 1;
}
int16_t getsin(int16_t degree) {
	uint8_t newdeg = 0;
	uint8_t sign = 0;
	while (degree >= 1024)
		degree -= 1024;
	while (degree < 0)
		degree += 1024;
	if (degree < 256) {
		newdeg = degree;
	} else if (degree < 512) {
		newdeg = 511 - degree;
	} else if (degree < 768) {
		newdeg = degree - 512;
		sign = 1;
	} else {
		newdeg = 1023 - degree;
		sign = 1;
	}

	int16_t result = (uint16_t) pgm_read_byte(&mysintable[newdeg]);
	if (newdeg > 128 && result == 0)
		result = 256;
	if (sign == 1)
		result = 0 - result;
	return (result);
}
