/*
 * interface.h
 *
 */

#ifndef INTERFACE_H_
#define INTERFACE_H_

#include "hardware.h"
#include "mydata.h"
#include <avr/eeprom.h>
#include <avr/pgmspace.h>

void init();

uint16_t getadc(uint8_t channel);


#endif /* INTERFACE_H_ */
