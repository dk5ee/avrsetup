/*
 * main.c
 */

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "servos.h"
#include "mymath.h"
#include "myuart.h"
#include "twilib.h"
#include "adcint.h"

uint8_t blinkdata = 0;
void blink() {
	DDRB |= 1 << PB5;
	blinkdata = 1 - blinkdata;
	if (blinkdata) {
		PORTB |= 1 << PB5;
	} else {
		PORTB &= ~(1 << PB5);
	}

}
volatile uint8_t count;
volatile uint8_t c2;

int main() {
	initsin();
	initservos();
	myuart_init();
	TWIinit();
	adcinit();
	adcstart();

	startservos();
	c2 = 0;
	count = 3;
	uint16_t mydeg = 0;
	uint8_t scale = 40;
	while (1) {
		blink();
		_delay_ms(50);
		c2++;
		mydeg += 2;
		if (mydeg > 1024)
			mydeg -= 1024;
		for (int i2 = 0; i2 < 5; i2++)
			setservo(i2, scale * getsin(mydeg + servodegrees[i2]));

		/*	if (c2 > 3 ) {
		 c2 = 0;
		 count++;
		 if (count > 12) {
		 //stopservos();
		 count = 0;
		 }
		 switch (count) {
		 case 0:
		 initservos();
		 break;
		 case 1:

		 setservo(0, -4000);
		 setservo(1, -4000);
		 setservo(4, -4000);
		 setservo(2, -4000);
		 setservo(3, -4000);
		 break;
		 case 2:
		 setservo(0, 4000);
		 setservo(1, 4000);
		 setservo(4, 4000);
		 setservo(2, 4000);
		 setservo(3, 4000);
		 break;
		 case 3:
		 setservo(0, 0000);
		 setservo(1, -4000);
		 setservo(4, -4000);
		 setservo(2, 4000);
		 setservo(3, 4000);
		 break;
		 case 4:
		 setservo(0, -4000);
		 setservo(1, -4000);
		 setservo(4, 4000);
		 setservo(2, 4000);
		 setservo(3, 000);
		 break;
		 case 5:
		 setservo(0, -4000);
		 setservo(1, 4000);
		 setservo(4, 4000);
		 setservo(2, 000);
		 setservo(3, -4000);
		 break;
		 case 6:
		 setservo(0, 4000);
		 setservo(1, 4000);
		 setservo(4, 000);
		 setservo(2, -4000);
		 setservo(3, -4000);
		 break;
		 case 7:
		 setservo(0, 4000);
		 setservo(1, 000);
		 setservo(4, -4000);
		 setservo(2, -4000);
		 setservo(3, 4000);
		 break;
		 case 8:
		 setservo(0, 0000);
		 setservo(1, 4000);
		 setservo(4, 4000);
		 setservo(2, -4000);
		 setservo(3, -4000);
		 break;
		 case 9:
		 setservo(0, 4000);
		 setservo(1, 4000);
		 setservo(4, -4000);
		 setservo(2, -4000);
		 setservo(3, 000);
		 break;
		 case 10:
		 setservo(0, 4000);
		 setservo(1, -4000);
		 setservo(4, -4000);
		 setservo(2, 000);
		 setservo(3, 4000);
		 break;
		 case 11:
		 setservo(0, -4000);
		 setservo(1, -4000);
		 setservo(4, 000);
		 setservo(2, 4000);
		 setservo(3, 4000);
		 break;
		 case 12:
		 setservo(0, -4000);
		 setservo(1, 000);
		 setservo(4, 4000);
		 setservo(2, 4000);
		 setservo(3, -4000);
		 break;

		 default:

		 break;
		 }
		 }*/

	}
	return 0;
}

/*
 * restliche ISRs

 ISR ( INT0_vect ) {}
 ISR ( INT1_vect ) {}
 ISR ( TIMER2_COMP_vect ) {}
 ISR ( TIMER2_OVF_vect ) {}
 ISR ( TIMER1_CAPT_vect ) {}
 ISR ( TIMER1_COMPA_vect ) {}
 ISR ( TIMER1_COMPB_vect ) {}
 ISR ( TIMER1_OVF_vect ) {}
 ISR ( TIMER0_OVF_vect ) {}
 ISR ( SPI_STC_vect ) {}
 ISR ( USART_RXC_vect ) {}
 ISR ( USART_UDRE_vect ) {}
 ISR ( USART_TXC_vect ) {}
 ISR ( ADC_vect ) {}
 ISR ( EE_RDY_vect ) {}
 ISR ( ANA_COMP_vect ) {}
 ISR ( TWI_vect ) {}
 ISR ( SPM_RDY_vect ) {}

 */
