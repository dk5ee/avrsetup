/*
 * servos.c
 */

#include "servos.h"



void initservos(){
	servopins[0]= PB0;
	servopins[1]= PB1;
	servopins[2]= PB2;
	servopins[3]= PB3;
	servopins[4]= PB4;
	SERVODDR |= (1<<PB0) |(1<<PB1) |(1<<PB2) |(1<<PB3) |(1<<PB4) ;

	servocalibs[0] = servomiddle;
	servocalibs[1] = servomiddle;
	servocalibs[2] = servomiddle;
	servocalibs[3] = servomiddle;
	servocalibs[4] = servomiddle;

	servodegrees[0]=0;
	servodegrees[1]=819;
	servodegrees[2]=410;
	servodegrees[3]=205;
	servodegrees[4]=614;

	servopos[0]=0;
	servopos[1]=0;
	servopos[2]=0;
	servopos[3]=0;
	servopos[4]=0;

	servosumtime = 0;
	return;
}
void setservo(uint8_t servo, int16_t pos) {
	cli();
	if (servo<5 ) {
		if ((pos >= servomin) && (pos <= servomax)) {
			servopos[servo]=pos;
		}
	}
	sei();
}
void startservos() {
	servoirqcount=0;
	TCCR1A = 0;
	//prescaler 8 means:
	// @ 16 mhz
	// counting from 0 to 65535 takes 30ms
	TCCR1B = (0<<CS12) | (1<<CS11) | (0<<CS10);
	TIMSK1|= (1<<TOIE1);

}
void stopservos() {
	TIMSK1&= ~ (1<<TOIE1);
}

ISR (TIMER1_OVF_vect) {
	if (servoirqcount==255) {
		//start OR pause has finished
		//set prescaler 1
		TCCR1B = (0<<CS12) | (0<<CS11) | (1<<CS10);
		servosumtime =0; //reset sumtime;
	} else {
		//disable last servo
		uint8_t lastservo=servopins[servoirqcount];
		SERVOPORT &= ~(1<<lastservo);
	}

	servoirqcount++;
	if (servoirqcount>4) {
		//all servos done, do the pause
		servoirqcount=255;
		//set prescaler 8
		TCCR1B = (0<<CS12) | (1<<CS11) | (0<<CS10);
		TCNT1 = servomaxtime - servosumtime;
	} else {
		uint8_t thisservo=servopins[servoirqcount];
		//put this servo high
		SERVOPORT |= (1<<thisservo);
		//calculate value for servo and preload counter
		uint16_t thisservotime= servocalibs[servoirqcount] + servopos[servoirqcount];
		servosumtime += (thisservotime >>3);
		TCNT1 = 65536 - thisservotime;
	}


}
