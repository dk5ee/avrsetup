/*
 * servos.h
 */

#ifndef SERVOS_H_
#define SERVOS_H_

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define SERVOPORT PORTB
#define SERVODDR DDRB

#define servocalib 40000
#define servomiddle 22000
#define servomax 10000
#define servomin -10000
#define servomaxtime 20000

volatile uint8_t servopins[5];
volatile uint16_t servodegrees[5];
volatile uint16_t servocalibs[5];

volatile int16_t servopos[5];
volatile uint16_t servosumtime;
volatile uint8_t servoirqcount;



void initservos();
void setservo(uint8_t servo, int16_t pos);
void startservos();




#endif /* SERVOS_H_ */
