/*
 * mymath.h
 */

#ifndef MYMATH_H_
#define MYMATH_H_
#include <stdint.h>

uint8_t mysin[256];

void initsin();
int16_t getsin(int16_t degree);

#endif /* MYMATH_H_ */
