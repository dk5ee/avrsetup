/*
 */

#ifndef HD44780_H_
#define HD44780_H_

//everything is included here:
#include "constdata.h"

#define HD44780_RS PB0

#define HD44780_E PB1
#define HD44780_PIN4 PD4

#define HD44780_PORT_SIG PORTB
#define HD44780_DDR_SIG DDRB
#define HD44780_PORT_DATA PORTD
#define HD44780_DDR_DATA DDRD

#define HD44780_E_HIGH	PORTB |= (1<<HD44780_E)
#define HD44780_E_LOW	PORTB &= ~(1<<HD44780_E)
#define HD44780_RS_HIGH	PORTB |= (1<<HD44780_RS)
#define HD44780_RS_LOW	PORTB &= ~(1<<HD44780_RS)

//next two functions are only used internally
//void HD44780_short_enable();
//void HD44780_send4bit(uint8_t data);
void HD44780_sendcommand(uint8_t data);
void HD44780_senddata(uint8_t data);
void HD44780_init();
void HD44780_setpos(uint8_t x);
void HD44780_clear();


void HD44780_outword(uint16_t w); //hexadecimal, word -> 4 chars
void HD44780_outbyte(uint8_t a); //hexadecimal, byte -> 2 chars
uint8_t HD44780_outuint(uint16_t wert); //returns count of characters
uint8_t HD44780_outint(int16_t d); //returns count of characters
void HD44780_out99(uint8_t d); //outputs two decimal digits 00..99 for clock
void HD44780_outbin(uint8_t a); //outputs binary, byte -> 8 chars

#endif /* HD44780_H_ */
