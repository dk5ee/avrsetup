/*
 *      this adclib cycles through adc channel 0..15 and remembers the values
 */
#ifndef ADCLIB_H
#define ADCLIB_H
//everything is included here:
#include "constdata.h"

volatile uint16_t adcarray[16];

//adcget fetches stored value, interrupt save
uint16_t adcget(uint8_t channel);

void adcclear();
void adcinit();
void adcstart();
void adcstop();

#endif /*ADCLIB_H*/
