/*
 */

#ifndef ADCKEYS_H_
#define ADCKEYS_H_

//everything is included here:
#include "constdata.h"

#define KEY_VAL_SELECT 	0x0280
#define KEY_VAL_LEFT 	0x019a
#define KEY_VAL_DOWN 	0x0101
#define KEY_VAL_UP 		0x0063
#define KEY_VAL_RIGHT 	0x0000
#define KEY_VAL_NO_KEY 	0x03ff

#define KEY_SELECT 	5
#define KEY_LEFT 	4
#define KEY_DOWN 	3
#define KEY_UP 		2
#define KEY_RIGHT 	1
#define KEY_NO_KEY 	0

uint8_t adckeys_outkey();
uint8_t adckeys_getcommand();
uint8_t adckeys_parsekey();

#endif /* ADCKEYS_H_ */

