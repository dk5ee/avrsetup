/*
 */
#include "constdata.h"

const char hex[16] = "0123456789ABCDEF";



uint16_t getdatabyte(uint8_t type, uint16_t address) {
	switch (type) {
	case 3:
		//read eeprom
		return eeprom_read_word((uint16_t*) (address));
		break;
	case 2:
		//read ADC
		return adcget(address & 0x0f);
		break;
	case 1:
		//read flash data
		return pgm_read_word(address);
		break;
	case 0:
		//get ram address
		return (*((uint16_t*) (address)));
		break;
	default:
		break;
	}
	return 0;
}

