/*
 */

#include "adckeys.h"

volatile uint8_t keypressed = 0;
volatile uint8_t keysent = 0;
uint8_t adckeys_getcommand() {
	if (keysent != keypressed) {
		keysent = keypressed;
		return keypressed;
	}
	return KEY_NO_KEY;
}
uint8_t adckeys_parsekey() {
	uint16_t adc = adcget(0);
	uint8_t oldkey = keypressed;
	keypressed = KEY_RIGHT;
	if (adc > ((KEY_VAL_NO_KEY + KEY_VAL_SELECT) >> 1)) {
		keypressed = KEY_NO_KEY;
	} else if (adc > ((KEY_VAL_SELECT + KEY_VAL_LEFT) >> 1)) {
		keypressed = KEY_SELECT;
	} else if (adc > ((KEY_VAL_LEFT + KEY_VAL_DOWN) >> 1)) {
		keypressed = KEY_LEFT;
	} else if (adc > ((KEY_VAL_DOWN + KEY_VAL_UP) >> 1)) {
		keypressed = KEY_DOWN;
	} else if (adc > ((0x0000 + KEY_VAL_UP) >> 1)) {
		keypressed = KEY_UP;
	}
	if (oldkey != keypressed)
		keysent = KEY_NO_KEY;
	return keypressed;
}

uint8_t adckeys_outkey() {
	switch (adckeys_parsekey()) {
	case KEY_NO_KEY:
		return ' ';
	case KEY_SELECT:
		return '#';
	case KEY_LEFT:
		return 127;
	case KEY_DOWN:
		return 'v';
	case KEY_UP:
		return '^';
	case KEY_RIGHT:
		return 126;
	default:
		return ' ';
	}
}
