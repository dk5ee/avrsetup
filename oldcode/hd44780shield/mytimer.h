/*
 *      timer 2 is used as clock, counts time since initialization.
 */

#ifndef MYTIMER_H_
#define MYTIMER_H_

#include "constdata.h"

volatile uint16_t time1;
volatile uint8_t time_secs;
volatile uint32_t time_up;
volatile uint8_t time_mins;
volatile uint8_t time_hours;
volatile uint16_t time_days;

void timer2init();

#endif /* MYTIMER_H_ */
