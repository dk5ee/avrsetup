/*
 *      hd44780 internal katakana font
 */

#include "katakana.h"

const uint8_t PGMkatakana[] PROGMEM = {
		' ', ' ', ' ', 'W', 'O', '*', 0b10100110, ' ', ' ',		' ', // 1 WO*
		' ', ' ', 'S', 'E', 'N', '*', 0b11111010, ' ', ' ',		' ', // 2 Sen*
		' ', ' ', 'M', 'A', 'N', '*', 0b11111011, ' ', ' ',		' ', // 3 Man*
		' ', ' ', ' ', 'E', 'N', '*', 0b11111100, ' ', ' ',		' ', // 4 En*
		'S', 'O', 'K', 'U', 'O', 'N', 0b10101111, ' ', ' ',		' ', // 5 Sokuon
		'N', 'I', 'G', 'O', 'R', 'I', 0b11011110, ' ', ' ',		' ', // 6 Nigori
		' ', ' ', 'M', 'A', 'R', 'U', 0b11011111, ' ', ' ',		' ', // 7 Maru
		' ', ' ', ' ', ' ', ' ', 'A', 0b10110001, ' ', ' ',		' ', // 8 A
		' ', ' ', ' ', ' ', ' ', 'I', 0b10110010, ' ', ' ',		' ', // 9 I
		' ', ' ', ' ', ' ', ' ', 'U', 0b10110011, ' ', ' ',		' ', // 10 U
		' ', ' ', ' ', ' ', ' ', 'E', 0b10110100, ' ', ' ',		' ', // 11 E
		' ', ' ', ' ', ' ', ' ', 'O', 0b10110101, ' ', ' ',		' ', // 12 O
		' ', ' ', ' ', ' ', 'K', 'A', 0b10110110, ' ', ' ',		' ', // 13 KA
		' ', ' ', ' ', ' ', 'K', 'I', 0b10110111, ' ', ' ',		' ', // 14 KI
		' ', ' ', ' ', ' ', 'K', 'U', 0b10111000, ' ', ' ',		' ', // 15 KU
		' ', ' ', ' ', ' ', 'K', 'E', 0b10111001, ' ', ' ',		' ', // 16 KE
		' ', ' ', ' ', ' ', 'K', 'O', 0b10111010, ' ', ' ',		' ', // 17 KO
		' ', ' ', ' ', ' ', 'S', 'A', 0b10111011, ' ', ' ',		' ', // 18 SA
		' ', ' ', ' ', 'S', 'H', 'I', 0b10111100, ' ', ' ',		' ', // 19 SHI
		' ', ' ', ' ', ' ', 'S', 'U', 0b10111101, ' ', ' ',		' ', // 20 SU
		' ', ' ', ' ', ' ', 'S', 'E', 0b10111110, ' ', ' ',		' ', // 21 SE
		' ', ' ', ' ', ' ', 'S', 'O', 0b10111111, ' ', ' ',		' ', // 22 SO
		' ', ' ', ' ', ' ', 'T', 'A', 0b11000000, ' ', ' ',		' ', // 23 TA
		' ', ' ', ' ', 'C', 'H', 'I', 0b11000001, ' ', ' ',		' ', // 24 CHI
		' ', ' ', ' ', 'T', 'S', 'U', 0b11000010, ' ', ' ',		' ', // 25 TSU
		' ', ' ', ' ', ' ', 'T', 'E', 0b11000011, ' ', ' ',		' ', // 26 TE
		' ', ' ', ' ', ' ', 'T', 'O', 0b11000100, ' ', ' ',		' ', // 27 TO
		' ', ' ', ' ', ' ', 'N', 'A', 0b11000101, ' ', ' ',		' ', // 28 NA
		' ', ' ', ' ', ' ', 'N', 'I', 0b11000110, ' ', ' ',		' ', // 29 NI
		' ', ' ', ' ', ' ', 'N', 'U', 0b11000111, ' ', ' ',		' ', // 30 NU
		' ', ' ', ' ', ' ', 'N', 'E', 0b11001000, ' ', ' ',		' ', // 31 NE
		' ', ' ', ' ', ' ', 'N', 'O', 0b11001001, ' ', ' ',		' ', // 32 NO
		' ', ' ', ' ', ' ', 'H', 'A', 0b11001010, ' ', ' ',		' ', // 33 HA
		' ', ' ', ' ', ' ', 'H', 'I', 0b11001011, ' ', ' ',		' ', // 34 HI
		' ', ' ', ' ', ' ', 'F', 'U', 0b11001100, ' ', ' ',		' ', // 35 FU
		' ', ' ', ' ', ' ', 'H', 'E', 0b11001101, ' ', ' ',		' ', // 36 HE
		' ', ' ', ' ', ' ', 'H', 'O', 0b11001110, ' ', ' ',		' ', // 37 HO
		' ', ' ', ' ', ' ', 'M', 'A', 0b11001111, ' ', ' ',		' ', // 38 MA
		' ', ' ', ' ', ' ', 'M', 'I', 0b11010000, ' ', ' ',		' ', // 39 MI
		' ', ' ', ' ', ' ', 'M', 'U', 0b11010001, ' ', ' ',		' ', // 40 MU
		' ', ' ', ' ', ' ', 'M', 'E', 0b11010010, ' ', ' ',		' ', // 41 ME
		' ', ' ', ' ', ' ', 'M', 'O', 0b11010011, ' ', ' ',		' ', // 42 MO
		' ', ' ', ' ', ' ', 'Y', 'A', 0b11010100, ' ', ' ',		' ', // 43 YA
		' ', ' ', ' ', ' ', 'Y', 'U', 0b11010101, ' ', ' ',		' ', // 44 YU
		' ', ' ', ' ', ' ', 'Y', 'O', 0b11010110, ' ', ' ',		' ', // 45 YO
		' ', ' ', ' ', ' ', 'R', 'A', 0b11010111, ' ', ' ',		' ', // 46 RA
		' ', ' ', ' ', ' ', 'R', 'I', 0b11011000, ' ', ' ',		' ', // 47 RI
		' ', ' ', ' ', ' ', 'R', 'U', 0b11011001, ' ', ' ',		' ', // 48 RU
		' ', ' ', ' ', ' ', 'R', 'E', 0b11011010, ' ', ' ',		' ', // 49 RE
		' ', ' ', ' ', ' ', 'R', 'O', 0b11011011, ' ', ' ',		' ', // 50 RO
		' ', ' ', ' ', ' ', 'W', 'A', 0b11011100, ' ', ' ',		' ', // 51 WA
		' ', ' ', ' ', ' ', ' ', 'N', 0b11011101, ' ', ' ',		' ', // 52 N
		' ', ' ', ' ', 'K', 'Y', 'A', 0b10110111, 0b10101100, ' ',		' ', // 53 Kya
		' ', ' ', ' ', 'K', 'Y', 'U', 0b10110111, 0b10101101, ' ',		' ', // 54 Kyu
		' ', ' ', ' ', 'K', 'Y', 'O', 0b10110111, 0b10101110, ' ',		' ', // 55 Kyo
		' ', ' ', ' ', 'S', 'H', 'A', 0b10111100, 0b10101100, ' ',		' ', // 56 Sha
		' ', ' ', ' ', 'S', 'H', 'U', 0b10111100, 0b10101101, ' ',		' ', // 57 Shu
		' ', ' ', ' ', 'S', 'H', 'O', 0b10111100, 0b10101110, ' ',		' ', // 58 Sho
		' ', ' ', ' ', 'C', 'H', 'A', 0b11000001, 0b10101100, ' ',		' ', // 59 Cha
		' ', ' ', ' ', 'C', 'H', 'U', 0b11000001, 0b10101101, ' ',		' ', // 60 Chu
		' ', ' ', ' ', 'C', 'H', 'O', 0b11000001, 0b10101110, ' ',		' ', // 61 Cho
		' ', ' ', ' ', 'N', 'Y', 'A', 0b11000110, 0b10101100, ' ',		' ', // 62 Nya
		' ', ' ', ' ', 'N', 'Y', 'U', 0b11000110, 0b10101101, ' ',		' ', // 63 Nyu
		' ', ' ', ' ', 'N', 'Y', 'O', 0b11000110, 0b10101110, ' ',		' ', // 64 Nyo
		' ', ' ', ' ', 'H', 'Y', 'A', 0b11001011, 0b10101100, ' ',		' ', // 65 Hya
		' ', ' ', ' ', 'H', 'Y', 'U', 0b11001011, 0b10101101, ' ',		' ', // 66 Hyu
		' ', ' ', ' ', 'H', 'Y', 'O', 0b11001011, 0b10101110, ' ',		' ', // 67 Hyo
		' ', ' ', ' ', 'M', 'Y', 'A', 0b11010000, 0b10101100, ' ',		' ', // 68 Mya
		' ', ' ', ' ', 'M', 'Y', 'U', 0b11010000, 0b10101101, ' ',		' ', // 69 Myu
		' ', ' ', ' ', 'M', 'Y', 'O', 0b11010000, 0b10101110, ' ',		' ', // 70 Myo
		' ', ' ', ' ', 'R', 'Y', 'A', 0b11011000, 0b10101100, ' ',		' ', // 71 Rya
		' ', ' ', ' ', 'R', 'Y', 'U', 0b11011000, 0b10101101, ' ',		' ', // 72 Ryu
		' ', ' ', ' ', 'R', 'Y', 'O', 0b11011000, 0b10101110, ' ',		' ', // 73 Ryo
		' ', ' ', ' ', ' ', 'G', 'A', 0b10110110, 0b11011110, ' ',		' ', // 74 GA
		' ', ' ', ' ', ' ', 'G', 'I', 0b10110111, 0b11011110, ' ',		' ', // 75 GI
		' ', ' ', ' ', ' ', 'G', 'U', 0b10111000, 0b11011110, ' ',		' ', // 76 GU
		' ', ' ', ' ', ' ', 'G', 'E', 0b10111001, 0b11011110, ' ',		' ', // 77 GE
		' ', ' ', ' ', ' ', 'G', 'O', 0b10111010, 0b11011110, ' ',		' ', // 78 GO
		' ', ' ', ' ', ' ', 'Z', 'A', 0b10111011, 0b11011110, ' ',		' ', // 79 ZA
		' ', ' ', ' ', ' ', 'J', 'I', 0b10111100, 0b11011110, ' ',		' ', // 80 JI
		' ', ' ', ' ', ' ', 'Z', 'U', 0b10111101, 0b11011110, ' ',		' ', // 81 ZU
		' ', ' ', ' ', ' ', 'Z', 'E', 0b10111110, 0b11011110, ' ',		' ', // 82 ZE
		' ', ' ', ' ', ' ', 'Z', 'O', 0b10111111, 0b11011110, ' ',		' ', // 83 ZO
		' ', ' ', ' ', ' ', 'D', 'A', 0b11000000, 0b11011110, ' ',		' ', // 84 DA
		' ', ' ', ' ', ' ', 'D', 'E', 0b11000011, 0b11011110, ' ',		' ', // 85 DE
		' ', ' ', ' ', ' ', 'D', 'O', 0b11000100, 0b11011110, ' ',		' ', // 86 DO
		' ', ' ', ' ', ' ', 'B', 'A', 0b11001010, 0b11011110, ' ',		' ', // 87 BA
		' ', ' ', ' ', ' ', 'B', 'I', 0b11001011, 0b11011110, ' ',		' ', // 88 BI
		' ', ' ', ' ', ' ', 'B', 'U', 0b11001100, 0b11011110, ' ',		' ', // 89 BU
		' ', ' ', ' ', ' ', 'B', 'E', 0b11001101, 0b11011110, ' ',		' ', // 90 BE
		' ', ' ', ' ', ' ', 'B', 'O', 0b11001110, 0b11011110, ' ',		' ', // 91 BO
		' ', ' ', ' ', ' ', 'V', 'U', 0b10110011, 0b11011110, ' ',		' ', // 92 VU
		' ', ' ', ' ', ' ', 'P', 'A', 0b11001010, 0b11011111, ' ',		' ', // 93 PA
		' ', ' ', ' ', ' ', 'P', 'I', 0b11001011, 0b11011111, ' ',		' ', // 94 PI
		' ', ' ', ' ', ' ', 'P', 'U', 0b11001100, 0b11011111, ' ',		' ', // 95 PU
		' ', ' ', ' ', ' ', 'P', 'E', 0b11001101, 0b11011111, ' ',		' ', // 96 PE
		' ', ' ', ' ', ' ', 'P', 'O', 0b11001110, 0b11011111, ' ',		' ', // 97 PO
		' ', ' ', ' ', ' ', 'L', 'A', 0b11010111, 0b11011111, ' ',		' ', // 98 LA
		' ', ' ', ' ', ' ', 'L', 'I', 0b11011000, 0b11011111, ' ',		' ', // 99 LI
		' ', ' ', ' ', ' ', 'L', 'U', 0b11011001, 0b11011111, ' ',		' ', // 100 LU
		' ', ' ', ' ', ' ', 'L', 'E', 0b11011010, 0b11011111, ' ',		' ', // 101 LE
		' ', ' ', ' ', ' ', 'L', 'O', 0b11011011, 0b11011111, ' ',		' ', // 102 LO
		' ', ' ', ' ', 'G', 'Y', 'A', 0b10110111, 0b11011110, 0b10101100,		' ', // 103 Gya
		' ', ' ', ' ', 'G', 'Y', 'U', 0b10110111, 0b11011110, 0b10101101,		' ', // 104 Gyu
		' ', ' ', ' ', 'G', 'Y', 'O', 0b10110111, 0b11011110, 0b10101110,		' ', // 105 Gyo
		' ', ' ', ' ', 'B', 'Y', 'A', 0b11001011, 0b11011110, 0b10101100,		' ', // 106 Bya
		' ', ' ', ' ', 'B', 'Y', 'U', 0b11001011, 0b11011110, 0b10101101,		' ', // 107 Byu
		' ', ' ', ' ', 'B', 'Y', 'O', 0b11001011, 0b11011110, 0b10101110,		' ', // 108 Byo
		' ', ' ', ' ', 'P', 'Y', 'A', 0b11001011, 0b11011111, 0b10101100,		' ', // 109 Pya
		' ', ' ', ' ', 'P', 'Y', 'U', 0b11001011, 0b11011111, 0b10101101,		' ', // 110 Pyu
		' ', ' ', ' ', 'P', 'Y', 'O', 0b11001011, 0b11011111, 0b10101110,		' ', // 111 Pyo
		' ', ' ', ' ', ' ', 'Y', 'I', 0b10110010, 0b10101000, ' ',		' ', // 112 Yi
		' ', ' ', ' ', ' ', 'Y', 'E', 0b10110010, 0b10101010, ' ',		' ', // 113 Ye
		' ', ' ', ' ', ' ', 'W', 'A', 0b10110011, 0b10100111, ' ',		' ', // 114 Wa
		' ', ' ', ' ', ' ', 'W', 'I', 0b10110011, 0b10101000, ' ',		' ', // 115 Wi
		' ', ' ', ' ', ' ', 'W', 'U', 0b10110011, 0b10101001, ' ',		' ', // 116 Wu
		' ', ' ', ' ', ' ', 'W', 'E', 0b10110011, 0b10101010, ' ',		' ', // 117 We
		' ', ' ', ' ', ' ', 'W', 'O', 0b10110011, 0b10101011, ' ',		' ', // 118 Wo
		' ', ' ', ' ', 'W', 'Y', 'U', 0b10110011, 0b10101101, ' ',		' ', // 119 Wyu
		' ', ' ', ' ', 'K', 'Y', 'E', 0b10110111, 0b10101010, ' ',		' ', // 120 Kye
		' ', ' ', ' ', 'G', 'Y', 'E', 0b10110111, 0b11011110, 0b10101010,		' ', // 121 Gye
		' ', ' ', ' ', ' ', 'V', 'A', 0b10110011, 0b11011110, 0b10100111,		' ', // 122 Va
		' ', ' ', ' ', ' ', 'V', 'I', 0b10110011, 0b11011110, 0b10101000,		' ', // 123 Vi
		' ', ' ', ' ', ' ', 'V', 'E', 0b10110011, 0b11011110, 0b10101010,		' ', // 124 Ve
		' ', ' ', ' ', ' ', 'V', 'O', 0b10110011, 0b11011110, 0b10101011,		' ', // 125 Vo
		' ', ' ', ' ', 'V', 'Y', 'E', 0b10110011, 0b11011110, 0b10101000,		0b10101010, // 126 Vye
		' ', ' ', ' ', 'V', 'Y', 'A', 0b10110011, 0b11011110, 0b10101100, ' ', // 127 Vya
		' ', ' ', ' ', 'V', 'Y', 'U', 0b10110011, 0b11011110, 0b10101101, ' ', // 128 Vyu
		' ', ' ', ' ', 'V', 'Y', 'O', 0b10110011, 0b11011110, 0b10101110, ' ', // 129 Vyo
		' ', ' ', ' ', 'K', 'W', 'A', 0b10111000, 0b10100111, ' ', ' ', // 130 Kwa
		' ', ' ', ' ', 'K', 'W', 'I', 0b10111000, 0b10101000, ' ', ' ', // 131 Kwi
		' ', ' ', ' ', 'K', 'W', 'E', 0b10111000, 0b10101010, ' ', ' ', // 132 Kwe
		' ', ' ', ' ', 'K', 'W', 'O', 0b10111000, 0b10101011, ' ', ' ', // 133 Kwo
		' ', ' ', ' ', 'G', 'W', 'A', 0b10111000, 0b11011110, 0b10100111, ' ', // 134 Gwa
		' ', ' ', ' ', 'G', 'W', 'I', 0b10111000, 0b11011110, 0b10101000, ' ', // 135 Gwi
		' ', ' ', ' ', 'G', 'W', 'E', 0b10111000, 0b11011110, 0b10101010, ' ', // 136 Gwe
		' ', ' ', ' ', 'G', 'W', 'O', 0b10111000, 0b11011110, 0b10101011, ' ', // 137 Gwo
		' ', ' ', ' ', 'S', 'H', 'E', 0b10111100, 0b10101010, ' ', ' ', // 138 She
		' ', ' ', ' ', ' ', 'J', 'E', 0b10111100, 0b11011110, 0b10101010, ' ', // 139 Je
		' ', ' ', ' ', 'S', 'W', 'I', 0b10111101, 0b10101000, ' ', ' ', // 140 Swi
		' ', ' ', ' ', 'Z', 'W', 'I', 0b10111101, 0b11011110, 0b10101000, ' ', // 141 Zwi
		' ', ' ', ' ', 'C', 'H', 'E', 0b11000001, 0b10101010, ' ', ' ', // 142 Che
		' ', ' ', ' ', 'T', 'S', 'A', 0b11000010, 0b10100111, ' ', ' ', // 143 Tsa
		' ', ' ', ' ', 'T', 'S', 'I', 0b11000010, 0b10101000, ' ', ' ', // 144 Tsi
		' ', ' ', ' ', 'T', 'S', 'E', 0b11000010, 0b10101010, ' ', ' ', // 145 Tse
		' ', ' ', ' ', 'T', 'S', 'O', 0b11000010, 0b10101011, ' ', ' ', // 146 Tso
		' ', ' ', 'T', 'S', 'Y', 'U', 0b11000010, 0b10101101, ' ', ' ', // 147 Tsyu
		' ', ' ', ' ', ' ', 'T', 'I', 0b11000011, 0b10101000, ' ', ' ', // 148 Ti
		' ', ' ', ' ', ' ', 'T', 'U', 0b11000100, 0b10101001, ' ', ' ', // 149 Tu
		' ', ' ', ' ', 'T', 'Y', 'U', 0b11000011, 0b10101101, ' ', ' ', // 150 Tyu
		' ', ' ', ' ', ' ', 'D', 'I', 0b11000011, 0b11011110, 0b10101000, ' ', // 151 Di
		' ', ' ', ' ', ' ', 'D', 'U', 0b11000100, 0b11011110, 0b10101001, ' ', // 152 Du
		' ', ' ', ' ', 'D', 'Y', 'U', 0b11000011, 0b11011110, 0b10101101, ' ', // 153 Dyu
		' ', ' ', ' ', 'N', 'Y', 'E', 0b11000110, 0b10101010, ' ', ' ', // 154 Nye
		' ', ' ', ' ', 'H', 'Y', 'E', 0b11001011, 0b10101010, ' ', ' ', // 155 Hye
		' ', ' ', ' ', 'B', 'Y', 'E', 0b11001011, 0b11011110, 0b10101010, ' ', // 156 Bye
		' ', ' ', ' ', 'P', 'Y', 'E', 0b11001011, 0b11011111, 0b10101010, ' ', // 157 Pye
		' ', ' ', ' ', ' ', 'F', 'A', 0b11001100, 0b10100111, ' ', ' ', // 158 Fa
		' ', ' ', ' ', ' ', 'F', 'I', 0b11001100, 0b10101000, ' ', ' ', // 159 Fi
		' ', ' ', ' ', ' ', 'F', 'E', 0b11001100, 0b10101010, ' ', ' ', // 160 Fe
		' ', ' ', ' ', ' ', 'F', 'O', 0b11001100, 0b10101011, ' ', ' ', // 161 Fo
		' ', ' ', ' ', 'F', 'Y', 'A', 0b11001100, 0b10101100, ' ', ' ', // 162 Fya
		' ', ' ', ' ', 'F', 'Y', 'U', 0b11001100, 0b10101101, ' ', ' ', // 163 Fyu
		' ', ' ', ' ', 'F', 'Y', 'O', 0b11001100, 0b10101110, ' ', ' ', // 164 Fyo
		' ', ' ', ' ', 'F', 'Y', 'E', 0b11001100, 0b10101000, 0b10101010, ' ', // 165 Fye
		' ', ' ', ' ', ' ', 'H', 'U', 0b11001110, 0b10101001, ' ', ' ', // 166 Hu
		' ', ' ', ' ', 'M', 'Y', 'E', 0b11010000, 0b10101010, ' ', ' ', // 167 Mye
		' ', ' ', ' ', 'R', 'Y', 'E', 0b11011000, 0b10101010, ' ', ' ' // 168 Rye

		};/* */

uint8_t readkata(uint8_t no, uint8_t info) {
	if ((no > 167) || (info > 9))
		return ' ';
	uint16_t index = no;
	index *= 10;

	index += info;
	return pgm_read_byte(&PGMkatakana[index]);
}

void katakanadisp(uint16_t a) {
	//HD44780_clear();

	while (a > 167) a -= 168;
	HD44780_outbyte(a);
	HD44780_senddata(' ');
	HD44780_senddata('K');
	HD44780_senddata('A');
	HD44780_senddata('T');
	HD44780_senddata('A');
	HD44780_senddata('K');
	HD44780_senddata('A');
	HD44780_senddata('N');
	HD44780_senddata('A');
	HD44780_senddata(' ');
	HD44780_senddata(0b10110110);
	HD44780_senddata(0b11000000);
	HD44780_senddata(0b10110110);
	HD44780_senddata(0b11000101);

	HD44780_setpos(16);
	HD44780_senddata(readkata(a, 0));
	HD44780_senddata(readkata(a, 1));
	HD44780_senddata(readkata(a, 2));
	HD44780_senddata(readkata(a, 3));
	HD44780_senddata(readkata(a, 4));
	HD44780_senddata(readkata(a, 5));
	HD44780_senddata(' ');
	HD44780_senddata(0b01111111);//arrow
	HD44780_senddata(0b01111110);//arrow
	HD44780_senddata(' ');
	HD44780_senddata(readkata(a, 6));
	HD44780_senddata(readkata(a, 7));
	HD44780_senddata(readkata(a, 8));
	HD44780_senddata(readkata(a, 9));


}
