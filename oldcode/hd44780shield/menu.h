/*
 */

#ifndef MENU_H_
#define MENU_H_
#include "constdata.h"

volatile uint8_t menuselected;
volatile uint8_t menustatus;
volatile uint8_t menueditpos;
volatile uint16_t menuposition;

void menuinit();
void menuparse();
void menucontent(); //show content of menupoint
void menushow(uint8_t menutitle); //show menutitle

#endif /* MENU_H_ */
