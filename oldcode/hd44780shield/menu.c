/*
 */

#include "menu.h"
void menuinit() {
	menuselected = 0;
	menustatus = 0;
	menuposition = 0;
}


const char menu1[8] PROGMEM = "Katakana";
const char menu2[8] PROGMEM = "example ";
const char menu3[8] PROGMEM = "TIME    ";
const char menu4[8] PROGMEM = "ADCVALUE";
const char menu5[8] PROGMEM = "CHARS   ";
const char menu6[8] PROGMEM = "RAM     ";
const char menu7[8] PROGMEM = "EEPROM  ";
const char menu8[8] PROGMEM = "FLASH   ";
const char* menus[8] =
		{ menu1, menu2, menu3, menu4, menu5, menu6, menu7, menu8 };

const char helloworld[16] PROGMEM = " HELLO WORLD!   ";

void menushow(uint8_t menutitle) {
	uint8_t a = 0;
	const char* menustring = menus[menutitle];
	for (a = 0; a < 8; a++) {
		uint8_t b = pgm_read_byte(menustring + a);
		HD44780_senddata(b);
	}
}

void menufun1(uint16_t data) {
	HD44780_outword(data);
	HD44780_senddata(' ');
	HD44780_senddata(data >> 8);
	HD44780_senddata(data & 0xff);
	HD44780_senddata('|');
	//output decimal value and fill with blanks
	uint8_t a= HD44780_outuint(data);
	uint8_t b=0;
	for (b= 6;b>a; b--) HD44780_senddata(' ');

}

void menucontent() {
	HD44780_setpos(0);
	if (menuselected != 0) {
		menushow(menuselected);
		HD44780_senddata(' ');
		HD44780_outword(menuposition);
		HD44780_senddata(' ');
		HD44780_outbyte(menueditpos);
		HD44780_setpos(16);
	}
	uint8_t merker = 0;
	switch (menuselected) {
	case 0: //katakana
		katakanadisp(menuposition);
		break;
	case 1: //hello world
		for (merker = 0; merker < 16; merker++) {
			HD44780_senddata(pgm_read_byte(helloworld + merker));
		}
		break;
	case 2: //time
		HD44780_out99(time_hours);
		HD44780_senddata(':');
		HD44780_out99(time_mins);
		HD44780_senddata(':');
		HD44780_out99(time_secs);
		HD44780_senddata(' ');
		HD44780_senddata(' ');
		HD44780_outword(time1);
		break;
	case 3: //adc
		menufun1(getdatabyte(2, menuposition));
		break;
	case 5: //ram
		menufun1(getdatabyte(0, menuposition));
		break;
	case 6: //eeprom
		menufun1(getdatabyte(3, menuposition));
		break;
	case 4: //chars
		merker = menuposition & 0xff;
		HD44780_senddata(merker);
		HD44780_senddata(' ');
		HD44780_outbyte(merker);
		HD44780_senddata(' ');
		HD44780_outbin(merker);
		HD44780_senddata(' ');
		HD44780_outuint(merker);
		HD44780_senddata(' ');
		HD44780_senddata(' ');
		HD44780_senddata(' ');
		break;
	case 7: //flash
		menufun1(getdatabyte(1, menuposition));
		break;


	default:
		break;
	}

}

void menuparse() {
	if (menustatus == 0) {
		switch (adckeys_getcommand()) {
		case KEY_NO_KEY:
			break;
		case KEY_SELECT:
			menustatus = 1;
			menuposition = 0;
			menueditpos = 0;
			HD44780_clear();
			break;
		case KEY_LEFT:
		case KEY_UP:
			menuselected--;
			if (menuselected > 0x07) {
				menuselected = 0x07;
			}
			break;
		case KEY_DOWN:
		case KEY_RIGHT:
			menuselected++;
			if (menuselected > 0x07) {
				menuselected = 0x00;
			}
			break;
		default:
			break;

		}
	} else {
		switch (adckeys_getcommand()) {
		case KEY_NO_KEY:
			break;
		case KEY_SELECT:
			menustatus = 0;
			HD44780_clear();
			break;
		case KEY_LEFT:
			menueditpos++;
			if (menueditpos > 3)
				menueditpos = 0;
			break;
		case KEY_DOWN:
			menuposition -= (1 << (menueditpos * 4));
			break;
		case KEY_UP:
			menuposition += (1 << (menueditpos * 4));
			break;
		case KEY_RIGHT:
			menueditpos--;
			if (menueditpos > 3)
				menueditpos = 3;
			break;
		default:
			break;
		}
	}

}
