/*
 *
 *      some code for atmega328 and arduino lcd keypad shield.
 *
 *      shield display is configured in hd44780.h
 *      shield keypad is configured in adckeys.h
 */

//every file is included here:
#include "constdata.h"


#define ENABLEGREETING

#ifdef ENABLEGREETING
//this data is stored in ram
//todo: move to PROGMEM
const char greeting[] = "  HACKERSPACE        BREMEN     ";
const char greeting2[] = "ArduinoLcdKeyPadDemo by bedah.de";

void dogreeting() {
	uint8_t a = 0;
	HD44780_clear();
	for (a = 0; a < 32; a++) {
		HD44780_setpos(a);
		HD44780_senddata(greeting[a]);
		_delay_ms(40);
	}
	_delay_ms(140);

	for (a = 0; a < 32; a++) {
		HD44780_setpos(a);
		HD44780_senddata(greeting2[a]);
		_delay_ms(40);
	}
	_delay_ms(440);

	for (a = 0; a < 32; a++) {
		HD44780_setpos(a);
		HD44780_senddata(0xff);
		_delay_ms(40);
	}
	_delay_ms(140);
	HD44780_clear();
}
#endif

void init() {
	timer2init();
	adcinit();
	adcclear();
	adcstart();
	HD44780_init();
	menuinit();
#ifdef ENABLEGREETING
	dogreeting();
#endif
}

int main(void) {
	init();
	while (1) {
		menuparse(); //do the menu stuff and key handling
		if (menustatus) {
			adckeys_outkey(); //trigger key readings
			menucontent();
		} else {
			HD44780_setpos(0);
			HD44780_senddata(adckeys_outkey()); //show pressed key
			HD44780_setpos(2);
			menushow(menuselected);
		}
	}
}

