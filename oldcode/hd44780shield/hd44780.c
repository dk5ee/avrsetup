/*
 * inits hd44780 display 2x16 characters
 * set for:
 * LCD Keypad Shield For Arduino
 * http://www.dfrobot.com/index.php?route=product/product&product_id=51
 */

#include "hd44780.h"



void HD44780_short_enable() {
	HD44780_E_HIGH;
	_delay_us(2);
	HD44780_E_LOW;
}
void HD44780_send4bit(uint8_t data) {
	//wir senden nur untere 4 bits..
	data &= 0x0F;
	HD44780_PORT_DATA &= ~(0xF << HD44780_PIN4); //bits löschen
	HD44780_PORT_DATA |= data << HD44780_PIN4;
	HD44780_short_enable();
}
void HD44780_sendcommand(uint8_t data) {
	HD44780_RS_LOW;
	HD44780_send4bit(data >> 4);
	HD44780_send4bit(data);
	_delay_us(100);
}
void HD44780_senddata(uint8_t data) {
	HD44780_RS_HIGH;
	HD44780_send4bit(data >> 4);
	HD44780_send4bit(data);
	_delay_us(100);
}

void HD44780_init() {
	//init pins and set to zero
	HD44780_DDR_DATA |= 0xF << HD44780_PIN4; //data 0.. data 3
	HD44780_PORT_DATA &= ~(0xF << HD44780_PIN4);
	HD44780_DDR_SIG |= 1 << HD44780_E; //enable
	HD44780_E_LOW;
	HD44780_DDR_SIG |= 1 << HD44780_RS; //register select
	HD44780_RS_LOW;
	//wait for boot..
	_delay_ms(155);
	HD44780_PORT_DATA |= 3 << HD44780_PIN4;
	HD44780_short_enable();
	_delay_ms(15);
	HD44780_PORT_DATA |= 3 << HD44780_PIN4;
	HD44780_short_enable();
	_delay_ms(4);
	HD44780_PORT_DATA |= 3 << HD44780_PIN4;
	HD44780_short_enable();
	_delay_ms(15);

	HD44780_send4bit(2); //enable 4 bit
	_delay_ms(1);
	HD44780_sendcommand(0x28); //set function: 2 row display
	_delay_ms(1);

	HD44780_sendcommand(0x08 + 0x04); //+ 0x04 + 0x02 + 0x01); //set display:
	HD44780_sendcommand(0x04 + 0x02 + 0x00);
	uint8_t a = 0;
	HD44780_sendcommand(0x40);
	for (a = 0; a < 64; a++) {
		HD44780_senddata(0);
	}
	HD44780_clear();
}
void HD44780_setpos(uint8_t x) {
	if ((x & 0x1f) > 15) {
		HD44780_sendcommand(0x80 + 0x40 + (x & 0x0F));
	} else {
		HD44780_sendcommand(0x80 + 0x00 + (x & 0x0F));
	}
}
void HD44780_clear() {
	HD44780_sendcommand(0x01);
	_delay_ms(2);
}

void HD44780_outword(uint16_t w) {
//	HD44780_senddata('0');
//	HD44780_senddata('x');
	HD44780_senddata(hex[(w >> 12) & 15]);
	HD44780_senddata(hex[(w >> 8) & 15]);
	HD44780_senddata(hex[(w >> 4) & 15]);
	HD44780_senddata(hex[w & 15]);

}

void HD44780_outbyte(uint8_t a) {
//	HD44780_senddata('0');
//	HD44780_senddata('x');
	HD44780_senddata(hex[(a >> 4) & 15]);
	HD44780_senddata(hex[a & 15]);
}
uint8_t HD44780_outuint(uint16_t wert) {
	uint16_t laufend = wert; //diese variable wird vermutlich vom compiler wegoptimiert
	uint8_t merker = 0;
	uint8_t charcount = 1;
	uint8_t erstes = 0; // um führende nullen zu eliminieren
	while (laufend >= 10000) {
		laufend -= 10000;
		merker++;
		erstes = 1;
	}
	if (erstes) {
		charcount++;
		HD44780_senddata((uint8_t) merker + '0');
	}
	merker = 0;
	while (laufend >= 1000) {
		laufend -= 1000;
		merker++;
		erstes = 1;
	}
	if (erstes) {
		charcount++;
		HD44780_senddata((uint8_t) merker + '0');
	}
	merker = 0;
	while (laufend >= 100) {
		laufend -= 100;
		merker++;
		erstes = 1;
	}
	if (erstes) {
		HD44780_senddata((uint8_t) merker + '0');
	}
	merker = 0;
	while (laufend >= 10) {
		laufend -= 10;
		merker++;
		erstes = 1;
	}
	if (erstes) {
		charcount++;
		HD44780_senddata((uint8_t) merker + '0');
	}
	//auf jeden fall letzte ziffer auspucken
	HD44780_senddata((uint8_t) laufend + '0');
	return charcount;

}
uint8_t HD44780_outint(int16_t d) {
	if (d < 0) {
		d = 0 - d;
		HD44780_senddata('-');
		return (HD44780_outuint(d) + 1);
	} else
		return HD44780_outuint(d);
}
void HD44780_outbin(uint8_t a) {
	for (int i = 8; i > 0; i--)
		if (a >> (i - 1) & 1)
			HD44780_senddata('1');
		else
			HD44780_senddata('0');

}
void HD44780_out99(uint8_t d) {
	if (d > 99) {
		HD44780_senddata('x');
		HD44780_senddata('x');
	} else {
		uint8_t b = 0;
		while (d > 9) {
			d -= 10;
			b++;
		}
		HD44780_senddata(b + '0');
		HD44780_senddata(d + '0');
	}
}
