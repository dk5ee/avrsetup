/*
 */

#ifndef CONSTDATA_H_
#define CONSTDATA_H_

//#include <avr/io.h> // made problems, do it the hard way:
//atmega328p only:
#define _AVR_IO_H_ //disable inclusion of avr/io.h
#include <avr/sfr_defs.h>
#include <avr/iom328p.h>
#include <avr/portpins.h>
#include <avr/common.h>
#include <avr/version.h>
#include <avr/fuse.h>
#include <avr/lock.h>
//end of hard way

#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/eeprom.h>

#include "adclib.h"
#include "hd44780.h"
#include "adckeys.h"
#include "mytimer.h"
#include "menu.h"
#include "katakana.h"

const char hex[16];


//wrapper for dumping internal values
uint16_t getdatabyte(uint8_t type, uint16_t address);

#endif /* CONSTDATA_H_ */
