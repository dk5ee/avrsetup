/*
 */

#include "mytimer.h"

volatile uint16_t time1 = 0;
volatile uint8_t time_secs = 0;
volatile uint32_t time_up = 0; //unused
volatile uint8_t time_mins = 0;
volatile uint8_t time_hours = 0;
volatile uint16_t time_days = 0;


void timer2init() {
	TCCR2A = 0;
	//TCCR2B = (0<<CS22)|(0<<CS21)|(1<<CS20); //no prescaler
	TCCR2B = (0 << CS22) | (1 << CS21) | (0 << CS20); //prescaler 8

	//enable TOV2 interrupt
	TIMSK2 = (1 << TOIE2);
}

ISR(TIMER2_OVF_vect) {
	//todo : preload value? for example 1600000÷1024 = 125² - more cycles saved, more accurate
	time1++;
	//if (time1==62500) { //<-- for prescaler 1
	if (time1 == 7812) { //<-- for prescaler 8, faster
		time1 = 0;
		time_secs++;
		time_up++;
		if (time_secs == 60) {
			time_mins++;
			time_secs = 0;
			if (time_mins == 60) {
				time_hours++;
				time_mins = 0;
				if (time_hours == 24) {
					time_days++; //overflows after 65k days, ~ 179 years
				}
			}
		}
	}

}
