#ifndef ADCLIB_H
#define ADCLIB_H

#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>

volatile uint16_t adcarray[16];

uint16_t adcget(uint8_t channel);
void adcclear();
void adcinit();
void adcstart();


#endif /*ADCLIB_H*/
