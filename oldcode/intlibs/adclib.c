#include "adclib.h"
#include <avr/interrupt.h>




uint16_t adcget(uint8_t channel) {
    cli();
    uint16_t i=adcarray[channel];
    sei();
    return i;
}

void adcclear() {
 uint8_t i;
 for (i=0;i<16;i++) {
	adcarray[i]=0;
}
}
void adcinit() {
     // Taktfrequenz (F_CPU/128) 
    ADCSRA = (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0) | (1<<ADIE) | (1<<ADSC); 
   // ADMUX = REF_VCC;
}
void adcstart() {
 ADCSRA |= (1<<ADSC);
}

ISR(ADC_vect) {
    uint16_t value = ADC; //readout adc value
    uint8_t channel = ADMUX & 7; //get chosen channel
   // uint8_t pd1 = PORTD & ; //später
    adcarray[channel
 //	+(pd1<<3)
		]=value;
    // Zähle von 0-7 und schalte dann PB4 bei Überlauf um.
//später
//    if (++channel > 7) {
//        PORTD ^= (1<<4);
//        channel=0;
//    }
    // Schalte zum nächsten Kanal
    channel++;
    ADMUX=(ADMUX & ~7) | channel;
    // Starte nächste Messung
    ADCSRA |= (1<<ADSC); 
}
