/*
 * timer0.h
 *
 */

#ifndef TIMER0_H_
#define TIMER0_H_
extern volatile uint8_t blinkstatus;
extern volatile uint8_t blinkcounter1; //loop 80->0
extern volatile uint16_t blinkcounter2; //wiederholungen
extern volatile uint8_t c3 ; //low timer0
extern volatile uint8_t c4 ; //high timer0
extern volatile uint8_t c5 ; //high timer0
extern volatile uint8_t c6 ; //high timer0
uint8_t sendstatus(uint8_t status);
void timer0_init();
ISR(TIMER0_OVF_vect );
#endif /* TIMER0_H_ */
