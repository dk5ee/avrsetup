#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "servo4017.h"


void setServo4017Counter(uint8_t number, uint16_t value) {
	if (number < SERVO4017COUNTS) {
		cli();
		servo4017Counters[number] = value;
		sei();
	}
}
void initServo4017Counter() {
	SERVO4017DIR |= (1 << CLOCKPIN);
	uint8_t c = 0;
	for (c = 0; c < 20; c++)
		setServo4017Counter(c, 1000);

	//0 => no prescaler
	TCCR1B |= (0 << CS12) | (0 << CS11) | (0 << CS10);
	//Overflow-Interrupt aktiviert
	TIMSK |= (1 << TOIE1);
	TCNT1 = servo4017Counters[0];
	nextServo4017Value = servo4017Counters[1];
	thisServo4017Count = 2;
}
void startServo4017Counter() {
	TIMSK |= (1 << TOIE1);

	INITSERVO4017

	SERVO4017STARTRESET

	SERVO4017ENABLE

	SERVO4017CLOCKLOW

	SERVO4017STOPRESET

	//
	SERVO4017DIR |= (1 << ENABLEPIN);
	//
	sei();
	// Enable global interrupts
}
void stopServo4017Counter() {
	TIMSK &= ~(1 << TOIE1);
}

ISR ( TIMER1_OVF_vect ) {
	// Vorfüllen des Timers
	TCNT1 = nextServo4017Value;

	PORTB ^= (1 << 0); // Toggle a LED at pb0

	//preload next value
	nextServo4017Value = servo4017Counters[thisServo4017Count];
	thisServo4017Count++;
	if (thisServo4017Count > SERVO4017COUNTS)
		thisServo4017Count = 0;

}
