/*
 * servo4017.h
 *
 */

#ifndef SERVO4017_H_
#define SERVO4017_H_

#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
/*
 * 74hc4017
 * pin 1	Q5
 * pin 2	Q1
 * pin 3	Q0
 * pin 4	Q2
 * pin 5	Q6
 * pin 6	Q7
 * pin 7	Q3
 * pin 8	GND
 * pin 9	Q8
 * pin 10	Q4
 * pin 11	Q9
 * pin 12	~Q5-9
 * pin 13	~CP1 - clock on falling edge
 * pin 14	CP0 - clock on rising edge
 * pin 15	MR - Rmaster reset input (active HIGH)
 * pin 16	Vcc
 *
 * Q[0:9] 3, 2, 4, 7, 10, 1, 5, 6, 9, 11 decoded output
 */

#define CLOCKPIN PB1  // cp0, clock on rising edge
#define RESETPIN PB2  // master reset,
#define ENABLEPIN PB3 //on 74hc4017 -> ~cp1
#define SERVO4017PORT PORTB
#define SERVO4017DIR DDRB
#define INITSERVO4017 SERVO4017PORT |= (1<<CLOCKPIN) |(1<<RESETPIN) |(1<<ENABLEPIN);

#define SERVO4017CLOCKHIGH SERVO4017PORT |= (1<<CLOCKPIN);
#define SERVO4017CLOCKLOW SERVO4017PORT &= ~(1<<CLOCKPIN);
#define SERVO4017ENABLE SERVO4017PORT &= ~(1<<ENABLEPIN);
#define SERVO4017DISSABLE SERVO4017PORT |= (1<<ENABLEPIN);
#define SERVO4017STARTRESET SERVO4017PORT |= (1<<RESETPIN);
#define SERVO4017STOPRESET SERVO4017PORT &= ~(1<<RESETPIN);

#define SERVO4017COUNTS 20

volatile uint16_t nextServo4017Value; //holds next value for fastestest preload
volatile uint8_t thisServo4017Count;
volatile uint16_t servo4017Counters[SERVO4017COUNTS];

void setServo4017Counter(uint8_t number, uint16_t value);
void initServo4017Counter();
void startServo4017Counter();
void stopServo4017Counter();


#endif /* SERVO4017_H_ */
