/*
 * timer0.c
 *
 */
#include <avr/interrupt.h>

#include "timer0.h"

volatile uint8_t c3 = 0; //low timer0
volatile uint8_t c4 = 0; //high timer0
volatile uint8_t c5 = 0; //high timer0
volatile uint8_t c6 = 0; //high timer0
#define blinkmax 4096;
volatile uint8_t blinkstatus = 0b11100100;
volatile uint8_t blinkcounter1 = 0; //loop 80->0
volatile uint16_t blinkcounter2 = 0; //wiederholungen

void timer0_init() {
	//DISSABLE timer0 nicht nötig
	//TIMSK &= ~(1 << TOIE0);

	// clock setzen -> timer starten
	TCCR0 |= CS01; //  seite 72
	TCCR0 |= CS00; //  seite 72

	//ENABLE timer0
	TIMSK |= (1 << TOIE0);

}

uint8_t sendstatus(uint8_t status) {
	if (blinkcounter1 > 0) {
		return (0); //noch am senden
	} else {
		blinkstatus = status;
		blinkcounter1 = 80;
		return (1);
	}
}
ISR(TIMER0_OVF_vect )
{

//	TCNT0 = 0;
	c3++;



	if (c3 == 0)
		c4++;
}
