#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#define F_CPU 16000000UL  // Systemtakt in Hz - Definition als unsigned long beachten
#define USART_BAUDRATE 31250UL      // Baudrate
#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)   // clever runden
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)

uint16_t NOTE_TABLE[128] = { 268, 284, 301,
		318,
		337,
		358, // 1st Octave C-0
		379, 401, 425, 451, 477, 506, 536, 568, 602,
		637,
		675,
		716, // 2nd Octave C-1
		758, 803, 851, 902, 955, 1012, 1072, 1136, 1204,
		1275,
		1351,
		1432, // 3rd Octave C-2
		1517, 1607, 1703, 1804, 1911, 2025, 2145, 2273, 2408,
		2551,
		2703,
		2864, // 4th Octave C-3
		3034, 3215, 3406, 3608, 3823, 4050, 4291, 4547, 4817,
		5103,
		5407,
		5728, // 5th Octave C-4
		6069, 6430, 6812, 7217, 7647, 8101, 8583, 9094, 9634,
		10207,
		10814,
		11457, // 6th Octave C-5
		12139, 12860, 13625, 14435, 15294, 16203, 17167, 18188, 19269, 20415,
		21629,
		22915, // 7th Octave C-6
		24278, 25721, 27251, 28871, 30588, 32407, 34334, 36376, 38539, 40830,
		43258, 45830, // 8th Octave C-7
		48556, 51443, 54502, 57743, 61176, 64814 };

uint8_t MIDI_CHANNEL=0;
uint8_t MIDI_STACK[128]; // FIFO for MIDI-In Bytes
uint8_t MIDI_STACK_Bytes=0; // # of Bytes in FIFO
uint8_t MIDI_STACK_Write=0; // Byte position to write next
uint8_t MIDI_STACK_Read = 0;  // Byte position to read next



uint8_t GetMIDIByte(void)
        {
        uint8_t MIDI_Byte = 0;                                                    // Data byte read from FIFO

        if (MIDI_STACK_Bytes > 0)                                                       // pull data only if the FIFO is not empty
                {
                MIDI_STACK_Read++;                                                      // increase read pointer
                MIDI_STACK_Read &= 127;                                                 // and normalize to max. 128 bytes
                MIDI_Byte = MIDI_STACK[MIDI_STACK_Read];                                // get byte from the FIFO queue
                MIDI_STACK_Bytes--;                                                     // decrease byte counter
                }
        return (MIDI_Byte);
        }



void initUART(){
   UCSRB |= (1 << RXEN) | (1 << TXEN)|  (1 << RXCIE);   // Turn on the transmission and reception circuitry
     // Enable the USART Recieve Complete interrupt (USART_RXC)
   UCSRC |= (1 << URSEL) | (1 << UCSZ0) | (1 << UCSZ1); // Use 8-bit character sizes

   UBRRL = BAUD_PRESCALE; // Load lower 8-bits of the baud rate value into the low byte of the UBRR register
   UBRRH = (BAUD_PRESCALE >> 8); // Load upper 8-bits of the baud rate value into the high byte of the UBRR register

   sei(); // Enable the Global Interrupt Enable flag so that interrupts can be processed
}

void initsid() {
	DDRB = 0xff;
	PORTB = 1; //auf read setzen
	DDRD = 0xff;
	PORTD = 128;
	DDRC = 0xff;
	PORTC = 0;
	//_delay_ms(500); //dem sid eine halbe sekunde zeit geben
}

void SIDout(uint8_t addi, uint8_t data) {
	uint8_t b = (data & 3) << 1; //unterste zwei bit von data, -> b1 und b2
	uint8_t c = (data >> 2) & 63; //obere 6 bit von data ->c0..c5
	uint8_t d = (addi & 31) << 2; //5 bit addi -> d2..d6
	PORTD = d + 128; //cs setzen
	PORTB = b;
	PORTC = c;
	PORTD = d; //cs off -> daten bitte annehmen

	//nun: 16 takte warten = 1 takt @ 1mhz
	asm volatile (
			"nop" "\n\t" //1
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t" //16
	);
	PORTD = d + 128; //cs wieder setzen
	PORTB = b + 1; //auf read setzen
	asm volatile (
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
			"nop" "\n\t"
	);
}
void initbank(uint8_t bank) {
	if (bank < 3) {
		SIDout(bank * 7 + 2, 0);
		SIDout(bank * 7 + 3, 8); //pulsewide 50%
		SIDout(bank * 7 + 5, 14 * 16 + 14); //attack & decay
		SIDout(bank * 7 + 6, 10 * 16 + 14); //sustain& release
		SIDout(bank * 7 + 4, 16); //dreiecksschwingung
	}
}
void clearsid() {
	uint8_t i = 0;
	for (i = 0; i < 21; i++)
		SIDout(i, 0);
	initbank(0);
	initbank(1);
	initbank(2);
	//setfilter(0x080, 15, 0,7, 3);
}
void ringmodtest() {
	clearsid();
	SIDout(1, 100); //freq high chan 1;
	SIDout(5, 219); //a=13;d=11;
	SIDout(15, 28); //frequenz high channel 3
	SIDout(24, 15); //laut!
	SIDout(4, 19);
	_delay_ms(400);
	SIDout(4, 18);
	_delay_ms(1600);


	clearsid();
}

void setnote(uint8_t bank, uint8_t note) {
	if (bank < 3) {
		uint16_t n = NOTE_TABLE[note + 36]; //frequenz aus tabelle holen
		SIDout(bank * 7 + 0, n & 255); //note low
		SIDout(bank * 7 + 1, n >> 8); //note high
	}
}
void bankon(uint8_t bank) {
	if (bank < 3) {
		SIDout(bank * 7 + 4, 16 + 1);
	}
}
void bankoff(uint8_t bank) {
	if (bank < 3) {
		SIDout(bank * 7 + 4, 16);
	}
}

void plink(uint8_t b) {
	if (b>0 && b < 64) {
		setnote(0,b);
		bankon(0);
		_delay_ms(50);
		bankoff(0);
		_delay_ms(50);
	}
}

int main(void) {
	initUART();
	_delay_ms(500);
		initsid();
		clearsid();
	//	uint8_t i = 0;
	//	uint8_t a = 0;
		_delay_ms(500);
		ringmodtest();
		//uint8_t i=0;
		while (1) {
			//if (GetMIDIByte()>0)
			plink(MIDI_STACK_Bytes);//GetMIDIByte());

		/*for (i=0;i<64;i++) {
			setnote(0,i);
			bankon(0);
			_delay_ms(50);
			bankoff(0);
			_delay_ms(50);

		};*/
		};
	return 0;
}

ISR (USART_RXC_vect){
	char ReceivedByte;
    ReceivedByte = UDR;
    if (MIDI_STACK_Bytes < 255)                                                     // put in FIFO only if there is room, else trash data
                {
                MIDI_STACK_Write++;                                                     // increase write pointer
                MIDI_STACK_Write &= 127;                                                // and normalize to max. 128 bytes
                MIDI_STACK[MIDI_STACK_Write] = ReceivedByte;                               // put Byte in the FIFO queue
                MIDI_STACK_Bytes++;                                                     // increase byte counter
                }

}
