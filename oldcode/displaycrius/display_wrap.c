#include <stdlib.h>
#include <avr/io.h>

#include <avr/pgmspace.h>

#include "display_wrap.h"
#include "ssd1306xled.h"
//todo: make faster!
 uint8_t px = 0;
 uint8_t py = 0;
 color_t color = 1;
 color_t bgcolor = 1;
void display_hline (int16_t x, int16_t y, int16_t r,color_t color) {
	if (y>0 && y< display_HEIGHT) {
		for (uint16_t i=0;i<r;i++) {
			display_pixel(x+i, y, color);
		}
	}
}
//todo: make faster!
void display_vline (int16_t x, int16_t y, int16_t r,color_t color) {
	if (x>0 && x< display_WIDTH) {
		for (uint16_t i=0;i<r;i++) {
			display_pixel(x,y+i, color);
		}
	}
}
void display_char(uint16_t x, uint16_t y, uint8_t c, color_t color,
		color_t bg_col) {
	x++;y++;
	if (bg_col == color) {
		for (uint8_t i = 0; i < 5; i++) {
			uint8_t line = pgm_read_byte(DISPLAY_font + (c * 5) + i);
			for (uint8_t j = 0; j < 8; j++) {
				if (line & 0x1) {
					display_pixel(x + i, y + j, color);
				}
				line >>= 1;
			}
		}
	} else {
		for (uint8_t i = 0; i < 5; i++) {
			uint8_t line = pgm_read_byte(DISPLAY_font + (c * 5) + i);
			for (uint8_t j = 0; j < 8; j++) {
				if (line & 0x1) {
					display_pixel(x + i, y + j, color);
				} else {
					display_pixel(x + i, y + j, bg_col);
				}
				line >>= 1;
			}
			//display_pixel(x + 5, y, bg_col);
		}
		display_vline(x+5,y,8,bg_col);
	}
}

void display_circle(int16_t x0, int16_t y0, int16_t r, color_t color) {
  int16_t f = 1 - r;
  int16_t ddF_x = 1;
  int16_t ddF_y = -2 * r;
  int16_t x = 0;
  int16_t y = r;

  display_pixel(x0  , y0+r, color);
  display_pixel(x0  , y0-r, color);
  display_pixel(x0+r, y0  , color);
  display_pixel(x0-r, y0  , color);

  while (x<y) {
    if (f >= 0) {
      y--;
      ddF_y += 2;
      f += ddF_y;
    }
    x++;
    ddF_x += 2;
    f += ddF_x;

    display_pixel(x0 + x, y0 + y, color);
    display_pixel(x0 - x, y0 + y, color);
    display_pixel(x0 + x, y0 - y, color);
    display_pixel(x0 - x, y0 - y, color);
    display_pixel(x0 + y, y0 + x, color);
    display_pixel(x0 - y, y0 + x, color);
    display_pixel(x0 + y, y0 - x, color);
    display_pixel(x0 - y, y0 - x, color);
  }
}

void display_circlehelper( int16_t x0, int16_t y0,
 int16_t r, uint8_t cornername,int16_t delta, color_t color) {
  int16_t f     = 1 - r;
  int16_t ddF_x = 1;
  int16_t ddF_y = -2 * r;
  int16_t x     = 0;
  int16_t y     = r;

  while (x<y) {
    if (f >= 0) {
      y--;
      ddF_y += 2;
      f     += ddF_y;
    }
    x++;
    ddF_x += 2;
    f     += ddF_x;
    if (cornername & 0x4) {
    	display_pixel(x0 + x, y0 + y, color);
    	display_pixel(x0 + y, y0 + x, color);
    }
    if (cornername & 0x2) {
    	display_pixel(x0 + x, y0 - y, color);
    	display_pixel(x0 + y, y0 - x, color);
    }
    if (cornername & 0x8) {
    	display_pixel(x0 - y, y0 + x, color);
    	display_pixel(x0 - x, y0 + y, color);
    }
    if (cornername & 0x1) {
    	display_pixel(x0 - y, y0 - x, color);
    	display_pixel(x0 - x, y0 - y, color);
    }
  }
}

void display_fillCircle(int16_t x0, int16_t y0, int16_t r,
 color_t color) {
	display_vline(x0, y0-r, 2*r+1, color);
	display_circlehelper(x0, y0, r, 3, 0, color);
}

void clearline(uint8_t line) {
	while (line > 39)
		line -= 40;
	for (uint8_t j = 0; j < 8; j++) {
		display_hline(0, line * 8 + j, display_WIDTH, 0);
	}
}



void echo(uint8_t ch) {
	if (ch == 13) return;
	if (ch == 10) {
		px = 0;
		py++;
		if (py >= (display_HEIGHT/display_FONTHEIGHT)) {
			py = 0;
		}
		clearline(py);
	} else {
		display_char(px * (display_FONTWIDTH), py *  (display_FONTHEIGHT), ch, color, bgcolor);
		px++;
		if (px >= (display_WIDTH/display_FONTWIDTH)) {
			px = 0;
		 	py++;
			if (py >= (display_HEIGHT/display_FONTHEIGHT)) {
				py = 0;
			}
			clearline(py);
		}
	}
}
void echo_str_pgm(const char* str) {
	char c;
	    while (0 != (c = pgm_read_byte(str))) {

	    	echo(c);
	        str++;
	    }
}
void echo_str( char* str) {
    while (*str) {
    	echo(*str++);
    }
}
void display_setcol(color_t c,	color_t bg){
	color=c;
		    	bgcolor=bg;
}
void display_setpxpy(int16_t x, int16_t y) {
	px=x;
	py=y;
}
