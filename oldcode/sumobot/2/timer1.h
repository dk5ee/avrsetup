/*
 * timer1.h
 *
 */

#ifndef TIMER1_H_
#define TIMER1_H_
#include <avr/io.h>
#include <avr/interrupt.h>

#define ENABLESERVOPOWERPIN DDRB |=(1<<0)
#define SERVOPOWERON PORTB |= (1<<0)
#define SERVOPOWEROFF PORTB &= ~(1<<0)
#define ENABLESERVOPINS DDRB |= (1<<1) | (1<<2)
#define DISSABLESERVOPINS DDRB &= ~((1<<1) | (1<<2))

void   set_servo1(uint16_t duration);
void   set_servo2(uint16_t duration);
void   set_timertop(uint16_t duration);

void servo_enable();
void servo_dissable();

#endif /* TIMER1_H_ */
