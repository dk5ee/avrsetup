/*
 * sumodefs.c
 *
 */

#include "sumodefs.h"
void sumoinit(){
	INITLED;
	LEDOFF;


	INITPS2POWERPIN;
	PS2POWEROFF;

	adcinit();
	adcclear();
	timer0_init();
}

void mouseon(){
	PS2POWERON;
	ps2boot(); // this may take a while..
}
void mouseoff(){
	ps2shutdown();
	PS2POWEROFF;
}

void sumoloop(){
	uint8_t a=0;
	while (1) {
		if (keypressed) {
			timer0_stop();
			a=10;
			keypressed=0;
			do {
				LEDON;
				_delay_ms(500);
				LEDOFF;
				_delay_ms(500);
				a--;
			} while (a>0);
			timer0_init();
		}
		if (sendstatus(a)) a++;

	}

}
