/*
 * ps2lib.c
 *
 */

#include "ps2lib.h"

/*
 * pd3/ int1: clockpin	(arduino: 3)
 * pd4 datapin			(arduino: 4)
 * das ganze spiel geht über external int1
 */

//ein paar defines für die pins:
#define PS2DATADDR DDRD
#define PS2DATAPORT PORTD
#define PS2DATAPIN PIND
#define PS2DATAPINNR PD4
#define PS2CLOCKPIN PIND
#define PS2CLOCKDDR DDRD
#define PS2CLOCKPORT PORTD
#define PS2CLOCKPINNR PD3
//der ISR vector:
#define PS2INT_VECT INT1_vect

//ein paar makros...
#define PS2DATAIN PS2DATADDR &= ~(1<<PS2DATAPINNR)
#define PS2DATAOUT PS2DATADDR |= (1<<PS2DATAPINNR)
#define PS2DATALOW PS2DATAPORT &= ~(1<<PS2DATAPINNR)
#define PS2DATAHIGH PS2DATAPORT |= (1<<PS2DATAPINNR)
#define PS2DATAISHIGH ((PS2DATAPIN & (1<<PS2DATAPINNR)) != 0)
#define PS2DATAISLOW ((PS2DATAPIN & (1<<PS2DATAPINNR)) == 0)

#define PS2DH PS2DATAIN; PS2DATAHIGH
#define PS2DL PS2DATAOUT; PS2DATALOW

#define PS2CLOCKIN PS2CLOCKDDR &= ~(1<<PS2CLOCKPINNR)
#define PS2CLOCKOUT PS2CLOCKDDR |= (1<<PS2CLOCKPINNR)
#define PS2CLOCKLOW PS2CLOCKPORT &= ~(1<<PS2CLOCKPINNR)
#define PS2CLOCKHIGH PS2CLOCKPORT |= (1<<PS2CLOCKPINNR)
#define PS2CLOCKISHIGH ((PS2CLOCKPIN & (1<<PS2CLOCKPINNR)) != 0)
#define PS2CLOCKISLOW ((PS2CLOCKPIN & (1<<PS2CLOCKPINNR)) == 0)

#define PS2CH PS2CLOCKIN; PS2CLOCKHIGH
#define PS2CL PS2CLOCKLOW; PS2CLOCKOUT

//External Interrupt Control Register A, früher mit: MCUCR, nun EICRA
//makro um den int1 zu intialisieren: fallende flanke ist gefragt
#define PS2INITINT EICRA &=~(1<<ISC10); EICRA |= (1<<ISC11)
//enable/dissable, früher: GICR, nun
//todo: ist EIFR löschen hier nötig?
#define PS2ENABLEINT EIMSK |= (1<<INT1);EIFR |= (1 << INTF1)
#define PS2DISSABLEINT EIMSK &= ~(1<<INT1)

//nun laufende variablen für ISR
volatile uint8_t ps2bitcount = 0; // zählt gesendete/empfangen bits mit
volatile uint8_t ps2flag = 0; // 0=idle; 1= recieving now; >1: free running ps2 scroll dings
volatile uint8_t ps2paritycount = 0; //zählt für odd parity mit
volatile uint8_t ps2INdata = 0; //hält das gerade emfangene byte


#define PS2_BUFSIZE 16
static uint8_t ps2_Buf[PS2_BUFSIZE];
static volatile uint8_t ps2_Buf_Head = 0;
static volatile uint8_t ps2_Buf_Tail = 0;
static volatile uint8_t ps2_Buf_Count = 0;
//todo: timeouterror auch verwenden
#define __TIMEOUT 60000
volatile uint8_t timeouterror = 0;
static volatile uint8_t ps2_5 = 0;

void waitforclock() {
	uint16_t timeout = __TIMEOUT;
	//first: wait while clock is LOW
	do {
		timeout--;
		if (timeout == 0) {
			timeouterror = 1;
		}
	} while (timeout > 0 && PS2CLOCKISLOW);
	//then: wait while clock is HIGH
	timeout = __TIMEOUT;
	do {
		timeout--;
		if (timeout == 0) {
			timeouterror = 1;
		}
	} while (timeout > 0 && PS2CLOCKISHIGH);
}
/* unused:
 uint8_t ps2isactive() {
 uint16_t timeout = __TIMEOUT;
 while (ps2flag == 1) {
 _delay_us(1);
 timeout--;
 if (timeout == 0) {
 timeouterror = 1;
 return 1;
 }
 }; //warten mit timeout wenn gerade gesendet wird
 return 0;
 }
 */
void ps2run() {
	ps2flag = 2;
}
void ps2init() {
	cli();
	//falls schon andere dinge aktiv
	PS2INITINT; //vorbereiten..
	//clock+data auf high
	PS2DH;
	PS2CH;
	PS2ENABLEINT;
	//int on falling edge
	sei();
}
uint8_t ps2incount() {
	return ps2_Buf_Count;
}
//ps2get: liefert zeichen aus puffer oder 0 wenn leer.
uint8_t ps2get() {
	if (ps2_Buf_Count > 0) {
		uint8_t tmp;
		tmp = (ps2_Buf_Tail + 1) & (PS2_BUFSIZE - 1);
		ps2_Buf_Count--;
		ps2_Buf_Tail = tmp;
		return (ps2_Buf[tmp]);
	} else
		return 0;
}
void ps2clearbuf() {
	while (ps2incount())
		ps2get();
}
//sends one data, waits for couple of responses
void ps2pollsend(uint8_t data, uint8_t responses) {

	// ps2isactive();

	//dissable int on falling edge
	PS2DISSABLEINT; // wir wollen nicht durch nachstehendes einen interrupt auslösen

	//clock to low - block device
	PS2DH;
	PS2CL;
	//wait 100µs
	_delay_us(100);
	//data to low - request to send
	PS2DL;
	_delay_us(2);
	//clock to high
	PS2CH;
	uint8_t j = 0; //bitcounter..
	uint8_t parity = 0;

	//first: send data
	//startbit..
	do {
	} while (PS2CLOCKISHIGH);
	//8 bits..
	for (j = 0; j < 8; j++) {
		waitforclock();
		if (data & 1) {
			parity++;
			//set data=high
			PS2DH;
		} else {
			//set data=low
			PS2DL;
		}
		data >>= 1; //schieb schieb..
	}
	//parity..
	waitforclock();
	if (parity & 1) {
		//odd parity: set data low now
		PS2DL;

	} else {
		//odd parity: set data high now
		PS2DH;
	}
	//stopbit
	waitforclock();
	//now: stop bit
	//set data high
	PS2DH;
	//ack..
	waitforclock();
	//todo: check: data should be low now


	//then: recieve some data
	uint8_t i = 0;
	for (i = 0; i < responses; i++) {
		waitforclock();
		parity = 0;//reset parity
		//if (PS2DATAISLOW)

		//todo: start bit, should be low, now ignored

		uint8_t indata = 0;
		for (j = 0; j < 8; j++) {
			//8 bits..
			waitforclock();
			if (PS2DATAISHIGH) {
				parity++;
				indata += (1 << j);
			}
		}
		//one parity..

		waitforclock();
		if (PS2DATAISHIGH)
			parity++;
		if (parity & 1) { //ok, daten speichern..
			uint8_t tmp;
			tmp = (ps2_Buf_Head + 1) & (PS2_BUFSIZE - 1);
			ps2_Buf_Head = tmp;
			ps2_Buf[tmp] = indata;
			ps2_Buf_Count++;
			if (ps2_Buf_Count == PS2_BUFSIZE) {
				//fix overflow, remove item from Tail
				ps2_Buf_Tail = (ps2_Buf_Tail + 1) & (PS2_BUFSIZE - 1);
				ps2_Buf_Count--;
			}

		} else { //todo: falsches parity

		}
		//one stop bit
		waitforclock();

		//todo: check for high;


	}
	//enable int on falling edge - ab jetzt übernimmt das ps/2 gerät und die ISR
	PS2ENABLEINT;
}

void ps2boot() {
	ps2init();
	// reset..
	ps2pollsend(0xFF, 3);
	ps2pollsend(0xFF, 3);
	ps2pollsend(0xFF, 3);
	// intellimouse init
	ps2pollsend(0xF3, 1);
	ps2pollsend(200, 1);
	ps2pollsend(0xF3, 1);
	ps2pollsend(100, 1);
	ps2pollsend(0xF3, 1);
	ps2pollsend(80, 1);
	//read version
 	ps2pollsend(0xf2, 2);
 	//fast sample.. 200hz
 	ps2pollsend(0xF3, 1); ps2pollsend(200, 1);
 	//high res: 8counts/mm
 	ps2pollsend(0xe8, 1); ps2pollsend(03, 1);
 	//1:1 scaling
 	ps2pollsend(0xe6, 1);

 	//enable reporting and let ISR take over..
	ps2clearbuf();
 	ps2pollsend(0xF4, 1);
	ps2clearbuf();
	//ps2run: signal ISR to do more stuff
	ps2run();
}
void ps2shutdown() {
	PS2DISSABLEINT;
	ps2clearbuf();
	ps2reset();
}

//blick aufs erste empfangene byte werfen..
/*
uint8_t ps2peek() {
	if (ps2_Buf_Count > 0) {
		return (ps2_Buf[(ps2_Buf_Tail + 1) & (PS2_BUFSIZE - 1)]);
	} else
		return 0;
}
*/
//enable automatic mode..

uint16_t ps2x() {
	cli();
	uint16_t wert = ps2_xval;
	sei();
	return wert;
}
uint16_t ps2y() {
	cli();
	uint16_t wert = ps2_yval;
	sei();
	return wert;
}
uint16_t ps2z() {
	cli();
	uint16_t wert = ps2_zval;
	sei();
	return wert;
}
uint8_t ps2b() {
	return (ps2_buttval & 0x7);
}
uint8_t ps2flags() {
	return (ps2_5);
}
void ps2resetflags() {
	ps2_5 = 0;
}
void ps2reset() {
	cli();
	ps2_buttval = 0;
	ps2_xval = 0;
	ps2_yval = 0;
	ps2_zval = 0;
	ps2_5 = 0;
	sei();
}

ISR(PS2INT_VECT)
{

	if (ps2bitcount == 0) {
		if (ps2flag == 0)
			ps2flag = 1;
		//wenn data==low gehen daten los, ansonsten rausspringen aus int
		if (PS2DATAISHIGH)
			return;
		//ansonsten: init..
		ps2INdata = 0; //in data
		ps2paritycount = 0;//parity
	} else if (ps2bitcount < 9) {
		if (PS2DATAISHIGH) {
			ps2paritycount++;
			ps2INdata += (1 << (ps2bitcount - 1));
		}
	} else if (ps2bitcount == 9) {
		if (PS2DATAISHIGH)
			ps2paritycount++;
		if (ps2paritycount & 1) { //ok, daten speichern..
			if (ps2flag == 1) {
				uint8_t tmp;
				tmp = (ps2_Buf_Head + 1) & (PS2_BUFSIZE - 1);
				ps2_Buf_Head = tmp;
				ps2_Buf[tmp] = ps2INdata;
				ps2_Buf_Count++;
				if (ps2_Buf_Count == PS2_BUFSIZE) {
					//fix overflow, remove item from Tail
					ps2_Buf_Tail = (ps2_Buf_Tail + 1) & (PS2_BUFSIZE - 1);
					ps2_Buf_Count--;
				}
			} else {
				//magic check byte..
				//first byte: bit3 is set, at least one bit of upper 4 bits not set
				uint8_t data = ps2INdata;
				if ((ps2flag > 4) && (data & (1 << 3)) && ((data & (0xf0))
						!= 0xf0))
					ps2flag = 2; //found first byte!
				if (ps2flag == 2) {
					ps2_buttval = data;
					ps2_5 |= (data & 0x07);
				} else if (ps2flag == 3) { //x
					 //if (ps2_buttval & (1 << 6))data = 255; // ignore overflow..
					if (ps2_buttval & (1 << 4)) {
						ps2_xval += data-256;
					} else {
						ps2_xval += data;
					}
					if (data)
						ps2_5 |= (1 << 4);
				} else if (ps2flag == 4) {
					//if (ps2_buttval & (1 << 7))	data = 255; // ignore overflow..
					if (ps2_buttval & (1 << 5)) {
						ps2_yval += data-256;
					} else {
						ps2_yval += data;
					}
					if (data)
						ps2_5 |= (1 << 5);
				} else {
					int8_t z = data;
					ps2_zval += z;
					if (data)
						ps2_5 |= (1 << 6);
				}
				ps2flag++;
			}

		} else { //falsches parity
		}

	} else if (ps2bitcount == 10) { //stop bit, muss high sein.
		if (ps2flag == 1)
			ps2flag = 0;
		ps2bitcount = 0;
		return;
	}
	ps2bitcount++;
}
