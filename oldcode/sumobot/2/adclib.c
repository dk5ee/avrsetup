#include "adclib.h"



uint16_t adcget(uint8_t channel) {
    cli();
    uint16_t i=adcarray[channel];
    sei();
    return i;
}

void adcclear() {
 uint8_t i;
 for (i=0;i<16;i++) {
	adcarray[i]=0;
}
}
void adcinit() {
	//set reference voltage
	//ref0 ref1
		//	0	0	extern vref
		//	1	0	avcc external
		//	0	1	reserved
		//	1	1	internal 1.1v
	 ADMUX = (1<<REFS1) | (1 <<REFS0);
	 ADCSRB =0;
     // Taktfrequenz (F_CPU/128) 
    ADCSRA = (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0)  ;
    //adps[8]=2,2,4,8,16,32,64,128 - here: 128, slowest
    //interne referenzspannung 1.1V

}
void adcstart() {
	ADMUX &= 0b11110000;//deselect channel
	ADCSRA |= 1 << ADIE; //interrupt enable
    ADCSRA |= (1<<ADSC);//start conversion
}
void adcstop(){
	//hardcore stop - no more interrupts
	ADCSRA = 0;
//	adcclear();
}

ISR(ADC_vect) {
    uint16_t value = ADC; //readout adc value
    uint8_t channel = ADMUX & 15; //get chosen channel
    adcarray[channel]=value;
    // Schalte zum nächsten Kanal
    channel++;
    ADMUX=(ADMUX & ~15) | channel;
    // Starte nächste Messung
    ADCSRA |= (1<<ADSC); 
}

/**annotations
 *
 * ATmega48A/PA/88A/PA/168A/PA/328/P adc channel list
 * 0	ADC0
 * 1	ADC1
 * 2	ADC2
 * 3	ADC3
 * 4	ADC4
 * 5	ADC5
 * 6	ADC6	only smd
 * 7	ADC7	only smd
 * 8	ADC8	internal temperature
 * 9..13	reserved
 * 14	1.1V
 * 15	0V
 *
 */
