#ifndef PS2LIB_H
#define PS2LIB_H 1
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>



//8 funktionen die eigentlich alles machen..
void ps2boot(); //setzt ps2 maus in wheelmode
uint16_t ps2x();	//x
uint16_t ps2y();	//y
uint16_t ps2z();	//z
uint8_t ps2b();		//tasten
uint8_t ps2flags();	//flags: für jede taste und dimension eines, wird gesetzt bei änderung
void ps2resetflags();
void ps2reset();	//alle zähler zurücksetzen
void ps2shutdown(); //anhalten, alles löschen

static volatile uint8_t ps2_buttval = 0;
static volatile int16_t ps2_xval = 0;
static volatile int16_t ps2_yval = 0;
static volatile int16_t ps2_zval = 0;


//andere funktionen
/*
void ps2init(); //einfaches init
void ps2pollsend(uint8_t data, uint8_t responses); //senden ein zeichen an ps2gerät, wartet auf anzahl von antworten, dann ps2 freilauf
uint8_t ps2incount(); //anzahl daten im fifo
uint8_t ps2get();//ein byte aus dem fifo holen
uint8_t ps2peek();//das erste byte im fifo anschauen
void ps2run();//freilaufmodus scrollwheelmaus
*/

#endif
