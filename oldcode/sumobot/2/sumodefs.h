/*
 * sumodefs.h
 *
 */

#ifndef SUMODEFS_H_
#define SUMODEFS_H_

#include <avr/io.h>
//#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "usart.h"
#include "adclib.h"



#include "timer0.h"

//ps2 defines - rest defs in ps2lib.h
#define INITPS2POWERPIN DDRD |=(1<<5)
#define PS2POWERON PORTD |= (1<<5)
#define PS2POWEROFF PORTD &= ~(1<<5)
#include "ps2lib.h"

//servo defines

//todo: timer 1 servo - PB1, PB2 / OCR1A OCR1B
#include "timer1.h"

/*ferner:
 * ADC0: vcc/2
 * ADC1: vcc
 * PD0 RXD
 * PD1 TXD
 * PD6 AUX1
 * PD7 AUX2
 *
 */


void sumoinit();
void sumoloop();



#endif /* SUMODEFS_H_ */
