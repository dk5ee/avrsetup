/*
 * timer1.c
 *
 */

#include "timer1.h"

uint16_t servo1 = 0;
uint16_t servo2 = 0;


//setservo - interrupt save
/*
 * prescaler == 8
	 1ms..
	 12mhz= 1500
	 16mhz= 2000
	 20mhz= 2500
	 25mhz= 3125

	 2ms..
	 12mhz= 3000
	 16mhz= 4000
	 20mhz= 5000
	 25mhz= 6250

*/

void inline set_servo1(uint16_t duration) {
	cli();
	servo1 = duration;
	sei();
}
void inline set_servo2(uint16_t duration) {
	cli();
	servo2 = duration;
	sei();
}
void set_timertop(uint16_t duration) {
	/*
	 * prescaler == 8
	 20ms..
	 12mhz= 30000
	 16mhz= 40000
	 20mhz= 50000
	 25mhz= 62500
	 */
	cli();
	TCNT1 =duration;
	sei();
}

void servo_enable() {
	//1. power on
	ENABLESERVOPOWERPIN;
	SERVOPOWERON;
	//2. dissable pwm
	servo1 = 0xffff;
	OCR1A = servo1;
	servo2 = 0xffff;
	OCR1B = servo2;
	//3. enable pwm pins
	ENABLESERVOPINS;
	set_timertop(40000);
	//5. clear on compare, fast PWM
	TCCR1A = (1 << COM1A1) | (0 << COM1A0) | (1 << COM1B1) | (0 << COM1B0) | (1
			<< WGM11) | (0 << WGM10);
	//6. prescaler 8
	TCCR1B = (1 << WGM12) | (1 << WGM13) | (0 << CS10) | (1 << CS11) | (0
			<< CS12);
	//WGM1= 1110 : Waveform Generation Mode 14 - Fast PWM
	//7. change pwm duration on timer overflow interrupt
	TIMSK1 = (1<<TOIE1);
}
void servo_dissable() {
	TIMSK1 &= ~(1<<TOIE1);
	SERVOPOWEROFF;
	DISSABLESERVOPINS;
	//CS1 => 0 => dissable timer
	TCCR1B = (1 << WGM12) | (1 << WGM13) | (0 << CS10) | (0 << CS11) | (0
			<< CS12);

}
ISR(TIMER1_OVF_vect){
	OCR1A = servo1;
	OCR1B = servo2;
}
