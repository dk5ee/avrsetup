/*
 * usart.c
 *
 */

/*------------------------------------------------------------------------------
 * Copyright 1994-2008 The FreeBSD Project. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE FREEBSD PROJECT "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE FREEBSD PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *----------------------------------------------------------------------------*/

#include "usart.h"

/* Static Variables */
static uint8_t USART0_RxBuf[USART0_RX_BUFFER_SIZE];
static volatile uint8_t USART0_RxHead = 0;
static volatile uint8_t USART0_RxTail = 0;
static volatile uint8_t USART0_RxCount = 0;
static uint8_t USART0_TxBuf[USART0_TX_BUFFER_SIZE];
static volatile uint8_t USART0_TxHead = 0;
static volatile uint8_t USART0_TxTail = 0;
static volatile uint8_t USART0_TxCount = 0;

/**----------------------------------
 * @brief Initialize USART
 * @param void
 * @return void
 *----------------------------------*/
void usart0_init(void) {
	cli();
#undef BAUD // avoid compiler warning
#define BAUD 9600UL
#include <util/setbaud.h>
	UBRR0L = UBRRL_VALUE;
	UBRR0H = UBRRH_VALUE;
	UCSR0B = ((1 << RXCIE0) | (1 << RXEN0) | (1 << TXEN0));
#ifdef URSEL
	UCSRC = (1<<URSEL)|(1<<UCSZ1)|(1<<UCSZ0);
#else
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);
#endif
#if USE_2X
	UCSR0A |= (1 << U2X0);
#endif /*USE_2X*/
	USART0_RxTail = 0;
	USART0_RxHead = 0;
	USART0_TxTail = 0;
	USART0_TxHead = 0;


	sei();
}
void usart0_exit(void) {
	cli(); //dissable ints
	UCSR0B &= ~((1 << RXCIE0) | (1 << RXEN0) | (1 << TXEN0));
	sei();
	USART0_RxTail = 0;
		USART0_RxHead = 0;
		USART0_TxTail = 0;
		USART0_TxHead = 0;

}

/**----------------------------------
 * @brief Receives Char from USART Buffer
 * @param void
 * @return char
 *----------------------------------*/

uint8_t usart0_getc(void) {
	uint8_t tmp;
	while (USART0_RxHead == USART0_RxTail)
		;
	tmp = (USART0_RxTail + 1) & USART0_RX_BUFFER_MASK;
	USART0_RxCount--;
	USART0_RxTail = tmp;
	return USART0_RxBuf[tmp];
}

/**----------------------------------
 * @brief Sends Char to USART Buffer
 * @param Char to send
 * @return void
 *----------------------------------*/

void usart0_putc(char data) {
	uint8_t tmp;
	tmp = (USART0_TxHead + 1) & USART0_TX_BUFFER_MASK;
	USART0_TxCount++;
	while (tmp == USART0_TxTail)
		; //block until some space in buf
	USART0_TxBuf[tmp] = data;
	USART0_TxHead = tmp;
	UCSR0B |= (1 << UDRIE0);
}


/**----------------------------------
 * @brief Test if received char is in Buffer
 * @param void
 * @return Return 0 (FALSE) if the receive buffer is empty
 *----------------------------------*/

uint8_t usart0_data_received(void) {
	return USART0_RxCount;
	//return ( USART0_RxHead != USART0_RxTail );
}
uint8_t usart0_data_tosend(void) {
	return USART0_TxCount;

}

// now: shortcuts

const char hex[] = "0123456789ABCDEF";
void outword(uint16_t w) {
	usart0_putc('0');
	usart0_putc('x');
	usart0_putc(hex[(w >> 12) & 15]);
	usart0_putc(hex[(w >> 8) & 15]);
	usart0_putc(hex[(w >> 4) & 15]);
	usart0_putc(hex[w & 15]);

}

void outbyte(uint8_t a) {
	usart0_putc('0');
	usart0_putc('x');
	usart0_putc(hex[(a >> 4) & 15]);
	usart0_putc(hex[a & 15]);
	BR;
}
void outuint(uint16_t wert) {
	uint16_t laufend = wert; //diese variable wird vermutlich vom compiler wegoptimiert
	uint8_t merker = '0';
	uint8_t erstes = 0; // um führende nullen zu eliminieren
	while (laufend >= 10000) {
		laufend -= 10000;
		merker++;
		erstes = 1;
	}
	if (erstes) {
		usart0_putc((uint8_t) merker + '0');
	}
	merker = '0';
	while (laufend >= 1000) {
		laufend -= 1000;
		merker++;
		erstes = 1;
	}
	if (erstes) {
		usart0_putc((uint8_t) merker + '0');
	}
	merker = '0';
	while (laufend >= 100) {
		laufend -= 100;
		merker++;
		erstes = 1;
	}
	if (erstes) {
		usart0_putc((uint8_t) merker + '0');
	}
	merker = '0';
	while (laufend >= 10) {
		laufend -= 10;
		merker++;
		erstes = 1;
	}
	if (erstes) {
		usart0_putc((uint8_t) merker + '0');
	}
	//auf jeden fall letzte ziffer auspucken
	usart0_putc((uint8_t) laufend + '0');
	BR;
}
void outint(int16_t d){
	if (d<0){
		d=0-d;
		usart0_putc('-');
		outuint(d);
	} else outuint(d);
}
void outbin(uint8_t a) {
	for (int i = 8; i > 0; i--)
		if (a >> (i - 1) & 1)
			usart0_putc('1');
		else
			usart0_putc('0');


}

/**----------------------------------
 * ISR
 *----------------------------------*/

ISR(USART_RX_vect)
//UART0_RECV_IRQ)
{
	uint8_t data;
	uint8_t tmp;
	data = UDR0;
	tmp = (USART0_RxHead + 1) & USART0_RX_BUFFER_MASK;
	USART0_RxCount++;
	USART0_RxHead = tmp;
	USART0_RxBuf[tmp] = data;
}

ISR(USART_UDRE_vect)
//UART0_DATA_IRQ)
{
	uint8_t tmp;

	if (USART0_TxCount) {

		tmp = (USART0_TxTail + 1) & USART0_TX_BUFFER_MASK;
		USART0_TxTail = tmp;
		USART0_TxCount--;
		UDR0 = USART0_TxBuf[tmp];
	} else {
		UCSR0B &= ~(1 << UDRIE0);
	}
}


