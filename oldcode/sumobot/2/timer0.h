/*
 * timer0.h
 *
 */

#ifndef TIMER0_H_
#define TIMER0_H_
#include <avr/io.h>

#include <avr/interrupt.h>

#define INITLED DDRD |= (1<<2)
#define LEDOFF PORTD |= (1<<2)
#define LEDON PORTD &= ~( 1<<2)
#define TASTER ((PIND & (1<<PD2))==0)

extern volatile uint8_t keypressed ;
extern volatile uint8_t statusledshine ;


uint8_t sendstatus(uint8_t status);
void timer0_init();
void timer0_stop();
ISR(TIMER0_OVF_vect );
#endif /* TIMER0_H_ */
