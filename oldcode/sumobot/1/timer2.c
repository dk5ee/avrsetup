/*
 * timer2.c
 *
 */
#include <avr/interrupt.h>
#include "sumodefs.h"
#include "timer2.h"


volatile uint8_t c1 = 0; //low timer2
volatile uint8_t c2 = 0; //high timer2
void timer2_init() {
	//DISSABLE timer2 - not noetig
//	TIMSK &= ~(1 << TOIE2);
//	TIMSK &= ~(1 << OCIE2);
	//set clock source
	ASSR |= 1 << AS2;
	//write TCNT2, OCR2, TCCR2
	TCNT2 = 256 - 125;//Timer/Counter Register
	//TCNT2=0;
	//OCR2 = 0;//Output Compare Register - sollte 0 sein
	TCCR2 = (1 << CS20);//no prescaling

	//enable overflow..
	TIMSK |= 1 << TOIE2;
	while (ASSR & (TCN2UB | OCR2UB | TCR2UB)) {
		//To switch to asynchronous operation: Wait for TCN2UB, OCR2UB, and TCR2UB.
	};

}


ISR(TIMER2_OVF_vect )
{
	TCNT2 = 256 - 125;
	c1++;
	if (c1 == 0){
		c2++;
	}

}
