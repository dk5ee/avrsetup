/*
 * timer2.h
 *
 */

#ifndef TIMER2_H_
#define TIMER2_H_


extern volatile uint8_t c1; //low timer2
extern volatile uint8_t c2; //high timer2

void timer2_init();

ISR(TIMER2_OVF_vect );

#endif /* TIMER2_H_ */
