/*
 * timer0.c
 *
 */
#include <avr/interrupt.h>
#include "sumodefs.h"
#include "timer0.h"

volatile uint8_t c3 = 0; //low timer0
volatile uint8_t c4 = 0; //high timer0
volatile uint8_t c5 = 0; //high timer0
volatile uint8_t c6 = 0; //high timer0
#define blinkmax 4096;
volatile uint8_t blinkstatus = 0b11100100;
volatile uint8_t blinkcounter1 = 0; //loop 80->0
volatile uint16_t blinkcounter2 = 0; //wiederholungen

void timer0_init() {
	//DISSABLE timer0 nicht nötig
	//TIMSK &= ~(1 << TOIE0);

	// clock setzen -> timer starten
	TCCR0 |= CS01; //  seite 72
	TCCR0 |= CS00; //  seite 72

	//ENABLE timer0
	TIMSK |= (1 << TOIE0);

}

uint8_t sendstatus(uint8_t status) {
	if (blinkcounter1 > 0) {
		return (0); //noch am senden
	} else {
		blinkstatus = status;
		blinkcounter1 = 80;
		return (1);
	}
}
ISR(TIMER0_OVF_vect )
{

//	TCNT0 = 0;
	c3++;

	if ((c3 & 1) ) {
	  BLUE_off;
	} else {

		if (TASTER)
			keypressed = 1;
		//BLUE_on;
		uint8_t shine=blueledshine;
		if (blinkcounter2 == 0) {
			blinkcounter2 = blinkmax;
			//	if (blinkcounter1==0) {
			//		blinkcounter1=80;
			//} else
			uint8_t bc=blinkcounter1;
					//hier ist led aus, hier kann taste abgefragt werden..
			if (bc > 0) {
				shine = 0;
				if (bc > 72) {
					shine = 1;
				} else {
					if ((bc& 1) || ((bc & 8) == 0)) {
						//1 -> ungerade, pause zwischen einzelblinks
						//8 -> pause zwischen blinkblocks

					} else {
						//lange version
						uint8_t nibble = bc >> 4; // um je zwei bits auszuwählen
						uint8_t abits = (blinkstatus >> (nibble << 1)) & 3; //diese zwei bits aus status ziehen
						uint8_t thisbit = (bc >> 1) & 3; // schauen wo counter steht
						uint8_t bbits = ((2 << abits) - 1); // errechnen des bitmusters
						if ((bbits >> thisbit) & 1) {
							shine = 1;
						}
						//kurz - sollte compiler so umbauen und noch weiter vereinfachen
						//if ((((2<<((blinkstatus >> ((blinkcounter1 >>4)<<1)) & 3))-1)>>((blinkcounter1 >>1 )&3))&1) { blueledshine=1; }
					}
				}
				blinkcounter1=bc-1;
			}
		} else {
			blinkcounter2--;
			if (shine) BLUE_on;
		}
		blueledshine=shine;

	}

	if (c3 == 0)
		c4++;
}
