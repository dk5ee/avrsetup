/*
 * sumodefs.h
 *
 */

#ifndef SUMODEFS_H_
#define SUMODEFS_H_


//dreimal makros für leds
#define IR_l_on (PORTD |= (1<<PD0) )
#define IR_l_off (PORTD &= ~(1<<PD0) )
#define IR_r_on (PORTD |= (1<<PD1) )
#define IR_r_off (PORTD &= ~(1<<PD1) )
#define BLUE_off (PORTD |= (1<<PD2) )
#define BLUE_on (PORTD &= ~(1<<PD2) )
//true wenn taste gedrückt - nur zu testen vwenn blue_off
#define TASTER ((PIND & (1<<PD2))==0)

extern volatile uint8_t keypressed;
extern volatile uint8_t blueledshine;
void init_sumopins();

#endif /* SUMODEFS_H_ */
