/*
 * sumodefs.c
 *
 */
#include  <avr/io.h>
#include "sumodefs.h"

volatile uint8_t keypressed = 0;
volatile uint8_t blueledshine = 0;

void init_sumopins() {
	//max speed
		OSCCAL = 255;
		DDRD |= 1 << PD0;
		DDRD |= 1 << PD1;
		DDRD |= 1 << PD2;
		IR_l_off;
		IR_r_off;
		BLUE_off;

}
