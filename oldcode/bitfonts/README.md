
Source: https://www.mikrocontroller.net/topic/54860

Every font in 8 variants:
-horizontal or vertical
-mirrored or normal
-aligned to top or bottom if height != 8
Fonts larger than 16 Pixels are only avaiable in horizontal

Jede Schriftart ist in 8 Varianten enthalten:
- Horizontal und Vertikal angeordnete Bits (horizontal/vertikal)
- Gespiegelt und ungespiegelt (MSB/LSB)
- Links bzw. oben ausgerichtet, rechts bzw. unten ausgerichtet (1/2). Bei Schriftarten die eine Gr��e von einem Vielfachen von 8 Pixel in der entprechenden Richtung haben, sind beide Versionen identisch.

Schriftarten h�her als 16 Pixel sind nur in Horizontaler Anordnung verf�gbar
